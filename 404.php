<?
include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404", "Y");

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

$APPLICATION->SetTitle("Страница не найдена"); ?>

	<section class="ptb-55 gray-bg error-block-main">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="error-block-detail">
						<div class="row">
							<div class="col-lg-12 col-md-12">
								<div class="main-error-text">404</div>
								<div class="error-small-text">Извиняемся</div>
								<div class="error-slogan">Страница, которую вы ищете, не существует</div>
								<ul class="social-icon mb-20">
									<li><a href="#" title="Facebook" class="facebook"><i class="fa fa-facebook"> </i></a></li>
									<li><a href="#" title="Twitter" class="twitter"><i class="fa fa-twitter"> </i></a></li>
									<li><a href="#" title="instagram" class="instagram"><i class="fa fa-instagram"> </i></a></li>
									<li><a href="#" title="youtube" class="youtube"><i class="fa fa-youtube"> </i></a></li>
								</ul>
								<div class="mt-40"><a href="/" class="button blue"> Вернуться на главную </a></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>