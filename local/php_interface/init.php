<?
function printr($ar)
{
	global $USER;
	if ($USER->IsAdmin())
	{
		if (is_array($ar))
		{
			echo '<pre>';
			print_r($ar);
			echo '</pre>';
		} else
		{
			echo '<br>';
			var_dump($ar);
		}
	}
}

function getWordCase($int, $strRoot, $endOne, $endTwo)
{
	$result = '';
	if ($int == 0)
	{
		return $strRoot;
	}
	if ($int % 100 < 11 || $int % 100 > 14)
	{
		switch ($int % 10)
		{
			case 1:
				return $strRoot;
				break;
			case 2:
			case 3:
			case 4:
				return $strRoot . $endOne;
		}
	}
	return $strRoot . $endTwo;
}

?>
