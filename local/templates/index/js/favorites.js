    function isArray(data) {
        return Object.prototype.toString.call(data) == '[object Array]'
    }
    function inArray($search, $arr){
        for(var ind in $arr){
            if($search === $arr[ind]){
                return ind;
            }
	    }
	    return false;
    }
	var FAV = {};
    FAV.arIds = [];
    FAV.ClassActive = 'js-select-color';
    FAV.ClassMain = '.favorites';
    FAV.strCookieName = 'WT_FOVORITES';
    FAV.strAtributeID = 'data-id';
    FAV.CookieTime = 5184000; //60 days
	FAV.obCookies = '';
	FAV.count = '';


	FAV.getCookie = function () {
       var arCookie = [];
        arCookie = BX.getCookie(FAV.strCookieName);
		return arCookie;
    };
	FAV.setColorForActive = function (idEl) {
      $(FAV.ClassMain+'['+FAV.strAtributeID+'='+idEl+']').addClass(FAV.ClassActive).blur();

    };
    FAV.delColor = function (idEl) {
        $(FAV.ClassMain+'['+FAV.strAtributeID+'='+idEl+']').removeClass(FAV.ClassActive).blur();
    };
    BX.ready(function(){
        FAV.jsonIds = FAV.getCookie();
        if( FAV.jsonIds !== undefined){
            FAV.arIds = JSON.parse(FAV.jsonIds);
        }
        if(isArray(FAV.arIds)){
            for(key in FAV.arIds){
                FAV.setColorForActive(FAV.arIds[key]);
            }
        }
        FAV.count = FAV.arIds.length;
        if(FAV.count>0)
        $('.fix-menu li.fav span').text(FAV.count);

        $('.favorites').click(function () {
            var id = $(this).attr(FAV.strAtributeID);
			var strCookie;
            FAV.jsonIds = FAV.getCookie();
            if( FAV.jsonIds !== undefined){
                FAV.arIds = JSON.parse(FAV.jsonIds);
            }
            if(!isArray(FAV.arIds) || !FAV.arIds||FAV.jsonIds === undefined){
                FAV.arIds = [];
                FAV.arIds.push(id);
                FAV.jsonIds = JSON.stringify(FAV.arIds);
                FAV.setColorForActive(id);
            }else{
                if(inArray(id,FAV.arIds)){
                    FAV.delColor(id);
                    FAV.arIds.splice(inArray(id,FAV.arIds),1);
                    FAV.jsonIds = JSON.stringify(FAV.arIds);
                }else{
                    FAV.arIds.push(id);
                    FAV.setColorForActive(id);
                    FAV.jsonIds = JSON.stringify(FAV.arIds);
                }
            }
            FAV.count = FAV.arIds.length;
            if(FAV.count>0)
            $('.fix-menu li.fav span').text(FAV.count);

            console.log(FAV.arIds.length);
            console.log(FAV.arIds);
            console.log(FAV.jsonIds);
            BX.setCookie(FAV.strCookieName, FAV.jsonIds , {expires: 5184000,path:'/'});
        });
    });