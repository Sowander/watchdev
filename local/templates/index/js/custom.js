// JavaScript Document


$(function () {
    "use strict";


    $('.gallery').fancybox();

    function responsive_dropdown() {
        /* ---- For Mobile Menu Dropdown JS Start ---- */
        $('#menu span.opener').on("click", function () {
            if ($(this).hasClass("plus")) {
                $(this).parent().find('.mobile-sub-menu').slideDown();
                $(this).removeClass('plus');
                $(this).addClass('minus');
            }
            else {
                $(this).parent().find('.mobile-sub-menu').slideUp();
                $(this).removeClass('minus');
                $(this).addClass('plus');
            }
            return false;
        });
        /* ---- For Mobile Menu Dropdown JS End ---- */
        /* ---- For Sidebar JS Start ---- */
        $('.sidebar-box span.opener').on("click", function () {

            if ($(this).hasClass("plus")) {
                $(this).parent().find('.sidebar-contant').stop().slideDown();
                $(this).removeClass('plus');
                $(this).addClass('minus');
            }
            else {
                $(this).parent().find('.sidebar-contant').stop().slideUp();
                $(this).removeClass('minus');
                $(this).addClass('plus');
            }
            return false;
        });
        /* ---- For Sidebar JS End ---- */
        /* ---- For Footer JS Start ---- */
        $('.footer-static-block span.opener').on("click", function () {

            if ($(this).hasClass("plus")) {
                $(this).parent().find('.footer-block-contant').slideDown();
                $(this).removeClass('plus');
                $(this).addClass('minus');
            }
            else {
                $(this).parent().find('.footer-block-contant').slideUp();
                $(this).removeClass('minus');
                $(this).addClass('plus');
            }
            return false;
        });
        /* ---- For Footer JS End ---- */
    }

    function owlcarousel_slider() {
        /* ------------ OWL Slider Start  ------------- */
        /* ----- pro_cat_slider Start  ------ */
        $('.pro_cat_slider').owlCarousel({
            items: 5,
            nav: true,
            responsive: {
                0: {
                    items: 1,
                    nav: false
                },
                600: {
                    items: 2,
                    nav: false
                },
                900: {
                    items: 4,
                    nav: true
                },
                1200: {
                    items: 5
                }
            }
        });
        /* ----- pro_cat_slider End  ------ */
        /* ----- brand-logo Start  ------ */
        $('#brand-logo').owlCarousel({
            items: 5,
            nav: true,
            pagination: false,
            responsive: {
                0: {
                    items: 1,
                    nav: false
                },
                600: {
                    items: 2,
                    nav: false
                },
                900: {
                    items: 4,
                    nav: true
                },
                1200: {
                    items: 5
                }
            }
        });
        /* ----- brand-logo End  ------ */
        /* ---- Testimonial Start ---- */
        $("#client, .main-banner").owlCarousel({
            items: 1,
            nav: false,
            slideSpeed: 300,
            paginationSpeed: 400,
            autoPlay: false,
            pagination: true,
            singleItem: true,
            navigation: true,
            responsive: {
                768: {
                    nav: true
                }
            }
        });
        /* ----- Testimonial End  ------ */
        /* ----- Sub-banner Strat  ------ */
        $('#sub-banner-main').owlCarousel({
            items: 4,
            nav: false,
            pagination: false,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                900: {
                    items: 3
                },
                1200: {
                    items: 4
                }
            }
        });
        /* ----- Sub-banner End  ------ */
        /* ----- About-certificates Strat  ------ */
        $('#about-certificates').owlCarousel({
            items: 4,
            nav: true,
            pagination: false,
            loop: true,
            autoWidth: true,
            itemsDesktop: [1199, 3],
            itemsDesktopSmall: [991, 3],
            itemsTablet: [768, 2],
            itemsTabletSmall: [599, 2],
            itemsMobile: [419, 1]
        });


        /* ----- About-certificates End  ------ */
        /* ------------ OWL Slider End  ------------- */
    }

    $('.ui.dropdown').dropdown();

    $('.fotorama').fotorama({
        width: '100%',
        ratio: 16 / 12,
        nav: 'thumbs',
        thumbheight: '80px',
        thumbwidth: '25%',
        thumbfit: 'cover',
        arrows: true
    });

    function scrolltop_arrow() {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 250) {
                $('#scrollup').fadeIn(300);
            } else {
                $('#scrollup').fadeOut(300);
            }

            if ($(this).scrollTop() > 100) {
                $('header').addClass("header-fixed");
            }
            else {
                $('header').removeClass("header-fixed");
            }
        });

        $('#scrollup').on("click", function () {
            $("html, body").animate({scrollTop: 0}, 1000);
            return false;
        });

    }


    function custom_tab() {
        /* ----------- product category Tab Start  ------------ */
        $('.tab-stap').on('click', 'li', function () {
            // $('.tab-stap li').removeClass('active');
            // $(this).addClass('active');

            $(".product-slider-main").fadeOut(0);
            var currentLiID = $(this).attr('id');
            $("#data-" + currentLiID).fadeIn();
            return false;
        });
        /* ------------ product category Tab End  ------------ */
    }

    function setminheight() {
        $(".pro_cat").css("min-height", $(".product-slider-main").height());
        $(".pro-detail-main").css("min-height", $(".special-products-block").height());
    }

    /* Price-range Js Start */
    function price_range() {
        var priceRange = $('.price-range');
        var sliderRange = $('#slider-range');


        var minRange = priceRange.attr('minRange');
        var maxRange = priceRange.attr('maxRange');
        var minValue = priceRange.attr('minValue');
        var maxValue = priceRange.attr('maxValue');

        sliderRange.slider({
            range: true,
            min: +minRange,
            max: +maxRange,
            values: [minValue, maxValue],
            slide: function (event, ui) {
                $("#amount").html(ui.values[0] + ' <i class="fa fa-rub" aria-hidden="true"></i> - ' + ui.values[1] + ' <i class="fa fa-rub" aria-hidden="true"></i>');
            }
        });
        $("#amount").html(sliderRange.slider("values", 0) + ' <i class="fa fa-rub" aria-hidden="true"></i> - ' + sliderRange.slider("values", 1) + ' <i class="fa fa-rub" aria-hidden="true"></i>');

    }

    /* Price-range Js End */
    $('.popup').magnificPopup();
    /*Video_Popup Js Start*/
    function video_popup() {
        $('.popup-youtube').magnificPopup({
            disableOn: 700,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
            fixedContentPos: false
        });
    }

    /*Video_Popup Js Ends*/

    /* Product Detail Page Tab JS Start */
    function description_tab() {
        $("#tabs li a").on("click", function (e) {
            var title = $(e.currentTarget).attr("title");
            $(".tab_content li div").removeClass("selected");
            $(".tab-" + title).addClass("selected");
            $(".items-" + title).addClass("selected");
            $("#items").attr("class", "tab-" + title);

            return false;
        }).removeClass("selected");
    }

    /* Product Detail Page Tab JS End */

    $(document).ready(function () {
        owlcarousel_slider();
        price_range();
        responsive_dropdown();
        setminheight();
        description_tab();
        custom_tab();
        scrolltop_arrow();
        video_popup();
    });

    $(window).on("resize", function () {
        setminheight();
    });


});

$(window).on("load", function () {
    // Animate loader off screen
    $(".se-pre-con").fadeOut("slow");
});

$(document).ready(function () {
    $('.sidebar-mobile-filter').click(function () {
        $('body').addClass('body-hidden');
        $('.mobile-sidebar').addClass('mobile-sidebar-open');
    });

    $('.mobile-sidebar-close').click(function () {
        $('body').removeClass('body-hidden');
        $('.mobile-sidebar').removeClass('mobile-sidebar-open');
    });






// $('.popup_help').magnificPopup({
//     position: 'bottom left'
// });


    $("label[data-compare=yes]").click(function () {
        var className = 'js-select-color';
        if (!!$(this).hasClass(className)) {
            $(this).removeClass(className).blur();
        } else {
            $(this).addClass(className).blur();
        }
    });

    $('input[name=PERSONAL_PHONE]').mask("+7(999) 999-9999");
    $('input[name=user_phone]').mask("+7(999) 999-9999");

});


