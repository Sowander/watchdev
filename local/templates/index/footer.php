<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<?
if (CSite::InDir('/about/') and $APPLICATION->GetCurDir() != '/about/')
{
	?>
	</div>
	<div class="col-md-12">
		<hr>
		<div class="row">
			<div class="col-md-12">
				<div class="mt-20 right-side"><a href="/catalog/" class="button basic">Вернуться в магазин</a></div>
			</div>
		</div>
	</div>
	</div>
	</div>
	</div>
	</section>

<? } ?>



<? if ($dir != '/')
{ ?>
	</div>
<? } ?>


</div>


<? if ($dir == '/')
{ ?>
	<? $APPLICATION->IncludeComponent("bitrix:news.list", "main_brands", Array(
	"ACTIVE_DATE_FORMAT" => "",    // Формат показа даты
	"ADD_SECTIONS_CHAIN" => "N",    // Включать раздел в цепочку навигации
	"AJAX_MODE" => "N",    // Включить режим AJAX
	"AJAX_OPTION_ADDITIONAL" => "",    // Дополнительный идентификатор
	"AJAX_OPTION_HISTORY" => "N",    // Включить эмуляцию навигации браузера
	"AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
	"AJAX_OPTION_STYLE" => "Y",    // Включить подгрузку стилей
	"CACHE_FILTER" => "N",    // Кешировать при установленном фильтре
	"CACHE_GROUPS" => "Y",    // Учитывать права доступа
	"CACHE_TIME" => "3600",    // Время кеширования (сек.)
	"CACHE_TYPE" => "A",    // Тип кеширования
	"CHECK_DATES" => "Y",    // Показывать только активные на данный момент элементы
	"DETAIL_URL" => "",    // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
	"DISPLAY_BOTTOM_PAGER" => "N",    // Выводить под списком
	"DISPLAY_DATE" => "Y",    // Выводить дату элемента
	"DISPLAY_NAME" => "Y",    // Выводить название элемента
	"DISPLAY_PICTURE" => "Y",    // Выводить изображение для анонса
	"DISPLAY_PREVIEW_TEXT" => "Y",    // Выводить текст анонса
	"DISPLAY_TOP_PAGER" => "N",    // Выводить над списком
	"FIELD_CODE" => array(    // Поля
		0 => "",
		1 => "",
	),
	"FILTER_NAME" => "",    // Фильтр
	"HIDE_LINK_WHEN_NO_DETAIL" => "N",    // Скрывать ссылку, если нет детального описания
	"IBLOCK_ID" => "9",    // Код информационного блока
	"IBLOCK_TYPE" => "references",    // Тип информационного блока (используется только для проверки)
	"INCLUDE_IBLOCK_INTO_CHAIN" => "N",    // Включать инфоблок в цепочку навигации
	"INCLUDE_SUBSECTIONS" => "Y",    // Показывать элементы подразделов раздела
	"MESSAGE_404" => "",    // Сообщение для показа (по умолчанию из компонента)
	"NEWS_COUNT" => "40",    // Количество новостей на странице
	"PAGER_BASE_LINK_ENABLE" => "N",    // Включить обработку ссылок
	"PAGER_DESC_NUMBERING" => "N",    // Использовать обратную навигацию
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",    // Время кеширования страниц для обратной навигации
	"PAGER_SHOW_ALL" => "N",    // Показывать ссылку "Все"
	"PAGER_SHOW_ALWAYS" => "N",    // Выводить всегда
	"PAGER_TEMPLATE" => ".default",    // Шаблон постраничной навигации
	"PAGER_TITLE" => "Новости",    // Название категорий
	"PARENT_SECTION" => "",    // ID раздела
	"PARENT_SECTION_CODE" => "",    // Код раздела
	"PREVIEW_TRUNCATE_LEN" => "50",    // Максимальная длина анонса для вывода (только для типа текст)
	"PROPERTY_CODE" => array(    // Свойства
		0 => "",
		1 => "CLASS",
		2 => "NAME2",
		3 => "NAME3",
		4 => "",
	),
	"SET_BROWSER_TITLE" => "N",    // Устанавливать заголовок окна браузера
	"SET_LAST_MODIFIED" => "N",    // Устанавливать в заголовках ответа время модификации страницы
	"SET_META_DESCRIPTION" => "N",    // Устанавливать описание страницы
	"SET_META_KEYWORDS" => "N",    // Устанавливать ключевые слова страницы
	"SET_STATUS_404" => "N",    // Устанавливать статус 404
	"SET_TITLE" => "N",    // Устанавливать заголовок страницы
	"SHOW_404" => "N",    // Показ специальной страницы
	"SORT_BY1" => "ACTIVE_FROM",    // Поле для первой сортировки новостей
	"SORT_BY2" => "ID",    // Поле для второй сортировки новостей
	"SORT_ORDER1" => "ASC",    // Направление для первой сортировки новостей
	"SORT_ORDER2" => "ASC",    // Направление для второй сортировки новостей
	"STRICT_SECTION_CHECK" => "N",    // Строгая проверка раздела для показа списка
	"COMPONENT_TEMPLATE" => ".default"
),
	false
); ?>
<? } ?>


<!-- CONTAINER END -->

<div class="hidden">
	<div class="modal-form" id="orderCall">
		<form method="POST" id="formOrderCall">
			<div class="modal-title">
				<h3>Обратная связь</h3>
			</div>
			<div>
				<p>Менеджер ответит Вам в течение 15 минут</p>
			</div>
			<div class="input-box">
				<label for="modal-name">Имя<span class="required"> *</span></label>
				<input type="text" id="modal-name" name="modal-name" required placeholder="Введите имя">
			</div>
			<div class="input-box">
				<label for="modal-phone">Телефон<span class="required"> *</span></label>
				<input type="text" id="modal-phone" name="modal-phone" required placeholder="Введите телефон">
			</div>
			<div class="input-box">
				<label for="modal-email">Email</label>
				<input type="email" id="modal-email" name="modal-email" placeholder="Введите email">
			</div>
			<input type="hidden" name="link" value="<?= $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>">
			<div class="input-box">
				<input type="submit" value="Отправить" class="button green right-side">
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#formOrderCall").submit(function () {
            var form = $(this);
            var data = form.serialize();
            var form_body = $('.modal-form').html();
            $.ajax({
                type: 'POST',
                url: '/bin/mail.php',
                dataType: 'json',
                data: data,
                success: function (data) {
                    if (data['error']) {
                        alert(data['error']);
                    } else {
                        $('.modal-form').html('<p style="text-align:center">Ваша заявка отправлена</p><p style="text-align:center">Менеджер ответит Вам в течение 15 минут</p>');
                    }
                },
            });

            return false;
        });
    });
</script>
<!-- FOOTER START -->

<div class="footer dark-bg">
	<div class="container">
		<div class="footer-inner">
			<div class="footer-middle">
				<div class="row">
					<div class="col-md-2 col-sm-12 col-xs-12 f-col">
						<div class="footer-static-block"><span class="opener plus"></span>
							<h3 class="title">Сервис</h3>
							<? $APPLICATION->IncludeComponent("bitrix:menu", "bottom", Array(
								"ROOT_MENU_TYPE" => "service",    // Тип меню для первого уровня
								"MENU_CACHE_TYPE" => "A",    // Тип кеширования
								"MENU_CACHE_TIME" => "36000000",    // Время кеширования (сек.)
								"MENU_CACHE_USE_GROUPS" => "Y",    // Учитывать права доступа
								"MENU_THEME" => "site",
								"CACHE_SELECTED_ITEMS" => "N",
								"MENU_CACHE_GET_VARS" => "",    // Значимые переменные запроса
								"MAX_LEVEL" => "1",    // Уровень вложенности меню
								"CHILD_MENU_TYPE" => "",    // Тип меню для остальных уровней
								"USE_EXT" => "N",    // Подключать файлы с именами вида .тип_меню.menu_ext.php
								"DELAY" => "N",    // Откладывать выполнение шаблона меню
								"ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
								"COMPONENT_TEMPLATE" => ".default"
							),
								false
							); ?>

						</div>
					</div>
					<div class="col-md-2 col-sm-12 col-xs-12 f-col">
						<div class="footer-static-block"><span class="opener plus"></span>
							<h3 class="title">Магазин</h3>
							<? $APPLICATION->IncludeComponent(
								"bitrix:menu",
								"bottom",
								array(
									"ROOT_MENU_TYPE" => "bottom",
									"MENU_CACHE_TYPE" => "A",
									"MENU_CACHE_TIME" => "36000000",
									"MENU_CACHE_USE_GROUPS" => "Y",
									"MENU_THEME" => "site",
									"CACHE_SELECTED_ITEMS" => "N",
									"MENU_CACHE_GET_VARS" => array(),
									"MAX_LEVEL" => "1",
									"CHILD_MENU_TYPE" => "",
									"USE_EXT" => "N",
									"DELAY" => "N",
									"ALLOW_MULTI_SELECT" => "N",
									"COMPONENT_TEMPLATE" => "bottom"
								),
								false
							); ?>

						</div>
					</div>
					<div class="col-md-5 col-sm-12 col-xs-12">
						<div class="footer-static-block"><span class="opener plus"></span>
							<h3 class="title">О магазине</h3>
							<div class="footer-block-contant">
								<? $APPLICATION->IncludeComponent(
									"bitrix:main.include",
									"",
									Array(
										"AREA_FILE_SHOW" => "file",
										"PATH" => SITE_DIR . "include/footerabout.php",
										"AREA_FILE_RECURSIVE" => "N",
										"EDIT_MODE" => "php",
									),
									false,
									Array('HIDE_ICONS' => 'N')
								); ?>


								<div class="payment left-side float-none-sm float-none-xs center-xs">
									<ul class="payment_icon">
										<li class="discover">
											<a>
												<img src="<?= SITE_TEMPLATE_PATH ?>/images/qiwi.png" alt="">
											</a>
										</li>
										<li class="visa">
											<a>
												<img src="<?= SITE_TEMPLATE_PATH ?>/images/visa-img.png" alt="">
											</a>
										</li>
										<li class="mastro">
											<a>
												<img src="<?= SITE_TEMPLATE_PATH ?>/images/MasterCard.png" alt="">
											</a>
										</li>
										<li class="paypal">
											<a>
												<img src="<?= SITE_TEMPLATE_PATH ?>/images/yandexmoney.png" alt="">
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-12 col-xs-12 f-col">
						<div class="footer-static-block f-address"><span class="opener plus"></span>
							<h3 class="title">Как нас найти</h3>
							<div class="">
								<!-- <span class="opener plus"></span> -->
								<ul class="footer-block-contant address-footer">


									<? $APPLICATION->IncludeComponent(
										"bitrix:main.include",
										"",
										Array(
											"AREA_FILE_SHOW" => "file",
											"PATH" => SITE_DIR . "include/footeraddr.php",
											"AREA_FILE_RECURSIVE" => "N",
											"EDIT_MODE" => "php",
										),
										false,
										Array('HIDE_ICONS' => 'N')
									); ?>


									<li class="item">
										<div class="footer_social pt-xs-15 float-none-xs float-none-sm center-xs mt-xs-15">
											<ul class="social-icon">

												<? $APPLICATION->IncludeComponent(
													"bitrix:main.include",
													"",
													Array(
														"AREA_FILE_SHOW" => "file",
														"PATH" => SITE_DIR . "include/footersoc.php",
														"AREA_FILE_RECURSIVE" => "N",
														"EDIT_MODE" => "php",
													),
													false,
													Array('HIDE_ICONS' => 'N')
												); ?>


											</ul>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<hr>
			<div class="footer-bottom mb-30">
				<div class="row">

					<? $APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => SITE_DIR . "include/footercp.php",
							"AREA_FILE_RECURSIVE" => "N",
							"EDIT_MODE" => "php",
						),
						false,
						Array('HIDE_ICONS' => 'N')
					); ?>


				</div>
			</div>
		</div>
	</div>
</div>


<div class="scroll-top">
	<div id="scrollup"></div>
</div>
<!-- FOOTER END -->
</div>


<!-- <script>
/* ------------ Newslater-popup JS Start ------------- */
$(window).load(function() {
  $.magnificPopup.open({
	items: {src: '#newslater-popup'},type: 'inline'}, 0);
});
  /* ------------ Newslater-popup JS End ------------- */
</script> -->


<script id="dsq-count-scr" src="//watch-dev17-g-lab-ru.disqus.com/count.js" async></script>

</body>
</html>