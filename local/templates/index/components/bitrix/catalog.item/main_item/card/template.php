<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */

//print_r($itemIds);


?>
<?
$file = CFile::ResizeImageGet($item['PREVIEW_PICTURE']['ID'], array('width' => 276, 'height' => 331), BX_RESIZE_IMAGE_PROPORTIONAL, true);
$item['PREVIEW_PICTURE']['SRC'] = $file['src'];
?>

<div class="product-image">
	<a href="<?= $item['DETAIL_PAGE_URL'] ?>"> <img id="<?= $itemIds['PICT'] ?>" src="<?= $item['PREVIEW_PICTURE']['SRC'] ?>" alt=""> </a>
	<div class="product-item__sale">
		<? if ($price['RATIO_PRICE'] < $price['RATIO_BASE_PRICE'])
		{ ?>
		<span><i class="fa fa-tag" aria-hidden="true"></i> Скидка <?= round(100 - ($price['RATIO_PRICE'] * 100 / $price['RATIO_BASE_PRICE'])) ?>%<? } ?></span>
	</div>
	<? if ($item['PROPERTIES']['NEWPRODUCT']['VALUE'] == 'да')
	{ ?>
		<p class="product-item__new"> Новинка </p>
	<? } else
	{ ?>
		<p class="product-item__new"> &nbsp; </p>
	<? } ?>
</div>
<div class="product-item__price">
	<span id="<?= $itemIds['PRICE'] ?>" class="price"><?= $price['RATIO_PRICE'] ?><i class="fa fa-rub" aria-hidden="true"></i> &nbsp; </span>
	<? if ($arParams['SHOW_OLD_PRICE'] === 'Y')
	{ ?>
		<del id="<?= $itemIds['PRICE_OLD'] ?>" class="price old-price"
			<?= ($price['RATIO_PRICE'] >= $price['RATIO_BASE_PRICE'] ? 'style="display: none;"' : '') ?>>
			<?= $price['RATIO_BASE_PRICE'] ?><i class="fa fa-rub" aria-hidden="true"></i>
		</del>
	<? } ?>
</div>
<div class="product-item__name"><a href="<?= $item['DETAIL_PAGE_URL'] ?>"><?= $productTitle ?></a></div>
<? if ($item['PRODUCT']['QUANTITY'] > 0)
{ ?>
	<div class="product-item__stock"> в наличии</div>
<? } else
{ ?>
	<div class="product-item__stock"> под заказ</div>
<? } ?>
<div class="product-item__buttons" id="<?= $itemIds['BASKET_ACTIONS'] ?>">
	<? if ($item['PRODUCT']['QUANTITY'] > 0)
	{ ?>
		<a href="javascript:void(0)" id="<?= $itemIds['BUY_LINK'] ?>" class="product-item__button product-item__button--blue _left"> В корзину</a>
	<? } ?>


	<input style="display:none" id="<?= $itemIds['QUANTITY'] ?>" type="tel" name="<?= $arParams['PRODUCT_QUANTITY_VARIABLE']; ?>" value="1">
	<a href="javascript:void(0)" class="favorites product-item__button popup_help" data-id="<?=$item['ID']?>" data-content="Добавить в избранное"><i class="fa fa-heart-o
"></i></a>

	<?
	$jsSelectColor = '';
	if (isset($_SESSION[$arParams["COMPARE_NAME"]][$item["IBLOCK_ID"]]["ITEMS"][$item['ID']]))
	{
		$jsSelectColor = ' js-select-color';
	}
	?>
	<label style="cursor: pointer;margin: 0" id="<?= $itemIds['COMPARE_LINK'] ?>" data-compare="yes" class="product-item__button popup_help<?= $jsSelectColor ?>">
		<i class="fa fa-balance-scale"></i>
		<input style="display:none;" data-entity="compare-checkbox" type="checkbox" value="1">
	</label>

</div>