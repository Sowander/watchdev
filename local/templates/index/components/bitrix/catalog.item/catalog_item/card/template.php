<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */
?>
<?
$file = CFile::ResizeImageGet($item['PREVIEW_PICTURE']['ID'], array('width' => 276, 'height' => 331), BX_RESIZE_IMAGE_PROPORTIONAL, true);
$item['PREVIEW_PICTURE']['SRC'] = $file[src];
?>


<div class="product-image"><a href="<?= $item['DETAIL_PAGE_URL'] ?>"> <img src="<?= $item['PREVIEW_PICTURE']['SRC'] ?>" alt=""> </a>
	<div class="product-detail-inner">
		<div class="detail-inner-left left-side">
			<ul>
				<li class="pro-cart-icon">
					<form>
						<button title="Add to Cart"><i class="fa fa-cart-plus"></i></button>
					</form>
				</li>
				<li class="pro-wishlist-icon"><a href="#"><i class="fa fa-heart"></i></a></li>
				<li class="pro-compare-icon"><a href="#"><i class="fa fa-bar-chart"></i></a></li>
			</ul>
		</div>
	</div>
</div>
<div class="product-item-details">
	<div class="product-item-name"><a href="<?= $item['DETAIL_PAGE_URL'] ?>"><?= $productTitle ?></a></div>

	<div class="price-box"><span class="price"><?= $price['PRINT_RATIO_PRICE'] ?></span>


		<?
		if ($arParams['SHOW_OLD_PRICE'] === 'Y')
		{
			?>
			<del class="price old-price"
				<?= ($price['RATIO_PRICE'] >= $price['RATIO_BASE_PRICE'] ? 'style="display: none;"' : '') ?>>
				<?= $price['PRINT_RATIO_BASE_PRICE'] ?>
			</del>
			<?
		}
		?>


	</div>


</div>



