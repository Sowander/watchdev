(function () {
    'use strict';

    var initParent = BX.Sale.OrderAjaxComponent.init,
        getBlockFooterParent = BX.Sale.OrderAjaxComponent.getBlockFooter,
        editOrderParent = BX.Sale.OrderAjaxComponent.editOrder
    ;

    BX.namespace('BX.Sale.OrderAjaxComponentExt');

    BX.Sale.OrderAjaxComponentExt = BX.Sale.OrderAjaxComponent;

    BX.Sale.OrderAjaxComponentExt.init = function (parameters) {
        initParent.apply(this, arguments);

        var editSteps = this.orderBlockNode.querySelectorAll('.bx-soa-editstep'), i;
        for (i in editSteps) {
            if (editSteps.hasOwnProperty(i)) {
                BX.remove(editSteps[i]);
            }
        }

    };

    BX.Sale.OrderAjaxComponentExt.getBlockFooter = function (node) {
        var parentNodeSection = BX.findParent(node, {className: 'bx-soa-section'});

        getBlockFooterParent.apply(this, arguments);

        if (/bx-soa-auth|bx-soa-properties|bx-soa-paysystem|bx-soa-delivery/.test(parentNodeSection.id)) {
            BX.remove(parentNodeSection.querySelector('.pull-left'));
            BX.remove(parentNodeSection.querySelector('.pull-right'));
        }

    };


    BX.Sale.OrderAjaxComponentExt.editOrder = function (section) {

        editOrderParent.apply(this, arguments);

        var sections = this.orderBlockNode.querySelectorAll('.bx-soa-section.bx-active'), i;

        for (i in sections) {
            if (sections.hasOwnProperty(i)) {
                if (!(/bx-soa-auth|bx-soa-properties|bx-soa-paysystem|bx-soa-delivery/.test(sections[i].id))) {
                    sections[i].classList.add('bx-soa-section-hide');
                }
            }
        }

        this.show(BX('bx-soa-properties', 'bx-soa-paysystem', 'bx-soa-delivery'));


        this.editActivePaySystemBlock(true);
        this.editActiveDeliveryBlock(true);
        this.alignBasketColumns();

        if (!this.result.IS_AUTHORIZED) {
            this.switchOrderSaveButtons(true);
        }

    };
    BX.Sale.OrderAjaxComponentExt.createTotalUnit = function (name, value, params) {
        var totalValue, className = 'bx-soa-cart-total-line';
        value = value.replace('руб.', '<i class="fa fa-rub"></i>');


        name = name || '';
        value = value || '';
        params = params || {};

        if (params.error) {
            totalValue = [BX.create('A', {
                props: {className: 'bx-soa-price-not-calc'},
                html: value,
                events: {
                    click: BX.delegate(function () {
                        this.animateScrollTo(this.deliveryBlockNode);
                    }, this)
                }
            })];
        }
        else if (params.free) {
            totalValue = [BX.create('SPAN', {
                props: {className: 'bx-soa-price-free'},
                html: value
            })];
        }
        else {
            totalValue = [value];
        }

        if (params.total) {
            className += ' bx-soa-cart-total-line-total';
        }

        if (params.highlighted) {
            className += ' bx-soa-cart-total-line-highlighted';
        }


        return BX.create('TR', {
            props: {className: className},
            children: [
                BX.create('TD', {
                        children: [
                            BX.create(
                                'DIV', {
                                    props: {className: 'cart-totals align-center total'}, text: name
                                }
                            )
                        ]
                    }
                ),
                BX.create('TD', {}),
                BX.create('TD', {
                    children: [BX.create(
                        'DIV', {
                            props: {className: 'cart-totals'},
                            children: totalValue
                        }
                    )]
                }),
                BX.create('TD', {}),
                BX.create('TD', {})
            ]
        });
    };

    BX.Sale.OrderAjaxComponentExt.editActiveRegionBlock = function (activeNodeMode) {
        var node = activeNodeMode ? this.regionBlockNode : this.regionHiddenBlockNode,
            regionContent, regionNode, regionNodeCol;
        if (this.initialized.region) {

            BX.remove(BX.lastChild(node));
            node.appendChild(BX.firstChild(this.regionHiddenBlockNode));
        }
        else {
            regionContent = node.querySelector('.bx-soa-section-content');
            if (!regionContent) {
                regionContent = this.getNewContainer();
                node.appendChild(regionContent);
            }
            else
                BX.cleanNode(regionContent);

            this.getErrorContainer(regionContent);

            regionNode = BX.create('DIV', {props: {className: 'row'}});

            regionNodeCol = BX.create('DIV', {props: {className: 'col-xs-12'}});
            this.getPersonTypeControl(regionNodeCol);

            this.getProfilesControl(regionNodeCol);

            this.getDeliveryLocationInput(regionNodeCol);

            if (!this.result.SHOW_AUTH) {
                if (this.regionBlockNotEmpty) {
                    BX.addClass(this.regionBlockNode, 'bx-active');
                    this.regionBlockNode.style.display = '';
                }
                else {
                    BX.removeClass(this.regionBlockNode, 'bx-active');
                    this.regionBlockNode.style.display = 'none';

                    if (!this.result.IS_AUTHORIZED || typeof this.result.LAST_ORDER_DATA.FAIL !== 'undefined')
                        this.initFirstSection();
                }
            }


            regionNode.appendChild(regionNodeCol);
            regionContent.appendChild(regionNode);
            this.getBlockFooter(regionContent);
        }
    };

    BX.Sale.OrderAjaxComponentExt.initFirstSection = function (parameters) {

    };


})();