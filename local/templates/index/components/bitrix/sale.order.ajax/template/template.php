<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main,
	Bitrix\Main\Localization\Loc;

/**
 * @var array $arParams
 * @var array $arResult
 * @var $APPLICATION CMain
 * @var $USER CUser
 * @var $component SaleOrderAjax
 */

$context = Main\Application::getInstance()->getContext();
$request = $context->getRequest();
$server = $context->getServer();

if (empty($arParams['TEMPLATE_THEME']))
{
	$arParams['TEMPLATE_THEME'] = Main\ModuleManager::isModuleInstalled('bitrix.eshop') ? 'site' : 'blue';
}

if ($arParams['TEMPLATE_THEME'] == 'site')
{
	$templateId = Main\Config\Option::get('main', 'wizard_template_id', 'eshop_bootstrap', $component->getSiteId());
	$templateId = preg_match('/^eshop_adapt/', $templateId) ? 'eshop_adapt' : $templateId;
	$arParams['TEMPLATE_THEME'] = Main\Config\Option::get('main', 'wizard_' . $templateId . '_theme_id', 'blue', $component->getSiteId());
}

if (!empty($arParams['TEMPLATE_THEME']))
{
	if (!is_file($server->getDocumentRoot() . '/bitrix/css/main/themes/' . $arParams['TEMPLATE_THEME'] . '/style.css'))
	{
		$arParams['TEMPLATE_THEME'] = 'blue';
	}
}

$arParams['ALLOW_USER_PROFILES'] = $arParams['ALLOW_USER_PROFILES'] === 'Y' ? 'Y' : 'N';
$arParams['SKIP_USELESS_BLOCK'] = $arParams['SKIP_USELESS_BLOCK'] === 'N' ? 'N' : 'Y';

if (!isset($arParams['SHOW_ORDER_BUTTON']))
{
	$arParams['SHOW_ORDER_BUTTON'] = 'final_step';
}

$arParams['SHOW_TOTAL_ORDER_BUTTON'] = $arParams['SHOW_TOTAL_ORDER_BUTTON'] === 'Y' ? 'Y' : 'N';
$arParams['SHOW_PAY_SYSTEM_LIST_NAMES'] = $arParams['SHOW_PAY_SYSTEM_LIST_NAMES'] === 'N' ? 'N' : 'Y';
$arParams['SHOW_PAY_SYSTEM_INFO_NAME'] = $arParams['SHOW_PAY_SYSTEM_INFO_NAME'] === 'N' ? 'N' : 'Y';
$arParams['SHOW_DELIVERY_LIST_NAMES'] = $arParams['SHOW_DELIVERY_LIST_NAMES'] === 'N' ? 'N' : 'Y';
$arParams['SHOW_DELIVERY_INFO_NAME'] = $arParams['SHOW_DELIVERY_INFO_NAME'] === 'N' ? 'N' : 'Y';
$arParams['SHOW_DELIVERY_PARENT_NAMES'] = $arParams['SHOW_DELIVERY_PARENT_NAMES'] === 'N' ? 'N' : 'Y';
$arParams['SHOW_STORES_IMAGES'] = $arParams['SHOW_STORES_IMAGES'] === 'N' ? 'N' : 'Y';

if (!isset($arParams['BASKET_POSITION']))
{
	$arParams['BASKET_POSITION'] = 'after';
}

$arParams['SHOW_BASKET_HEADERS'] = $arParams['SHOW_BASKET_HEADERS'] === 'Y' ? 'Y' : 'N';
$arParams['DELIVERY_FADE_EXTRA_SERVICES'] = $arParams['DELIVERY_FADE_EXTRA_SERVICES'] === 'Y' ? 'Y' : 'N';
$arParams['SHOW_COUPONS_BASKET'] = $arParams['SHOW_COUPONS_BASKET'] === 'N' ? 'N' : 'Y';
$arParams['SHOW_COUPONS_DELIVERY'] = $arParams['SHOW_COUPONS_DELIVERY'] === 'N' ? 'N' : 'Y';
$arParams['SHOW_COUPONS_PAY_SYSTEM'] = $arParams['SHOW_COUPONS_PAY_SYSTEM'] === 'Y' ? 'Y' : 'N';
$arParams['SHOW_NEAREST_PICKUP'] = $arParams['SHOW_NEAREST_PICKUP'] === 'Y' ? 'Y' : 'N';
$arParams['DELIVERIES_PER_PAGE'] = isset($arParams['DELIVERIES_PER_PAGE']) ? intval($arParams['DELIVERIES_PER_PAGE']) : 9;
$arParams['PAY_SYSTEMS_PER_PAGE'] = isset($arParams['PAY_SYSTEMS_PER_PAGE']) ? intval($arParams['PAY_SYSTEMS_PER_PAGE']) : 9;
$arParams['PICKUPS_PER_PAGE'] = isset($arParams['PICKUPS_PER_PAGE']) ? intval($arParams['PICKUPS_PER_PAGE']) : 5;
$arParams['SHOW_PICKUP_MAP'] = $arParams['SHOW_PICKUP_MAP'] === 'N' ? 'N' : 'Y';
$arParams['SHOW_MAP_IN_PROPS'] = $arParams['SHOW_MAP_IN_PROPS'] === 'Y' ? 'Y' : 'N';
$arParams['USE_YM_GOALS'] = $arParams['USE_YM_GOALS'] === 'Y' ? 'Y' : 'N';
$arParams['USE_ENHANCED_ECOMMERCE'] = isset($arParams['USE_ENHANCED_ECOMMERCE']) && $arParams['USE_ENHANCED_ECOMMERCE'] === 'Y' ? 'Y' : 'N';
$arParams['DATA_LAYER_NAME'] = isset($arParams['DATA_LAYER_NAME']) ? trim($arParams['DATA_LAYER_NAME']) : 'dataLayer';
$arParams['BRAND_PROPERTY'] = isset($arParams['BRAND_PROPERTY']) ? trim($arParams['BRAND_PROPERTY']) : '';

$useDefaultMessages = !isset($arParams['USE_CUSTOM_MAIN_MESSAGES']) || $arParams['USE_CUSTOM_MAIN_MESSAGES'] != 'Y';

if ($useDefaultMessages || !isset($arParams['MESS_AUTH_BLOCK_NAME']))
{
	$arParams['MESS_AUTH_BLOCK_NAME'] = Loc::getMessage('AUTH_BLOCK_NAME_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_REG_BLOCK_NAME']))
{
	$arParams['MESS_REG_BLOCK_NAME'] = Loc::getMessage('REG_BLOCK_NAME_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_BASKET_BLOCK_NAME']))
{
	$arParams['MESS_BASKET_BLOCK_NAME'] = Loc::getMessage('BASKET_BLOCK_NAME_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_REGION_BLOCK_NAME']))
{
	$arParams['MESS_REGION_BLOCK_NAME'] = Loc::getMessage('REGION_BLOCK_NAME_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_PAYMENT_BLOCK_NAME']))
{
	$arParams['MESS_PAYMENT_BLOCK_NAME'] = Loc::getMessage('PAYMENT_BLOCK_NAME_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_DELIVERY_BLOCK_NAME']))
{
	$arParams['MESS_DELIVERY_BLOCK_NAME'] = Loc::getMessage('DELIVERY_BLOCK_NAME_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_BUYER_BLOCK_NAME']))
{
	$arParams['MESS_BUYER_BLOCK_NAME'] = Loc::getMessage('BUYER_BLOCK_NAME_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_BACK']))
{
	$arParams['MESS_BACK'] = Loc::getMessage('BACK_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_FURTHER']))
{
	$arParams['MESS_FURTHER'] = Loc::getMessage('FURTHER_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_EDIT']))
{
	$arParams['MESS_EDIT'] = Loc::getMessage('EDIT_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_ORDER']))
{
	$arParams['MESS_ORDER'] = Loc::getMessage('ORDER_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_PRICE']))
{
	$arParams['MESS_PRICE'] = Loc::getMessage('PRICE_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_PERIOD']))
{
	$arParams['MESS_PERIOD'] = Loc::getMessage('PERIOD_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_NAV_BACK']))
{
	$arParams['MESS_NAV_BACK'] = Loc::getMessage('NAV_BACK_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_NAV_FORWARD']))
{
	$arParams['MESS_NAV_FORWARD'] = Loc::getMessage('NAV_FORWARD_DEFAULT');
}

$useDefaultMessages = !isset($arParams['USE_CUSTOM_ADDITIONAL_MESSAGES']) || $arParams['USE_CUSTOM_ADDITIONAL_MESSAGES'] != 'Y';

if ($useDefaultMessages || !isset($arParams['MESS_PRICE_FREE']))
{
	$arParams['MESS_PRICE_FREE'] = Loc::getMessage('PRICE_FREE_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_ECONOMY']))
{
	$arParams['MESS_ECONOMY'] = Loc::getMessage('ECONOMY_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_REGISTRATION_REFERENCE']))
{
	$arParams['MESS_REGISTRATION_REFERENCE'] = Loc::getMessage('REGISTRATION_REFERENCE_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_AUTH_REFERENCE_1']))
{
	$arParams['MESS_AUTH_REFERENCE_1'] = Loc::getMessage('AUTH_REFERENCE_1_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_AUTH_REFERENCE_2']))
{
	$arParams['MESS_AUTH_REFERENCE_2'] = Loc::getMessage('AUTH_REFERENCE_2_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_AUTH_REFERENCE_3']))
{
	$arParams['MESS_AUTH_REFERENCE_3'] = Loc::getMessage('AUTH_REFERENCE_3_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_ADDITIONAL_PROPS']))
{
	$arParams['MESS_ADDITIONAL_PROPS'] = Loc::getMessage('ADDITIONAL_PROPS_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_USE_COUPON']))
{
	$arParams['MESS_USE_COUPON'] = Loc::getMessage('USE_COUPON_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_COUPON']))
{
	$arParams['MESS_COUPON'] = Loc::getMessage('COUPON_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_PERSON_TYPE']))
{
	$arParams['MESS_PERSON_TYPE'] = Loc::getMessage('PERSON_TYPE_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_SELECT_PROFILE']))
{
	$arParams['MESS_SELECT_PROFILE'] = Loc::getMessage('SELECT_PROFILE_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_REGION_REFERENCE']))
{
	$arParams['MESS_REGION_REFERENCE'] = Loc::getMessage('REGION_REFERENCE_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_PICKUP_LIST']))
{
	$arParams['MESS_PICKUP_LIST'] = Loc::getMessage('PICKUP_LIST_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_NEAREST_PICKUP_LIST']))
{
	$arParams['MESS_NEAREST_PICKUP_LIST'] = Loc::getMessage('NEAREST_PICKUP_LIST_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_SELECT_PICKUP']))
{
	$arParams['MESS_SELECT_PICKUP'] = Loc::getMessage('SELECT_PICKUP_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_INNER_PS_BALANCE']))
{
	$arParams['MESS_INNER_PS_BALANCE'] = Loc::getMessage('INNER_PS_BALANCE_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_ORDER_DESC']))
{
	$arParams['MESS_ORDER_DESC'] = Loc::getMessage('ORDER_DESC_DEFAULT');
}

$useDefaultMessages = !isset($arParams['USE_CUSTOM_ERROR_MESSAGES']) || $arParams['USE_CUSTOM_ERROR_MESSAGES'] != 'Y';

if ($useDefaultMessages || !isset($arParams['MESS_PRELOAD_ORDER_TITLE']))
{
	$arParams['MESS_PRELOAD_ORDER_TITLE'] = Loc::getMessage('PRELOAD_ORDER_TITLE_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_SUCCESS_PRELOAD_TEXT']))
{
	$arParams['MESS_SUCCESS_PRELOAD_TEXT'] = Loc::getMessage('SUCCESS_PRELOAD_TEXT_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_FAIL_PRELOAD_TEXT']))
{
	$arParams['MESS_FAIL_PRELOAD_TEXT'] = Loc::getMessage('FAIL_PRELOAD_TEXT_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_DELIVERY_CALC_ERROR_TITLE']))
{
	$arParams['MESS_DELIVERY_CALC_ERROR_TITLE'] = Loc::getMessage('DELIVERY_CALC_ERROR_TITLE_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_DELIVERY_CALC_ERROR_TEXT']))
{
	$arParams['MESS_DELIVERY_CALC_ERROR_TEXT'] = Loc::getMessage('DELIVERY_CALC_ERROR_TEXT_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_PAY_SYSTEM_PAYABLE_ERROR']))
{
	$arParams['MESS_PAY_SYSTEM_PAYABLE_ERROR'] = Loc::getMessage('PAY_SYSTEM_PAYABLE_ERROR_DEFAULT');
}

$scheme = $request->isHttps() ? 'https' : 'http';
switch (LANGUAGE_ID)
{
	case 'ru':
		$locale = 'ru-RU';
		break;
	case 'ua':
		$locale = 'ru-UA';
		break;
	case 'tk':
		$locale = 'tr-TR';
		break;
	default:
		$locale = 'en-US';
		break;
}

$this->addExternalCss('/bitrix/css/main/bootstrap.css');
$APPLICATION->SetAdditionalCSS('/bitrix/css/main/themes/' . $arParams['TEMPLATE_THEME'] . '/style.css', true);
$APPLICATION->SetAdditionalCSS($templateFolder . '/style.css', true);
$this->addExternalJs($templateFolder . '/order_ajax.js');
\Bitrix\Sale\PropertyValueCollection::initJs();
$this->addExternalJs($templateFolder . '/script.js');

if ($arParams['SHOW_PICKUP_MAP'] === 'Y' || $arParams['SHOW_MAP_IN_PROPS'] === 'Y')
{
	$this->addExternalJs($scheme . '://api-maps.yandex.ru/2.1.34/?load=package.full&lang=' . $locale);
}
?>
	<NOSCRIPT>
		<div style="color:red"><?= Loc::getMessage('SOA_NO_JS') ?></div>
	</NOSCRIPT>
<?

if (strlen($request->get('ORDER_ID')) > 0)
{
	include($server->getDocumentRoot() . $templateFolder . '/confirm.php');
} elseif ($arParams['DISABLE_BASKET_REDIRECT'] === 'Y' && $arResult['SHOW_EMPTY_BASKET'])
{
	include($server->getDocumentRoot() . $templateFolder . '/empty.php');
} else
{
	$hideDelivery = empty($arResult['DELIVERY']);
	?>

	<?//foreach($arResult['JS_DATA']['GRID']['ROWS'] as $arItem){
	?>
	<pre>
	<?//print_r($arResult);
	?>
	</pre>
	<?//}
	?>
	<!-- CONTAIN START -->
	<?
	if (IntVal($_GET['delete_cart']) > 0)
	{
		if (CModule::IncludeModule("sale"))
			CSaleBasket::Delete(IntVal($_GET['delete_cart']));

		$url = explode('?', $_SERVER['REQUEST_URI']);

		header('Location: ' . $url[0]);
	}

	?>
	<section class="checkout-section ptb-55">
		<div class="container">
			<div id="bx-soa-main-notifications">
				<div class="alert alert-danger" style="display:none"></div>
				<div data-type="informer" style="display:none"></div>
			</div>
			<div id="bx-soa-auth" class="bx-soa-section bx-soa-auth" style="display:none">
				<div class="bx-soa-section-title-container">
					<h2 class="bx-soa-section-title col-sm-9">
						<span class="bx-soa-section-title-count"></span><?= $arParams['MESS_AUTH_BLOCK_NAME'] ?>
					</h2>
				</div>
				<div class="bx-soa-section-content container-fluid"></div>
			</div>
			<form action="<?= $APPLICATION->GetCurPage(); ?>" method="POST" name="ORDER_FORM" id="bx-soa-order-form" enctype="multipart/form-data" class="main-form full">
				<input type="hidden" name="<?= $arParams['ACTION_VARIABLE'] ?>" value="saveOrderAjax">
				<input type="hidden" name="location_type" value="code">
				<input type="hidden" name="BUYER_STORE" id="BUYER_STORE" value="<?= $arResult['BUYER_STORE'] ?>">

				<div class="checkout-content mb-20">
					<div class="row">
						<div class="col-md-7">

							<?
							echo bitrix_sessid_post();

							if (strlen($arResult['PREPAY_ADIT_FIELDS']) > 0)
							{
								echo $arResult['PREPAY_ADIT_FIELDS'];
							}
							?>
							<div class="mb-20">
								<div class="row">
									<div class="col-xs-12 mb-20">
										<div class="heading-part checkout-content-heading">
											<h3 class="sub-heading">Контактные данные</h3>
										</div>
										<hr>
									</div>
									<div class="col-sm-4">
										<div class="input-box">
											<label for="name">Имя<span class="required"> *</span></label>
											<input name="ORDER_PROP_1" autocomplete="name" id="soa-property-1" type="text" required placeholder="Введите имя"
											       value="<?= $arResult['USER_VALS']['ORDER_PROP'][1] ?>">
										</div>
									</div>
									<div class="col-sm-4">
										<div class="input-box">
											<label for="phone">Телефон<span class="required"> *</span></label>
											<input type="text" name="ORDER_PROP_3" id="soa-property-3" autocomplete="tel" required placeholder="Введите телефон">
										</div>
									</div>
									<div class="col-sm-4">
										<div class="input-box">
											<label for="email">Email</label>
											<input type="email" name="ORDER_PROP_2" id="soa-property-2" autocomplete="email" placeholder="Введите Email"
											       value="<?= $arResult['USER_VALS']['ORDER_PROP'][2] ?>">
										</div>
									</div>
									<div class="col-md-12">
										<h3 class="sub-heading">Способ доставки</h3>
									</div>
									<div class="col-sm-4">
										<div class="input-box">
											<style>
												.bx-ui-sls-container {
													min-height: 32px !important;
												}
											</style>
											<label for="sity">Город<span class="required"> *</span></label>
											<!--<input type="text" id="sity" required placeholder="Введите город" value="">-->
											<? print_r($arResult['LOCATIONS'][6]['output'][0]) ?>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="input-box">
											<label for="address">Адрес доставки</label>
											<input type="text" name="ORDER_PROP_7" id="soa-property-7" placeholder="Введите адрес доставки">
										</div>
									</div>
									<div class="col-sm-4">
										<div class="input-box">
											<label for="del-variants">Варианты доставки<span class="required"> *</span></label>
											<div class="ui dropdown">
												<input class="delivery" id="" type="hidden" name="DELIVERY_ID" data-value="" data-price="">
												<div class="default text">Выберите вариант</div>
												<i class="dropdown icon"></i>
												<div class="menu">
													<? foreach ($arResult['DELIVERY'] as $arItem)
													{
														?>
														<div class="item del<?= $arItem['ID'] ?>" value="<?= $arItem['ID'] ?>"
														     data-price="<?= $arItem['PRICE'] ?>"><?= $arItem['NAME']; ?></div>
														<script>
                                                            $(document).ready(function () {
                                                                $('.del<?=$arItem['ID']?>').on('click', function () {

                                                                    var del_val = $('.del<?=$arItem['ID']?>').attr('value');
                                                                    var del_html = $('.del<?=$arItem['ID']?>').html();
                                                                    var del_price = $('.del<?=$arItem['ID']?>').attr('data-price');

                                                                    $('.delivery').attr('id', 'ID_DELIVERY_ID_' + del_val);
                                                                    $('.delivery').attr('data-value', del_html);
                                                                    $('.delivery').attr('data-price', del_price);
                                                                    $('.del-totals').html(del_price + ' <i class="fa fa-rub"></i>');

                                                                });
                                                            })
														</script>
													<?
													} ?>
												</div>
											</div>
										</div>
									</div>

									<div class="col-md-12">
										<h3 class="sub-heading">Способ оплаты</h3>
									</div>
									<div class="col-sm-4">
										<div class="input-box">
											<label>Варианты оплаты<span class="required"> *</span></label>
											<div class="ui dropdown">
												<input class="pay" type="hidden" id="" checked name="PAY_SYSTEM_ID" data-value="">
												<div class="default text">Выберите вариант</div>
												<i class="dropdown icon"></i>
												<div class="menu">
													<? foreach ($arResult['PAY_SYSTEM'] as $arItem)
													{
														?>
														<div class="item bx-soa-pp-company-checkbox" id="pay<?= $arItem['ID'] ?>"
														     value="<?= $arItem['ID'] ?>"><?= $arItem['NAME']; ?></div>
														<script>
                                                            $(document).ready(function () {
                                                                $('#pay<?=$arItem['ID']?>').on('click', function () {
                                                                    var pay_val = $('#pay<?=$arItem['ID']?>').attr('value');
                                                                    var pay_html = $('#pay<?=$arItem['ID']?>').html();

                                                                    $('.pay').attr('id', 'ID_PAY_SYSTEM_ID_' + pay_val);
                                                                    $('.pay').attr('data-value', pay_html);
                                                                });
                                                            })
														</script>
													<?
													} ?>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="input-box">
											<label for="sale">Промокод</label>
											<div class="action-input">
												<? if ($arParams["HIDE_COUPON"] != "Y"):
													$couponClass = "";
													if (array_key_exists('COUPON_VALID', $arResult))
													{
														$couponClass = ($arResult["COUPON_VALID"] == "Y") ? "good" : "bad";
													} elseif (array_key_exists('COUPON', $arResult) && strlen($arResult["COUPON"]) > 0)
													{
														$couponClass = "good";
													}
													?>
													<input type="text" id="coupon" name="COUPON" placeholder="Промокод" value="<?= $arItem['COUPON'] ?>" onchange="enterCoupon();"
													       class="<?= $couponClass ?>">
												<?
												else:?>
													&nbsp;
												<?endif; ?>
												<a href="javascript:void(0);">ок</a>
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<!--<div class="input-box">
										  <label>Скидочная карта</label>
										  <div class="ui dropdown">
											<input type="hidden" name="sale_card">
											<div class="default text">Нет</div>
											<i class="dropdown icon"></i>
											<div class="menu">
											  <div class="item" data-value="">10%</div>
											  <div class="item" data-value="">5%</div>
											  <div class="item" data-value="">Нет</div>
											</div>
										  </div>
										</div>-->
									</div>
									<div class="col-md-12">
										<h3 class="sub-heading">Комментарий к заказу</h3>
									</div>
									<div class="col-md-12">
										<textarea id="orderDescription" name="ORDER_DESCRIPTION"></textarea>
									</div>
								</div>
							</div>
							<div style="display: none;">
								<div id='bx-soa-basket-hidden' class="bx-soa-section"></div>
								<div id='bx-soa-region-hidden' class="bx-soa-section"></div>
								<div id='bx-soa-paysystem-hidden' class="bx-soa-section"></div>
								<div id='bx-soa-delivery-hidden' class="bx-soa-section"></div>
								<div id='bx-soa-pickup-hidden' class="bx-soa-section"></div>
								<div id="bx-soa-properties-hidden" class="bx-soa-section"></div>
								<div id="bx-soa-auth-hidden" class="bx-soa-section">
									<div class="bx-soa-section-content container-fluid reg"></div>
								</div>
							</div>

						</div>
						<div class="col-md-5">
							<div class="row">
								<div class="col-md-12 mb-20">
									<div class="heading-part checkout-content-heading">
										<h3>Ваш заказ</h3>
									</div>
									<hr>
								</div>
								<div class="col-md-12">
									<div class="checkout-table">
										<table class="table">
											<thead>
											<tr>
												<th></th>
												<th>Название</th>
												<th>Цена</th>
												<th>Количество</th>
												<th></th>
											</tr>
											</thead>
											<tbody>
											<? $itogo = 0; ?>
											<? $itogo1 = 0; ?>
											<? foreach ($arResult['JS_DATA']['GRID']['ROWS'] as $arItem)
											{
												?>
												<tr>
													<td>
														<a href="<?= $arItem['data']['DETAIL_PAGE_URL']; ?>">
															<div class="product-image">
																<?= $arItem['columns']['PREVIEW_PICTURE']; ?>
															</div>
														</a>
													</td>
													<td>
														<div class="product-title"><a href="<?= $arItem['data']['DETAIL_PAGE_URL']; ?>"><?= $arItem['data']['NAME']; ?></a></div>
													</td>
													<td class="price">
														<span><?= round($arItem['data']['PRICE']); ?></span> <i class="fa fa-rub">
															<? $itogo = $itogo + ($arItem['data']['PRICE'] * $arItem['columns']['QUANTITY']); ?>
													</td>
													<td>
														<div class="input-box">
															<span><?= $arItem['columns']['QUANTITY']; ?></span>
															<? $itogo1 = $itogo1 + $arItem['columns']['QUANTITY']; ?>
														</div>
													</td>
													<td>
														<i name="DELETE_<?= $arItem["ID"] ?>" id="DELETE_<?= $arItem['id'] ?>"
														   onclick="location.href='?delete_cart=<?= $arItem["id"] ?>'; return false;" title="Удалить товар из корзины"
														   data-id="<?= $arItem['id'] ?>" class="fa fa-trash-o cart-remove-item"></i>
													</td>
												</tr>
											<?
											} ?>
											</tbody>
											<tfoot>
											<tr>
												<td>
													<div class="cart-totals align-center total">Доставка:</div>
												</td>
												<td></td>
												<td>
													<div class="cart-totals del-totals"></div>
												</td>
												<td></td>
												<td></td>
											</tr>
											<tr>
												<td>
													<div class="cart-totals align-center total">Итого:</div>
												</td>
												<td></td>
												<td>
													<div class="cart-totals c-t"><?= round($itogo); ?> <i class="fa fa-rub"></div>
												</td>
												<td>
													<div class="cart-totals"><?= $itogo1; ?> шт</div>
												</td>
												<td></td>
											</tr>
											</tfoot>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div><a href="/catalog/" class="button basic grey">Вернуться в магазин</a></div>
					</div>
					<div class="col-sm-6">
						<div class="right-side float-none-xs">
							<button data-save-button="true" class="button green">Оформить заказ</button>
						</div>
					</div>
				</div>

			</form>
		</div>
	</section>
	<!-- CONTAINER END -->
	<div id="bx-soa-saved-files" style="display:none"></div>
	<div id="bx-soa-soc-auth-services" style="display:none">
		<?
		$arServices = false;
		$arResult['ALLOW_SOCSERV_AUTHORIZATION'] = Main\Config\Option::get('main', 'allow_socserv_authorization', 'Y') != 'N' ? 'Y' : 'N';
		$arResult['FOR_INTRANET'] = false;

		if (Main\ModuleManager::isModuleInstalled('intranet') || Main\ModuleManager::isModuleInstalled('rest'))
			$arResult['FOR_INTRANET'] = true;

		if (Main\Loader::includeModule('socialservices') && $arResult['ALLOW_SOCSERV_AUTHORIZATION'] === 'Y')
		{
			$oAuthManager = new CSocServAuthManager();
			$arServices = $oAuthManager->GetActiveAuthServices(array(
				'BACKURL' => $this->arParams['~CURRENT_PAGE'],
				'FOR_INTRANET' => $arResult['FOR_INTRANET'],
			));

			if (!empty($arServices))
			{
				$APPLICATION->IncludeComponent(
					'bitrix:socserv.auth.form',
					'flat',
					array(
						'AUTH_SERVICES' => $arServices,
						'AUTH_URL' => $arParams['~CURRENT_PAGE'],
						'POST' => $arResult['POST'],
					),
					$component,
					array('HIDE_ICONS' => 'Y')
				);
			}
		}
		?>
	</div>

	<div style="display: none">
		<?
		$APPLICATION->IncludeComponent(
			'bitrix:sale.location.selector.steps',
			'.default',
			array(),
			false
		);
		$APPLICATION->IncludeComponent(
			'bitrix:sale.location.selector.search',
			'.default',
			array(),
			false
		);
		?>
	</div>
	<?
	$signer = new Main\Security\Sign\Signer;
	$signedParams = $signer->sign(base64_encode(serialize($arParams)), 'sale.order.ajax');
	$messages = Loc::loadLanguageFile(__FILE__);
	?>
	<script>
        BX.message(<?=CUtil::PhpToJSObject($messages)?>);
        BX.Sale.OrderAjaxComponent.init({
            result: <?=CUtil::PhpToJSObject($arResult['JS_DATA'])?>,
            locations: <?=CUtil::PhpToJSObject($arResult['LOCATIONS'])?>,
            params: <?=CUtil::PhpToJSObject($arParams)?>,
            signedParamsString: '<?=CUtil::JSEscape($signedParams)?>',
            siteID: '<?=CUtil::JSEscape($component->getSiteId())?>',
            ajaxUrl: '<?=CUtil::JSEscape($component->getPath() . '/ajax.php')?>',
            templateFolder: '<?=CUtil::JSEscape($templateFolder)?>',
            propertyValidation: true,
            showWarnings: true,
            pickUpMap: {
                defaultMapPosition: {
                    lat: 55.76,
                    lon: 37.64,
                    zoom: 7
                },
                secureGeoLocation: false,
                geoLocationMaxTime: 5000,
                minToShowNearestBlock: 3,
                nearestPickUpsToShow: 3
            },
            propertyMap: {
                defaultMapPosition: {
                    lat: 55.76,
                    lon: 37.64,
                    zoom: 7
                }
            },
            orderBlockId: 'bx-soa-order',
            authBlockId: 'bx-soa-auth',
            basketBlockId: 'bx-soa-basket',
            regionBlockId: 'bx-soa-region',
            paySystemBlockId: 'bx-soa-paysystem',
            deliveryBlockId: 'bx-soa-delivery',
            pickUpBlockId: 'bx-soa-pickup',
            propsBlockId: 'bx-soa-properties',
            totalBlockId: 'bx-soa-total'
        });
	</script>
	<script>
		<?
		// spike: for children of cities we place this prompt
		$city = \Bitrix\Sale\Location\TypeTable::getList(array('filter' => array('=CODE' => 'CITY'), 'select' => array('ID')))->fetch();
		?>
        BX.saleOrderAjax.init(<?=CUtil::PhpToJSObject(array(
			'source' => $component->getPath() . '/get.php',
			'cityTypeId' => intval($city['ID']),
			'messages' => array(
				'otherLocation' => '--- ' . Loc::getMessage('SOA_OTHER_LOCATION'),
				'moreInfoLocation' => '--- ' . Loc::getMessage('SOA_NOT_SELECTED_ALT'), // spike: for children of cities we place this prompt
				'notFoundPrompt' => '<div class="-bx-popup-special-prompt">' . Loc::getMessage('SOA_LOCATION_NOT_FOUND') . '.<br />' . Loc::getMessage('SOA_LOCATION_NOT_FOUND_PROMPT', array(
						'#ANCHOR#' => '<a href="javascript:void(0)" class="-bx-popup-set-mode-add-loc">',
						'#ANCHOR_END#' => '</a>'
					)) . '</div>'
			)
		))?>);
	</script>
	<script>
		<?
		if ($arParams['SHOW_PICKUP_MAP'] === 'Y' || $arParams['SHOW_MAP_IN_PROPS'] === 'Y')
		{
		?>
        (function bx_ymaps_waiter() {
            if (typeof ymaps !== 'undefined')
                ymaps.ready(BX.proxy(BX.Sale.OrderAjaxComponent.initMaps, BX.Sale.OrderAjaxComponent));
            else
                setTimeout(bx_ymaps_waiter, 100);
        })();
		<?
		}

		if ($arParams['USE_YM_GOALS'] === 'Y')
		{
		?>
        (function bx_counter_waiter(i) {
            i = i || 0;
            if (i > 50)
                return;

            if (typeof window['yaCounter<?=$arParams['YM_GOALS_COUNTER']?>'] !== 'undefined')
                BX.Sale.OrderAjaxComponent.reachGoal('initialization');
            else
                setTimeout(function () {
                    bx_counter_waiter(++i)
                }, 100);
        })();
		<?
		}
		?>
	</script>
	<?
}
?>