<section class="checkout-section ptb-95 mobile-padding">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-4">
				<div class="sv-account mb-xs-30">
					<div class="tab-title-bg">
						<div class="heading-part">
							<div class="sv-header--h3"><i class="fa fa-user" aria-hidden="true"></i><?= $arUser[NAME] ?> <?= $arUser[LAST_NAME] ?></div>
						</div>
					</div>
					<hr class="mb-20">
					<div class=" account-tab-inner">
						<ul class="account-tab-stap">
							<li id="step1"><a href="/personal/">Общая информация<i class="fa fa-angle-right "></i></a></li>
							<li><a href="/compare/?action=COMPARE">Сравнение <i class="fa fa-angle-right "></i></a></li>
							<li><a href="/favorites/">Избранное <i class="fa fa-angle-right "></i></a></li>
							<li><a href="/personal/cart/">Моя корзина <i class="fa fa-angle-right "></i></a></li>
							<li id="step2"><a href="/personal/orders/?filter_history=Y">История заказов<i class="fa fa-angle-right "></i> </a></li>
							<li id="step3"><a href="/personal/private/?pwd=Y">Изменить пароль<i class="fa fa-angle-right "></i> </a></li>
							<li id="step4"><a href="/personal/private/?info=Y">Личная информация<i class="fa fa-angle-right "></i> </a></li>
							<div class="sv-divider"></div>
							<li><a href="/?logout=yes" class="sv-exit__button right-side">Выйти<i class="fa fa-sign-out" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-9 col-sm-8 ">