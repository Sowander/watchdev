<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

$availablePages = array();
$rsUser = CUser::GetByID($USER->getId());
$arUser = $rsUser->Fetch();
$availablePages[] = array(
	"path" => $arResult['PATH_TO_ORDERS'],
	"name" => Loc::getMessage("SPS_ORDER_PAGE_NAME"),
	"icon" => '<i class="fa fa-calculator"></i>'
);

$delimeter = ($arParams['SEF_MODE'] === 'Y') ? "?" : "&";
$availablePages[] = array(
	"path" => $arResult['PATH_TO_ORDERS'] . $delimeter . "filter_history=Y",
	"name" => Loc::getMessage("SPS_ORDER_PAGE_HISTORY"),
	"icon" => '<i class="fa fa-list-alt"></i>'
);


$availablePages[] = array(
	"path" => $arResult['PATH_TO_PRIVATE'],
	"name" => Loc::getMessage("SPS_PERSONAL_PAGE_NAME"),
	"icon" => '<i class="fa fa-user-secret"></i>'
);


$availablePages[] = array(
	"path" => $arParams['PATH_TO_BASKET'],
	"name" => Loc::getMessage("SPS_BASKET_PAGE_NAME"),
	"icon" => '<i class="fa fa-shopping-cart"></i>'
);


if (strlen($arParams["MAIN_CHAIN_NAME"]) > 0)
{
	$APPLICATION->AddChainItem(htmlspecialcharsbx($arParams["MAIN_CHAIN_NAME"]), $arResult['SEF_FOLDER']);
}

?>
<? $APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => $templateFolder . '/menu_start.php'
	)
);
?>

	<div id="data-step1" class="account-content " data-temp="tabdata ">
		<div class="row ">
			<div class="col-xs-12 ">
				<div class="sv-header--h3">Общая информация</div>
			</div>
		</div>
		<hr class="mb-20">
		<div class="row mb-30 ">
			<div class="col-xs-12 ">
				<div class="heading-part ">
					<h4 class="sub-heading ">Мы рады вам, <?= $arUser['NAME'] ?> <?= $arUser['LAST_NAME'] ?></h4>
				</div>
				<p> С помощью меню слева вы можете управлять вашим аккаунтом, отслеживать статус заказов, смотреть избранные товары и сравнивать их. </p>
			</div>
		</div>
		<div class="row mb-30 ">
			<div class="col-sm-6 ">
				<div class="sv-card ">
					<div class="sv-header--h4 "><?= $arUser['NAME'] ?> <?= $arUser['LAST_NAME'] ?>
					</div>
					<div class="sv-card-lists ">
						<div class="sv-card-lists__list ">
							<i class="fa fa-street-view " aria-hidden="true "></i>
							<p> г. Мытищи, Улица Мира 51, ТЦ "Июнь ", 1 этаж, зона Англии, перед центральным лифтом</p>
						</div>
						<div class="sv-card-lists__list "><i class="fa fa-envelope-o " aria-hidden="true ">
							</i>
							<p><?= $arUser['EMAIL'] ?></p>
						</div>
						<div class="sv-card-lists__list "><i class="fa fa-phone " aria-hidden="true "></i>
							</i>
							<p><?= $arUser['PERSONAL_PHONE'] ?></p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6 ">
				<div class="sv-header--h3 sv-header--mobile ">Акции, скидки магазина</div>
				<div class="sv-last-news">


					<? $APPLICATION->IncludeComponent(
						"bitrix:news.list",
						"actions_list",
						array(
							"ACTIVE_DATE_FORMAT" => "d.m.Y",
							"ADD_SECTIONS_CHAIN" => "Y",
							"AJAX_MODE" => "N",
							"AJAX_OPTION_ADDITIONAL" => "",
							"AJAX_OPTION_HISTORY" => "N",
							"AJAX_OPTION_JUMP" => "N",
							"AJAX_OPTION_STYLE" => "Y",
							"CACHE_FILTER" => "N",
							"CACHE_GROUPS" => "Y",
							"CACHE_TIME" => "36000000",
							"CACHE_TYPE" => "A",
							"CHECK_DATES" => "Y",
							"DETAIL_URL" => "",
							"DISPLAY_BOTTOM_PAGER" => "Y",
							"DISPLAY_DATE" => "Y",
							"DISPLAY_NAME" => "Y",
							"DISPLAY_PICTURE" => "Y",
							"DISPLAY_PREVIEW_TEXT" => "Y",
							"DISPLAY_TOP_PAGER" => "N",
							"FIELD_CODE" => array(
								0 => "ID",
								1 => "CODE",
								2 => "XML_ID",
								3 => "NAME",
								4 => "TAGS",
								5 => "SORT",
								6 => "PREVIEW_TEXT",
								7 => "",
							),
							"FILTER_NAME" => "",
							"HIDE_LINK_WHEN_NO_DETAIL" => "N",
							"IBLOCK_ID" => "11",
							"IBLOCK_TYPE" => "news",
							"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
							"INCLUDE_SUBSECTIONS" => "Y",
							"MESSAGE_404" => "",
							"NEWS_COUNT" => "20",
							"PAGER_BASE_LINK_ENABLE" => "N",
							"PAGER_DESC_NUMBERING" => "N",
							"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
							"PAGER_SHOW_ALL" => "N",
							"PAGER_SHOW_ALWAYS" => "N",
							"PAGER_TEMPLATE" => ".default",
							"PAGER_TITLE" => "Новости",
							"PARENT_SECTION" => "",
							"PARENT_SECTION_CODE" => "",
							"PREVIEW_TRUNCATE_LEN" => "",
							"PROPERTY_CODE" => array(
								0 => "to",
								1 => "from",
								2 => "",
							),
							"SET_BROWSER_TITLE" => "Y",
							"SET_LAST_MODIFIED" => "N",
							"SET_META_DESCRIPTION" => "Y",
							"SET_META_KEYWORDS" => "Y",
							"SET_STATUS_404" => "N",
							"SET_TITLE" => "Y",
							"SHOW_404" => "N",
							"SORT_BY1" => "ACTIVE_FROM",
							"SORT_BY2" => "SORT",
							"SORT_ORDER1" => "DESC",
							"SORT_ORDER2" => "ASC",
							"STRICT_SECTION_CHECK" => "N",
							"COMPONENT_TEMPLATE" => "actions_list"
						),
						false
					); ?>


				</div>
				<a href="/actions/" class="right-side link--blue">Подробнее</a>
			</div>
		</div>
		<hr class="mb-20">
		<div class="row mb-30 ">
			<div class="col-sm-6 ">

				<? $APPLICATION->IncludeComponent("salvador:main.feedback", "personal", Array(
					"USE_CAPTCHA" => "Y",    // Использовать защиту от автоматических сообщений (CAPTCHA) для неавторизованных пользователей
					"OK_TEXT" => "Спасибо, ваше сообщение принято.",    // Сообщение, выводимое пользователю после отправки
					"EMAIL_TO" => "kulbachenko@gmail.com",    // E-mail, на который будет отправлено письмо
					"REQUIRED_FIELDS" => array(    // Обязательные поля для заполнения
						0 => "PHONE",
						1 => "NAME",
					),
					"EVENT_MESSAGE_ID" => array(    // Почтовые шаблоны для отправки письма
						0 => "7",
					),
					"COMPONENT_TEMPLATE" => "template"
				),
					false
				); ?>


			</div>
			<div class="col-sm-6 ">
				<div class="attention">
					<p class="mb-10">Наши менеджеры с удовольствием помогут Вам с выбором часов:</p>
					<li> Предоставят бесплатную консультацию</li>
					<li> Найдём самые красивые часы за разумные деньги</li>
					<li> Оперативно и грамотно обработают заказ!</li>
					<li> Подобрать подарок - не проблема.</li>
				</div>
			</div>
		</div>
	</div>

<? $APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => $templateFolder . '/menu_end.php'
	)
);
?>