<?
$MESS ['PROFILE_DATA_SAVED'] = "Изменения сохранены";
$MESS ['NAME'] = "Имя";
$MESS ['LAST_NAME'] = "Фамилия";
$MESS ['SECOND_NAME'] = "Отчество";
$MESS ['NEW_PASSWORD_CONFIRM'] = "Подтверждение пароля";
$MESS ['NEW_PASSWORD_REQ'] = "Новый пароль";
$MESS ['MAIN_SAVEI'] = "Сохранить изменения";
$MESS ['MAIN_PSWD'] = "Пароль";
$MESS ['LEGEND_PROFILE'] = "Личная информация";
$MESS ['LEGEND_PROFILE_PWD'] = "Изменить пароль";
$MESS ['USER_MALE'] = "Муж";
$MESS ['USER_FEMALE'] = "Жен";
$MESS ['ATTENSION'] = "Здесь Вы можете изменить пароль для своей идентификации в salvador-watch.ru Новый пароль должен содержать не менее 6 и не более 30 символов: букв, цифр и знаков препинания.";
?>