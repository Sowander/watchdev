<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?><?
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
if ($arResult['DATA_SAVED'] == 'Y')
	echo ShowNote(Loc::getMessage('PROFILE_DATA_SAVED'));
?>
<div id="data-step4" class="account-content ">
	<? if ($_REQUEST['info'] == 'Y'): ?>
		<div class="row ">
			<div class="col-xs-12 ">
				<div class="sv-header--h3"><?= Loc::getMessage("LEGEND_PROFILE") ?></div>
			</div>
		</div>
		<hr class="mb-20">
		<form method="post" id="regForm" name="form1" action="" enctype="multipart/form-data" class="main-form full">
			<?= $arResult["BX_SESSION_CHECK"] ?>
			<input type="hidden" name="lang" value="<?= LANG ?>">
			<input type="hidden" name="ID" value="<?= $arResult["ID"]?>">
			<input type="hidden" name="LOGIN" value="<?= $arResult["arUser"]["LOGIN"] ?>">
			<input type="hidden" name="EMAIL" value="<?= $arResult["arUser"]["EMAIL"] ?>">
			<div class="row">
				<div class="col-xs-4">
					<div class="input-box">
						<label><?= Loc::getMessage('NAME') ?></label>
						<input type="text" required name="NAME" maxlength="50" value="<?= $arResult["arUser"]["NAME"] ?>">
					</div>
				</div>
				<div class="col-xs-4">
					<div class="input-box">
						<label><?= Loc::getMessage('LAST_NAME') ?></label>
						<input type="text" name="LAST_NAME" maxlength="50" value="<?= $arResult["arUser"]["LAST_NAME"] ?>">
					</div>
				</div>
				<div class="col-xs-4">
					<div class="input-box">
						<label><?= Loc::getMessage('SECOND_NAME') ?></label>
						<input type="text" name="SECOND_NAME" maxlength="50" value="<?= $arResult["arUser"]["SECOND_NAME"] ?>">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-4 ">
					<div class="input-box ">
						<label><?= Loc::getMessage('EMAIL') ?></label>
						<input type="email" name="EMAIL" maxlength="50" value="<? echo $arResult["arUser"]["EMAIL"] ?>">
					</div>
				</div>
				<div class="col-xs-4 ">
					<div class="input-box ">
						<label><?= Loc::getMessage('USER_PHONE') ?></label>
						<input type="text" name="PERSONAL_PHONE" maxlength="255" value="<?= $arResult["arUser"]["PERSONAL_PHONE"] ?>">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-4">
					<label>Пол</label>
					<ul class="sidebar-checking">
						<li>

							<input type="radio" id="PERSONAL_GENDER_M" name="PERSONAL_GENDER" value="M" <?= $arResult["arUser"]["PERSONAL_GENDER"] == 'M' ? 'checked' : ''; ?>>
							<label class="button grey basic" for="PERSONAL_GENDER_M"><?= Loc::getMessage("USER_MALE") ?></label>

						</li>
						<li>

							<input type="radio" id="PERSONAL_GENDER_F" name="PERSONAL_GENDER" value="F" <?= $arResult["arUser"]["PERSONAL_GENDER"] == 'F' ? 'checked' : ''; ?>>
							<label class="button grey basic" for="PERSONAL_GENDER_F"><?= Loc::getMessage("USER_FEMALE") ?></label>
						</li>
					</ul>
				</div>
			</div>
		</form>
	<? endif; ?>
	<? if ($_REQUEST['pwd'] == 'Y'): ?>
		<div class="row ">
			<div class="col-xs-12 ">
				<div class="sv-header--h3"><?= Loc::getMessage("LEGEND_PROFILE_PWD") ?></div>
			</div>
		</div>
		<hr class="mb-20">
		<div class="row mb-30">
			<div class="col-sm-6">
				<form method="post" id="regForm" name="form1" action="" enctype="multipart/form-data" class="main-form full main-form__center ">
					<?= $arResult["BX_SESSION_CHECK"] ?>
					<input type="hidden" name="lang" value="<?= LANG ?>">
					<input type="hidden" name="ID" value=<?= $arResult["ID"] ?>>
					<input type="hidden" name="LOGIN" value=<?= $arResult["arUser"]["LOGIN"] ?>>
					<input type="hidden" name="EMAIL" value=<?= $arResult["arUser"]["EMAIL"] ?>>
					<div class="input-box ">
						<label><?= Loc::getMessage('NEW_PASSWORD_REQ') ?></label>
						<input type="password" name="NEW_PASSWORD" maxlength="50" value="" placeholder="Введите новый пароль" required autocomplete="off">

						<label><?= Loc::getMessage('NEW_PASSWORD_CONFIRM') ?></label>
						<input type="password" name="NEW_PASSWORD_CONFIRM" maxlength="50" value="" placeholder="Повторите пароль" required autocomplete="off">
					</div>
				</form>
			</div>
			<div class="col-sm-6">
				<div class="attention">
					<p>
						<?= Loc::getMessage('ATTENSION'); ?>
					</p>
				</div>
			</div>
		</div>
	<? endif; ?>


	<?= ShowError($arResult["strProfileError"]); ?>
	<div class="row mt-20">
		<div class="col-xs-12">
			<?
			if ($arResult["SOCSERV_ENABLED"])
			{
				$APPLICATION->IncludeComponent("bitrix:socserv.auth.split", "glab", array(
					"SHOW_PROFILES" => "Y",
					"ALLOW_DELETE" => "Y"
				),
					false
				);
			}
			?>
		</div>
	</div>
	<hr class="mb-20 mt-20">
	<div class="row ">
		<div class="col-xs-12 ">
			<input form="regForm" type="submit" name="save" class="button green right-side" value="<?= Loc::getMessage('MAIN_SAVEI') ?>">
		</div>
	</div>

</div>