<ul>
	<?
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
	use Bitrix\Main\Localization\Loc;

	Loc::loadMessages(__FILE__);
	$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"] . "&amp;" : "");
	$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?" . $arResult["NavQueryString"] : "");

	if ($arResult["NavPageCount"] > 1)
	{
		if ($arResult["bDescPageNumbering"] === true):
			$bFirst = true;
			if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):
				if ($arResult["bSavePage"]):
					?>
					<li>
						<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>"><?= Loc::getMessage("glabnav_prev") ?></a>
					</li>
					<?
				else:
					if ($arResult["NavPageCount"] == ($arResult["NavPageNomer"] + 1)):
						?>
						<li><a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"><?= Loc::getMessage("glabnav_prev") ?></a></li>
						<?
					else:
						?>
						<li>
							<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>"><?=
								Loc::getMessage("glabnav_prev") ?></a>
						</li>
						<?
					endif;
				endif;
				?>
				<?

				if ($arResult["nStartPage"] < $arResult["NavPageCount"]):
					$bFirst = false;
					if ($arResult["bSavePage"]):
						?>
						<li>
							<a class="blog-page-first" href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["NavPageCount"]
							?>">1</a>
						</li>
						<?
					else:
						?>
						<li>
							<a class="blog-page-first" href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">1</a>
						</li>
						<?
					endif;
				endif;
			endif;
			do
			{
				$NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1;

				if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):
					?>
					<li class="active">
						<span class="<?= ($bFirst ? "blog-page-first " : "") ?>blog-page-current"><?= $NavRecordGroupPrint ?></span>
					</li>
					<?
				elseif ($arResult["nStartPage"] == $arResult["NavPageCount"] && $arResult["bSavePage"] == false):
					?>
					<li>
						<a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>" class="<?= ($bFirst ? "blog-page-first" : "") ?>"><?= $NavRecordGroupPrint ?></a>
					</li>
					<?
				else:
					?>
					<li>
						<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>"<?
						?> class="<?= ($bFirst ? "blog-page-first" : "") ?>"><?= $NavRecordGroupPrint ?></a>
					</li>

					<?
				endif;


				$arResult["nStartPage"]--;
				$bFirst = false;
			} while ($arResult["nStartPage"] >= $arResult["nEndPage"]);

			if ($arResult["NavPageNomer"] > 1):
				if ($arResult["nEndPage"] > 1):
					?>
					<li>
					<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=1"><?= $arResult["NavPageCount"] ?></a>
					</li>
					<?
				endif;

				?>
				<li>
					<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>"><?= Loc::getMessage("glabnav_next") ?></a>
				</li>
				<?
			endif;

		else:
			$bFirst = true;

			if ($arResult["NavPageNomer"] > 1):
				if ($arResult["bSavePage"]):
					?>
					<li>
						<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>"><?= Loc::getMessage("glabnav_prev") ?></a>
					</li>
					<?
				else:
					if ($arResult["NavPageNomer"] > 2):
						?>
						<li>
							<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>"><?=
								Loc::getMessage("glabnav_prev") ?></a>
						</li>
						<?
					else:
						?>
						<li>
							<a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"><?= Loc::getMessage("glabnav_prev") ?></a>
						</li>
						<?
					endif;

				endif;


				if ($arResult["nStartPage"] > 1):
					$bFirst = false;
					if ($arResult["bSavePage"]):
						?>
						<li>
							<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=1">1</a>
						</li>
						<?
					else:
						?>
						<li>
							<a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">1</a>
						</li>
						<?
					endif;
				endif;
			endif;

			do
			{
				if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):
					?>
					<li class="active">
						<span class="<?= ($bFirst ? "blog-page-first " : "") ?>blog-page-current"><?= $arResult["nStartPage"] ?></span>
					</li>
					<?
				elseif ($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):
					?>
					<li>
						<a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>" class="<?= ($bFirst ? "blog-page-first" : "") ?>"><?= $arResult["nStartPage"] ?></a>
					</li>
					<?
				else:
					?>
					<li>
					<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>" class="<?= ($bFirst ? "blog-page-first" : "") ?>"><?= $arResult["nStartPage"] ?></a>
					</li>
					<?
				endif;

				$arResult["nStartPage"]++;
				$bFirst = false;
			} while ($arResult["nStartPage"] <= $arResult["nEndPage"]);

			if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):
				if ($arResult["nEndPage"] < $arResult["NavPageCount"]):
					?>
					<li>
						<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["NavPageCount"] ?>"><?= $arResult["NavPageCount"] ?></a>
					</li>
					<?
				endif;
				?>
				<li>
					<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>"><?= Loc::getMessage("glabnav_next") ?></a>
				</li>
				<?
			endif;
		endif;

		if ($arResult["bShowAll"]):
			if ($arResult["NavShowAll"]):
			else:
				?>
				<li>
					<a class="blog-page-all" href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>SHOWALL_<?= $arResult["NavNum"] ?>=1"><?= Loc::getMessage("nav_all") ?></a>
				</li>
				<?
			endif;
		endif
		?>

		<?
	}
	?>
</ul>
