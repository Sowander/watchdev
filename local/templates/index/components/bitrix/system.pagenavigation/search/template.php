<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"] . "&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?" . $arResult["NavQueryString"] : "");
?>

<div class="row">
	<div class="col-xs-12">
		<div class="pagination-bar">
			<ul>
				<? if ($arResult["NavPageNomer"] > 1): ?>

					<? if ($arResult["bSavePage"]): ?>
						<li>
							<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>">
								<i class="fa fa-angle-left"></i>
							</a>
						</li>

					<? else: ?>
						<li class="active"><a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"><?= GetMessage("nav_begin") ?></a></li>

						<? if ($arResult["NavPageNomer"] > 2): ?>
							<li>
								<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>"><?= GetMessage("nav_prev") ?></a>
							</li>
						<? else: ?>
							<li><a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"><?= GetMessage("nav_prev") ?></a></li>
						<? endif ?>

					<? endif ?>

				<? else: ?>
					<li><a href="javascript:void(0);"><i class="fa fa-angle-left"></i></a></li>
				<? endif ?>

				<? while ($arResult["nStartPage"] <= $arResult["nEndPage"]): ?>

					<? if ($arResult["nStartPage"] == $arResult["NavPageNomer"]): ?>
						<li class="active"><span><?= $arResult["nStartPage"] ?></span></li>
					<? elseif ($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false): ?>
						<li><a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"><?= $arResult["nStartPage"] ?></a></li>
					<? else: ?>
						<li>
							<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>"><?= $arResult["nStartPage"] ?></a>
						</li>
					<? endif ?>
					<? $arResult["nStartPage"]++ ?>
				<? endwhile ?>
				<? if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]): ?>
					<li><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>"><i
									class="fa fa-angle-right"></i></a></li>
				<? else: ?>
					<li><a href="javascript:void(0);"><i class="fa fa-angle-right"></i></a></li>
				<? endif ?>
			</ul>
		</div>
	</div>
</div>