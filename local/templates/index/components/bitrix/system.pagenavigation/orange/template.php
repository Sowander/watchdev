<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"] . "&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?" . $arResult["NavQueryString"] : "");
?>

<ul>

	<? if ($arResult["bDescPageNumbering"] === true): ?>


	<? else: ?>


		<? if ($arResult["NavPageNomer"] > 1): ?>

			<? if ($arResult["bSavePage"]): ?>

				<li><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>"><i
								class="fa fa-angle-left"></i></a></li>

			<? else: ?>

				<? if ($arResult["NavPageNomer"] > 2): ?>
					<li><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>"><i
									class="fa fa-angle-left"></i></a></li>
				<? else: ?>
					<li><a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"><i class="fa fa-angle-left"></i></a></li>
				<? endif ?>

			<? endif ?>
		<? endif ?>

		<? while ($arResult["nStartPage"] <= $arResult["nEndPage"]): ?>

			<? if ($arResult["nStartPage"] == $arResult["NavPageNomer"]): ?>
				<li><span class="nav-current-page">&nbsp;<?= $arResult["nStartPage"] ?>&nbsp;</span></li>
			<? elseif ($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false): ?>
				<li><a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"><?= $arResult["nStartPage"] ?></a></li>
			<? else: ?>
				<li>
					<a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>"><?= $arResult["nStartPage"] ?></a>
				</li>
			<? endif ?>
			<? $arResult["nStartPage"]++ ?>
		<? endwhile ?>


		<? if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]): ?>
			<li><a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>"><i
							class="fa fa-angle-right"></i></a></li>

		<? endif ?>

	<? endif ?>

</ul>