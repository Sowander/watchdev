<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

//one css for all system.auth.* forms
$APPLICATION->SetAdditionalCSS("/bitrix/css/main/system.auth/flat/style.css");
?>
<section class="checkout-section ptb-95 mobile-padding">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12">

						<?
						if (!empty($arParams["~AUTH_RESULT"])):
							$text = str_replace(array("<br>", "<br />"), "\n", $arParams["~AUTH_RESULT"]["MESSAGE"]);
							?>
							<div class="alert <?= ($arParams["~AUTH_RESULT"]["TYPE"] == "OK" ? "alert-success" : "alert-danger") ?>"><?= nl2br(htmlspecialcharsbx($text)) ?></div>
						<? endif ?>

						<? if ($arResult["USE_EMAIL_CONFIRMATION"] === "Y" && is_array($arParams["AUTH_RESULT"]) && $arParams["AUTH_RESULT"]["TYPE"] === "OK"): ?>
							<div class="alert alert-success"><? echo GetMessage("AUTH_EMAIL_SENT") ?></div>
						<? else: ?>

						<? if ($arResult["USE_EMAIL_CONFIRMATION"] === "Y"): ?>
							<div class="alert alert-warning"><? echo GetMessage("AUTH_EMAIL_WILL_BE_SENT") ?></div>
						<? endif ?>

							<!--noindex-->
							<form class="main-form full main-form__center" method="post" action="<?= $arResult["AUTH_URL"] ?>" name="bform" enctype="multipart/form-data">
								<div class="row">
									<div class="col-xs-12 mb-20">
										<div class="heading-part heading-bg">
											<h2 class="heading">Регистрация</h2>
										</div>
									</div>
									<? if ($arResult["BACKURL"] <> ''): ?>
										<input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
									<? endif ?>
									<input type="hidden" name="AUTH_FORM" value="Y"/>
									<input type="hidden" name="TYPE" value="REGISTRATION"/>

									<!--<div class="bx-authform-formgroup-container">
							<div class="bx-authform-label-container"><? //=GetMessage("AUTH_NAME")?></div>
							<div class="bx-authform-input-container">
								<input type="text" name="USER_NAME" maxlength="255" value="<? //=$arResult["USER_NAME"]?>" />
							</div>
						</div>-->
									<div class="col-xs-12">
										<div class="input-box">
											<label for="f-name">Имя</label>
											<input type="text" id="f-name" required placeholder="Введите имя" name="USER_NAME" maxlength="255"
											       value="<?= $arResult["USER_NAME"] ?>">
										</div>
									</div>

									<!--<div class="bx-authform-formgroup-container">
							<div class="bx-authform-label-container"><? //=GetMessage("AUTH_LAST_NAME")?></div>
							<div class="bx-authform-input-container">
								<input type="text" name="USER_LAST_NAME" maxlength="255" value="<?= $arResult["USER_LAST_NAME"] ?>" />
							</div>
						</div>-->

									<!--<div class="bx-authform-formgroup-container">
							<div class="bx-authform-label-container"><span class="bx-authform-starrequired">*</span><? //=GetMessage("AUTH_LOGIN_MIN")?></div>
							<div class="bx-authform-input-container">
								<input type="text" name="USER_LOGIN" maxlength="255" value="<? //=$arResult["USER_LOGIN"]?>" />
							</div>
						</div>-->

									<div class="col-xs-12">
										<div class="input-box">
											<label for="login-email">Email</label>
											<input id="login-email" type="email" required placeholder="Введите email" name="USER_LOGIN" maxlength="255"
											       value="<?= $arResult["USER_LOGIN"] ?>">
										</div>
									</div>

									<!--<div class="bx-authform-formgroup-container">
							<div class="bx-authform-label-container"><span class="bx-authform-starrequired">*</span><? //=GetMessage("AUTH_PASSWORD_REQ")?></div>
							<div class="bx-authform-input-container">
				<? //if($arResult["SECURE_AUTH"]):?>
								<div class="bx-authform-psw-protected" id="bx_auth_secure" style="display:none"><div class="bx-authform-psw-protected-desc"><span></span><? //echo GetMessage("AUTH_SECURE_NOTE")?></div></div>

				<script type="text/javascript">
				document.getElementById('bx_auth_secure').style.display = '';
				</script>
				<? //endif?>
								<input type="password" name="USER_PASSWORD" maxlength="255" value="<? //=$arResult["USER_PASSWORD"]?>" autocomplete="off" />
							</div>
						</div>-->

									<div class="col-xs-12">
										<div class="input-box">
											<label for="login-pass">Пароль</label>
											<? if ($arResult["SECURE_AUTH"]): ?>
												<div class="bx-authform-psw-protected" id="bx_auth_secure" style="display:none">
													<div class="bx-authform-psw-protected-desc"><span></span><? echo GetMessage("AUTH_SECURE_NOTE") ?></div>
												</div>
												<script type="text/javascript">
                                                    document.getElementById('bx_auth_secure').style.display = '';
												</script>
											<? endif ?>
											<input id="login-pass" type="password" required placeholder="Введите пароль" name="USER_PASSWORD" maxlength="255"
											       value="<?= $arResult["USER_PASSWORD"] ?>" autocomplete="off">
										</div>
									</div>

									<!--<div class="bx-authform-formgroup-container">
							<div class="bx-authform-label-container"><span class="bx-authform-starrequired">*</span><? //=GetMessage("AUTH_CONFIRM")?></div>
							<div class="bx-authform-input-container">
				<? //if($arResult["SECURE_AUTH"]):?>
								<div class="bx-authform-psw-protected" id="bx_auth_secure_conf" style="display:none"><div class="bx-authform-psw-protected-desc"><span></span><? //echo GetMessage("AUTH_SECURE_NOTE")?></div></div>

				<script type="text/javascript">
				document.getElementById('bx_auth_secure_conf').style.display = '';
				</script>
				<? //endif?>
								<input type="password" name="USER_CONFIRM_PASSWORD" maxlength="255" value="<? //=$arResult["USER_CONFIRM_PASSWORD"]?>" autocomplete="off" />
							</div>
						</div>-->

									<div class="col-xs-12">
										<div class="input-box">
											<label for="re-enter-pass">Повторите пароль</label>
											<? if ($arResult["SECURE_AUTH"]): ?>
												<div class="bx-authform-psw-protected" id="bx_auth_secure_conf" style="display:none">
													<div class="bx-authform-psw-protected-desc"><span></span><? echo GetMessage("AUTH_SECURE_NOTE") ?></div>
												</div>

												<script type="text/javascript">
                                                    document.getElementById('bx_auth_secure_conf').style.display = '';
												</script>
											<? endif ?>
											<input id="re-enter-pass" type="password" required placeholder="Введите повторно пароль" name="USER_CONFIRM_PASSWORD" maxlength="255"
											       value="<?= $arResult["USER_CONFIRM_PASSWORD"] ?>" autocomplete="off">
										</div>
									</div>

									<!--<div class="bx-authform-formgroup-container">
							<div class="bx-authform-label-container"><? //if($arResult["EMAIL_REQUIRED"]):?><span class="bx-authform-starrequired">*</span><? //endif?><? //=GetMessage("AUTH_EMAIL")?></div>
							<div class="bx-authform-input-container">
								<input type="text" name="USER_EMAIL" maxlength="255" value="<? //=$arResult["USER_EMAIL"]?>" />
							</div>
						</div>-->

									<? if ($arResult["USER_PROPERTIES"]["SHOW"] == "Y"): ?>
										<? foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField): ?>

											<div class="bx-authform-formgroup-container">
												<div class="bx-authform-label-container"><? if ($arUserField["MANDATORY"] == "Y"): ?><span
															class="bx-authform-starrequired">*</span><? endif ?><?= $arUserField["EDIT_FORM_LABEL"] ?></div>
												<div class="bx-authform-input-container">
													<?
													$APPLICATION->IncludeComponent(
														"bitrix:system.field.edit",
														$arUserField["USER_TYPE"]["USER_TYPE_ID"],
														array(
															"bVarsFromForm" => $arResult["bVarsFromForm"],
															"arUserField" => $arUserField,
															"form_name" => "bform"
														),
														null,
														array("HIDE_ICONS" => "Y")
													);
													?>
												</div>
											</div>

										<? endforeach; ?>
									<? endif; ?>

									<? if ($arResult["USE_CAPTCHA"] == "Y"): ?>
										<input type="hidden" name="captcha_sid" value="<?= $arResult["CAPTCHA_CODE"] ?>"/>

										<div class="bx-authform-formgroup-container col-xs-12">
											<div class="bx-authform-label-container">
												<span class="bx-authform-starrequired">*</span><?= GetMessage("CAPTCHA_REGF_PROMT") ?>
											</div>
											<div class="bx-captcha"><img src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["CAPTCHA_CODE"] ?>" width="180" height="40"
											                             alt="CAPTCHA"/></div>
											<div class="bx-authform-input-container">
												<input type="text" name="captcha_word" maxlength="50" value="" autocomplete="off"/>
											</div>
										</div>
									<? endif ?>

									<div class="col-xs-12 _flex--space">
										<div class="check-box left-side">
											<label for="USER_REMEMBER" class="bx-filter-param-label">
												<input type="checkbox" id="USER_REMEMBER" name="USER_REMEMBER" value="Y" class="checkbox">
												<span class="checkbox-checked">
							<i class="fa fa-square-o"></i>
							<i class="fa fa-check-square-o"></i>
						</span>
												<p class="nowrap">Запомнить меня</p>
											</label>
										</div>
										<!--<div class="bx-authform-formgroup-container">-->
										<button class="button blue" type="submit" class="btn btn-primary" name="Register"/>
										<?= GetMessage("AUTH_REGISTER") ?></button>
										<!--</div>-->
									</div>

									<!--<div class="bx-authform-formgroup-container">
							<input type="submit" class="btn btn-primary" name="Register" value="<? //=GetMessage("AUTH_REGISTER")?>" />
						</div>-->


									<!--<div class="bx-authform-link-container">
							<a href="<? //=$arResult["AUTH_AUTH_URL"]?>" rel="nofollow"><b><? //=GetMessage("AUTH_AUTH")?></b></a>
						</div>-->

									<div class="col-xs-12 mt-20">
										<hr>
										<div class="new-account align-center mt-20"><span>Уже существует аккаунт?</span>
											<a class="link" href="<?= $arResult["AUTH_AUTH_URL"] ?>" rel="nofollow">Авторизоваться</a>
										</div>
									</div>
								</div>
							</form>
							<!--/noindex-->

							<script type="text/javascript">
                                document.bform.USER_NAME.focus();
							</script>

						<? endif ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>