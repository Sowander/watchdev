<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $component
 */

//one css for all system.auth.* forms
$APPLICATION->SetAdditionalCSS("/bitrix/css/main/system.auth/flat/style.css");
?>
<section class="checkout-section ptb-95 mobile-padding">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<form class="main-form full main-form__center" name="form_auth" method="post" target="_top" action="<?= $arResult["AUTH_URL"] ?>">
							<div class="row">


								<?
								if (!empty($arParams["~AUTH_RESULT"])):
									$text = str_replace(array("<br>", "<br />"), "\n", $arParams["~AUTH_RESULT"]["MESSAGE"]);
									?>
									<div class="alert alert-danger"><?= nl2br(htmlspecialcharsbx($text)) ?></div>
								<? endif ?>

								<?
								if ($arResult['ERROR_MESSAGE'] <> ''):
									$text = str_replace(array("<br>", "<br />"), "\n", $arResult['ERROR_MESSAGE']);
									?>
									<div class="alert alert-danger"><?= nl2br(htmlspecialcharsbx($text)) ?></div>
								<? endif ?>

								<div class="col-xs-12">
									<div class=" heading-part heading-bg ">
										<h2 class="heading ">Авторизация</h2>
									</div>
								</div>
								<div class="col-xs-12">
									<? if ($arResult["AUTH_SERVICES"]): ?>
										<?
										$APPLICATION->IncludeComponent("bitrix:socserv.auth.form",
											"flat",
											array(
												"AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
												"AUTH_URL" => $arResult["AUTH_URL"],
												"POST" => $arResult["POST"],
											),
											$component,
											array("HIDE_ICONS" => "Y")
										);
										?>
									<? endif ?>


									<input type="hidden" name="AUTH_FORM" value="Y"/>
									<input type="hidden" name="TYPE" value="AUTH"/>
									<? if (strlen($arResult["BACKURL"]) > 0): ?>
										<input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
									<? endif ?>
									<? foreach ($arResult["POST"] as $key => $value): ?>
										<input type="hidden" name="<?= $key ?>" value="<?= $value ?>"/>
									<? endforeach ?>

									<!--<div class="bx-authform-formgroup-container">
				<div class="bx-authform-label-container"><? //=GetMessage("AUTH_LOGIN")?></div>
				<div class="bx-authform-input-container">
					<input type="text" name="USER_LOGIN" maxlength="255" value="<? //=$arResult["LAST_LOGIN"]?>" />
				</div>
			</div>-->

									<div class="col-xs-12">
										<div class="input-box">
											<label for="login-email">Email</label>
											<input id="login-email" type="email " name="USER_LOGIN" maxlength="255" value="<?= $arResult["LAST_LOGIN"] ?>" required
											       placeholder="Введите email ">
										</div>
									</div>

									<!--<div class="bx-authform-formgroup-container">
				<div class="bx-authform-label-container"><? //=GetMessage("AUTH_PASSWORD")?></div>
				<div class="bx-authform-input-container">
	<? //if($arResult["SECURE_AUTH"]):?>
					<div class="bx-authform-psw-protected" id="bx_auth_secure" style="display:none"><div class="bx-authform-psw-protected-desc"><span></span><? //echo GetMessage("AUTH_SECURE_NOTE")?></div></div>

	<script type="text/javascript">
	document.getElementById('bx_auth_secure').style.display = '';
	</script>
	<? //endif?>
					<input type="password" name="USER_PASSWORD" maxlength="255" autocomplete="off" />
				</div>
			</div>-->

									<div class="col-xs-12">
										<div class="input-box ">
											<label for="login-pass">Пароль</label>
											<input id="login-pass" type="password" name="USER_PASSWORD" maxlength="255" autocomplete="off" required placeholder="Введите пароль">
										</div>
									</div>

									<? if ($arResult["CAPTCHA_CODE"]): ?>
										<input type="hidden" name="captcha_sid" value="<? echo $arResult["CAPTCHA_CODE"] ?>"/>

										<div class="bx-authform-formgroup-container dbg_captha">
											<div class="bx-authform-label-container">
												<? echo GetMessage("AUTH_CAPTCHA_PROMT") ?>
											</div>
											<div class="bx-captcha"><img src="/bitrix/tools/captcha.php?captcha_sid=<? echo $arResult["CAPTCHA_CODE"] ?>" width="180" height="40"
											                             alt="CAPTCHA"/></div>
											<div class="bx-authform-input-container">
												<input type="text" name="captcha_word" maxlength="50" value="" autocomplete="off"/>
											</div>
										</div>
									<? endif; ?>

									<? if ($arResult["STORE_PASSWORD"] == "Y"): ?>
									<!--<div class="bx-authform-formgroup-container">
				<div class="checkbox">
					<label class="bx-filter-param-label">
						<input type="checkbox" id="USER_REMEMBER" name="USER_REMEMBER" value="Y" />
						<span class="bx-filter-param-text"><? //=GetMessage("AUTH_REMEMBER_ME")?></span>
					</label>
				</div>
			</div>-->

									<div class="col-xs-12 _flex--space">
										<div class="check-box left-side">
											<label for="USER_REMEMBER" class="bx-filter-param-label">
												<input type="checkbox" id="USER_REMEMBER" name="USER_REMEMBER" value="Y" class="checkbox">
												<span class="checkbox-checked">
							<i class="fa fa-square-o"></i>
							<i class="fa fa-check-square-o"></i>
						</span>
												<p class="nowrap">Запомнить меня</p>
											</label>
										</div>
										<? endif ?>
										<!--<div class="bx-authform-formgroup-container">-->
										<button class="button blue" type="submit" class="btn btn-primary" name="Login"/>
										<?= GetMessage("AUTH_AUTHORIZE") ?></button>
										<!--</div>-->
									</div>

									<? if ($arParams["NOT_SHOW_LINKS"] != "Y"): ?>
										<hr class="bxe-light">

										<noindex>
											<!--<div class="bx-authform-link-container">
				<a href="<? //=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>" rel="nofollow"><b><? //=GetMessage("AUTH_FORGOT_PASSWORD_2")?></b></a>
			</div>-->
											<div class="col-xs-12 "><a title="Забыли пароль?" class="forgot-password mtb-20 " href="<?= $arResult["AUTH_FORGOT_PASSWORD_URL"] ?>">Забыли
													пароль?</a>
												<hr>
											</div>
										</noindex>
									<? endif ?>

									<? if ($arParams["NOT_SHOW_LINKS"] != "Y" && $arResult["NEW_USER_REGISTRATION"] == "Y" && $arParams["AUTHORIZE_REGISTRATION"] != "Y"): ?>
										<!--noindex-->
										<!--<div class="bx-authform-link-container">
				<? //=GetMessage("AUTH_FIRST_ONE")?><br />
				<a href="<? //=$arResult["AUTH_REGISTER_URL"]?>" rel="nofollow"><b><? //=GetMessage("AUTH_REGISTER")?></b></a>
			</div>-->

										<div class="col-xs-12 ">
											<div class="new-account align-center mt-20 ">
												<span>Нет аккаунта?</span>
												<a class="link " title="Регистрация" href="<?= $arResult["AUTH_REGISTER_URL"] ?>" rel="nofollow">Зарегистрироваться</a>
											</div>
										</div>
										<!--/noindex-->
									<? endif ?>
								</div>

							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
</section>

<script type="text/javascript">
	<?if (strlen($arResult["LAST_LOGIN"]) > 0):?>
    try {
        document.form_auth.USER_PASSWORD.focus();
    } catch (e) {
    }
	<?else:?>
    try {
        document.form_auth.USER_LOGIN.focus();
    } catch (e) {
    }
	<?endif?>
</script>

