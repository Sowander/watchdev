<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */

//one css for all system.auth.* forms
$APPLICATION->SetAdditionalCSS("/bitrix/css/main/system.auth/flat/style.css");
?>

<section class="checkout-section ptb-95 mobile-padding">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<?
						if (!empty($arParams["~AUTH_RESULT"])):
							$text = str_replace(array("<br>", "<br />"), "\n", $arParams["~AUTH_RESULT"]["MESSAGE"]);
							?>
							<div class="alert <?= ($arParams["~AUTH_RESULT"]["TYPE"] == "OK" ? "alert-success" : "alert-danger") ?>"><?= nl2br(htmlspecialcharsbx($text)) ?></div>
						<? endif ?>


						<form class="main-form full main-form__center" name="bform" method="post" target="_top" action="<?= $arResult["AUTH_URL"] ?>">
							<div class="row">
								<? if ($arResult["BACKURL"] <> ''): ?>
									<input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
								<? endif ?>

								<div class="col-xs-12">
									<div class=" heading-part heading-bg">
										<h2 class="heading">Забыли пароль?</h2>
									</div>
								</div>
								<div class="col-xs-12">
									<div class="col-xs-12">
										<p class="attention">В форму ниже введите свой электронный адрес, указанный при регистрации, и через несколько минут на Ваш E-mail придет
											письмо с паролем</p>
									</div>
									<input type="hidden" name="AUTH_FORM" value="Y">
									<input type="hidden" name="TYPE" value="SEND_PWD">
									<div class="col-xs-12">
										<div class="input-box">
											<label for="login-email">Email</label><span class="required">*</span>
											<input name="USER_LOGIN" maxlength="255" value="<?= $arResult["LAST_LOGIN"] ?>" id="login-email" type="email" required
											       placeholder="Введите email">
										</div>
									</div>
									<div class="col-xs-12 align-right">
										<input name="submit" type="submit" class="button blue" name="send_account_info" value="Выслать пароль">
									</div>
								</div>
								<? if ($arResult["USE_CAPTCHA"]): ?>
									<input type="hidden" name="captcha_sid" value="<?= $arResult["CAPTCHA_CODE"] ?>"/>

									<div class="bx-authform-formgroup-container">
										<div class="bx-authform-label-container"><? echo GetMessage("system_auth_captcha") ?></div>
										<div class="bx-captcha"><img src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["CAPTCHA_CODE"] ?>" width="180" height="40"
										                             alt="CAPTCHA"/></div>
										<div class="bx-authform-input-container">
											<input type="text" name="captcha_word" maxlength="50" value="" autocomplete="off"/>
										</div>
									</div>

								<? endif ?>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
        document.bform.onsubmit = function () {
            document.bform.USER_EMAIL.value = document.bform.USER_LOGIN.value;
        };
        document.bform.USER_LOGIN.focus();
	</script>

</section>
