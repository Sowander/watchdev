<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<!--  Client-Main Block Start  -->
<section class="client-main container align-center ptb-55">
	<div class="client-inner">
		<div id="client" class="owl-carousel">
			<? foreach ($arResult["ITEMS"] as $arItem)
			{ ?>
				<div class="item client-detail">
					<div class="client-img"><img src="<?= $arItem[PREVIEW_PICTURE][SRC] ?>" alt="<?= $arItem[NAME] ?>"></div>
					<h4 class="client-title"><?= $arItem[NAME] ?></h4>
					<p><?= $arItem[PREVIEW_TEXT] ?></p>
					<h4 class="sub-title"><?= $arItem[PROPERTIES]["EMAIL"][VALUE] ?></h4>
				</div>
			<? } ?>


		</div>
	</div>
</section>
<!--  Client-Main Block End  -->

