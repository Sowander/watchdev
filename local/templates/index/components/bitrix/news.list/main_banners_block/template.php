<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<!--  Sub-banner Block Start  -->
<section class="sub-banner-block">
	<div id="sub-banner-main" class="owl-carousel">
		<? foreach ($arResult["ITEMS"] as $arItem)
		{ ?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>

			<div id="<?= $this->GetEditAreaId($arItem['ID']); ?>" class="item">
				<div class="sub-banner">
					<div class="sub-img">
						<a href="<?= $arItem[CODE] ?>">
							<img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>">
						</a>
					</div>
					<div class="sub-desc">
						<div class="sub-desc-inner">
							<button class="btn btn-color small">
								<a href="<?= $arItem[CODE] ?>"><?= $arItem[NAME] ?></a>
							</button>
							<div class="sub-title"><?= $arItem[PROPERTIES][NAME2][VALUE] ?></div>
							<div class="sub-subtitle"><?= $arItem[PROPERTIES][NAME3][VALUE] ?></div>
						</div>
					</div>
				</div>
			</div>
		<? } ?>


	</div>
</section>
<!--  Sub-banner Block End  -->
