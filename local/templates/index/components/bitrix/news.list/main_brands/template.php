<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<section class="pb-50 brand-main">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="heading-part">
					<h2 class="main_title">Бренды часов</h2>
				</div>
			</div>
		</div>
		<div class="row brand">
			<div class="col-md-12">
				<div id="brand-logo" class="owl-carousel align_center">
					<? foreach ($arResult["ITEMS"] as $arItem)
					{ ?>
						<div class="item"><a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt="<? echo $arItem["NAME"] ?>"></a>
						</div>
					<? } ?>
				</div>
			</div>
		</div>
	</div>
</section>
