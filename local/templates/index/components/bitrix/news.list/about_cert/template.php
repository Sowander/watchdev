<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<section class="pb-95">
	<div class="container">
		<div class="">
			<div class="heading-part">
				<h2 class="main_title">Сертификаты</h2>
			</div>
		</div>
		<div class="about-certificates owl-carousel" id="about-certificates">

			<? foreach ($arResult["ITEMS"] as $arItem)
			{ ?>


				<div class="about-certificates-item">
					<a href="<?= $arItem["DETAIL_PICTURE"]["SRC"] ?>" class="gallery" data-fancybox="gallery">
						<img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt="">
					</a>
				</div>


			<? } ?>


		</div>
	</div>
</section>

