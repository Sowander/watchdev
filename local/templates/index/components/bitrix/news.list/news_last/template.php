<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<ul>
	<? foreach ($arResult["ITEMS"] as $arItem)
	{ ?>
		<li>
			<? if ($arItem["SMALL_IMG"])
			{ ?>
				<div class="pro-media"> <a><img alt="<? echo $arItem["NAME"] ?>" src="<? echo $arItem["SMALL_IMG"] ?>"></a> </div><? } ?>
			<div class="pro-detail-info"><a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>"><? echo $arItem["NAME"] ?></a>
				<div class="post-info"><? echo $arItem["DISPLAY_ACTIVE_FROM"] ?></div>
			</div>
		</li>
	<? } ?>

</ul>


