<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<section class="ptb-140 shopbybg">
	<div class="row">
		<div class="col-lg-5 col-md-5"></div>
		<div class="col-lg-7 col-md-7">
			<div class="col-xs-12">
				<div class="heading-part mb-40">
					<h2 class="main_title">

						<? $APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_SHOW" => "file",
								"PATH" => SITE_DIR . "include/sbz1.php",
								"AREA_FILE_RECURSIVE" => "N",
								"EDIT_MODE" => "php",
							),
							false,
							Array('HIDE_ICONS' => 'Y')
						); ?>

					</h2>
				</div>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 mb-16">
				<p>
					<? $APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => SITE_DIR . "include/sbz2.php",
							"AREA_FILE_RECURSIVE" => "N",
							"EDIT_MODE" => "php",
						),
						false,
						Array('HIDE_ICONS' => 'Y')
					); ?>

				</p>
			</div>
			<div class="col-lg-12 col-sm-12 col-xs-12">

				<? foreach ($arResult["ITEMS"] as $arItem)
				{ ?>
					<div class="col-lg-5 col-md-6 col-sm-6 col-xs-6 plr_0">
						<div class="<?= $arItem[PROPERTIES]["CLASS"][VALUE] ?>">
							<div class="content-title"><?= $arItem[NAME] ?> :</div>
							<div class="content-shortdesc"><?= $arItem[PREVIEW_TEXT] ?></div>
							<div class="watchhover">
								<div class="watchdetail"><a class="watch" href="<?= $arItem[PROPERTIES]["ITEM"][VALUE] ?>"><?= $arItem[PROPERTIES]["ITEMNAME"][VALUE] ?></a> <a
											class="watch popup-youtube" href="<?= $arItem[PROPERTIES]["VIDEO"][VALUE] ?>"><?= $arItem[PROPERTIES]["VIDEONAME"][VALUE] ?></a>
								</div>
							</div>
						</div>
					</div>
				<? } ?>


			</div>
		</div>
	</div>
</section>
