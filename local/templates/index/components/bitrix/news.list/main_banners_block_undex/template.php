<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<!--  Parallax Block Start  -->
<section class="ptb-55 parallax-block" data-stellar-background-ratio="0.5">
	<div class="container service-feature-block">
		<div class="row">
			<? foreach ($arResult["ITEMS"] as $arItem)
			{ ?>
				<div class="col-sm-4 col-xs-12">
					<div class="ser-feature-block">
						<div class="<?= $arItem[PROPERTIES]["CLASS"][VALUE] ?>">
							<div class="ser-title"><?= $arItem[NAME] ?></div>
							<div class="ser-subtitle"><?= $arItem[PREVIEW_TEXT] ?></div>
						</div>
					</div>
				</div>
			<? } ?>

		</div>
	</div>
</section>
<!--  Parallax Block End  -->


