<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<section class="pb-95 blog">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="heading-part">
					<h2 class="main_title">Последние новости</h2>
				</div>
			</div>
		</div>
		<div class="row blog-flex">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 home-blog-item">
				<?
				$arItem = $arResult["ITEMS"][0];
				?>

				<div class="item">
					<div class="blog-item">
						<div class="blog-media">
							<img src="/images/blog_img1.jpg" alt="<?= $arItem["NAME"] ?>">
						</div>
						<div class="blog-detail opasity">
							<div class="blog-detail-inner">
								<div class="blog-contant">
									<a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>">
										<h3><?= $arItem["NAME"] ?></h3>
									</a>
									<div class="blog-desc">
										<a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>">
											<?= $arItem["PREVIEW_TEXT"] ?>
										</a>
									</div>
									<div class="post-info">
										<ul>
											<li class="post-date"><a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></a></li>
											<? /* <li class="post-comment"><a href="#">(5) comments</a></li>*/ ?>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 home-blog-item">
				<?
				$arItem = $arResult["ITEMS"][1];
				?>
				<div class="item">
					<div class="blog-item">
						<div class="blog-media">
							<img src="/images/blog_img2.jpg" alt="Tizzy">
						</div>
						<div class="blog-detail">
							<div class="blog-detail-inner">
								<div class="blog-contant">
									<a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>">
										<h3><?= $arItem["NAME"] ?></h3>
									</a>
									<div class="blog-desc">
										<a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>">
											<?= $arItem["PREVIEW_TEXT"] ?>
										</a>
									</div>
									<div class="post-info">
										<ul>
											<li class="post-date"><a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row blog-flex">
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 home-blog-item">
				<?
				$arItem = $arResult["ITEMS"][2];
				?>
				<div class="item">
					<div class="blog-item">
						<div class="blog-media">
							<img src="images/blog_img1.jpg" alt="Tizzy">
						</div>
						<div class="blog-detail">
							<div class="blog-detail-inner">
								<div class="blog-contant">
									<a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>">
										<h3><?= $arItem["NAME"] ?></h3>
									</a>
									<div class="blog-desc">
										<a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>">
											<?= $arItem["PREVIEW_TEXT"] ?>
										</a>
									</div>
									<div class="post-info">
										<ul>
											<li class="post-date"><a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 home-blog-item">
				<?
				$arItem = $arResult["ITEMS"][3];
				?>
				<div class="item">
					<div class="blog-item">
						<div class="blog-media">
							<img src="images/blog_img2.jpg" alt="Tizzy">
						</div>
						<div class="blog-detail">
							<div class="blog-detail-inner">
								<div class="blog-contant">
									<a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>">
										<h3><?= $arItem["NAME"] ?></h3>
									</a>
									<div class="blog-desc">
										<a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>">
											<?= $arItem["PREVIEW_TEXT"] ?>
										</a>
									</div>
									<div class="post-info">
										<ul>
											<li class="post-date"><a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


			<? /*
		  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 home-blog-item">
            <?
			$arItem = $arResult["ITEMS"][4];
			?>
			<div class="item">
              <div class="blog-item">
                <div class="blog-media">
                  <img src="images/blog_img3.jpg" alt="Tizzy">
                </div>
                <div class="blog-detail">
                  <div class="blog-detail-inner">
                    <div class="blog-contant">
                      <a href="<?echo $arItem["DETAIL_PAGE_URL"]?>">
                        <h3><?=$arItem["NAME"]?></h3>
                      </a>
                      <div class="blog-desc">
                        <a href="<?echo $arItem["DETAIL_PAGE_URL"]?>">
                          <?=$arItem["PREVIEW_TEXT"]?>
                        </a>
                      </div>
                      <div class="post-info">
                        <ul>
                          <li class="post-date"><a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 home-blog-item">
            <?
			$arItem = $arResult["ITEMS"][5];
			?>
			<div class="item">
              <div class="blog-item">
                <div class="blog-media">
                  <img src="images/blog_img4.jpg" alt="Tizzy">
                </div>
                <div class="blog-detail">
                  <div class="blog-detail-inner">
                    <div class="blog-contant">
                      <a href="<?echo $arItem["DETAIL_PAGE_URL"]?>">
                        <h3><?=$arItem["NAME"]?></h3>
                      </a>
                      <div class="blog-desc">
                        <a href="<?echo $arItem["DETAIL_PAGE_URL"]?>">
                          <?=$arItem["PREVIEW_TEXT"]?>
                        </a>
                      </div>
                      <div class="post-info">
                        <ul>
                          <li class="post-date"><a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
		  */ ?>


		</div>
	</div>
</section>

