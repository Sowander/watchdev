<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/**
 * @global array $arParams
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @global string $cartId
 */
$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');
?>
<?
$count = 0;
foreach ($arResult["CATEGORIES"] as $category => $items)
{
	if (empty($items))
		continue;
	foreach ($items as $v)
	{
		$count += $v['QUANTITY'];
	}
}
?>
<a href="#" id="AbascketCount"><span id="bascketCount"> <i class="fa fa-cart-arrow-down" aria-hidden="true"></i>
		<small class="cart-notification"><?= $count ?></small> </span>
</a>