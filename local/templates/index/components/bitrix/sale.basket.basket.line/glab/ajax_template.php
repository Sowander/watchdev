<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$this->IncludeLangFile('template.php');
$count = 0;
$cartId = $arParams['cartId'];
$totalPrice = str_replace(array(' руб.', 'руб.'), "", $arResult['TOTAL_PRICE']);
require(realpath(dirname(__FILE__)) . '/top_template.php');

if ($arResult['NUM_PRODUCTS'] > 0)
{
	?>
	<div data-role="basket-item-list" class="bx-basket-item-list cart-dropdown header-link-dropdown">
		<ul id="<?= $cartId ?>products" class="bx-basket-item-list-container cart-list link-dropdown-list">
			<? foreach ($arResult["CATEGORIES"] as $category => $items):
				if (empty($items))
					continue;
				?>
				<? foreach ($items as $v):?>
				<li>
					<a class="close-cart" onclick="<?= $cartId ?>.removeItemFromCart(<?= $v['ID'] ?>)" title="<?= GetMessage("TSB1_DELETE") ?>"><i class="fa fa-trash-o
							cart-remove-item"></i></a>
					<div class="media">
						<a href="<?= $v["DETAIL_PAGE_URL"] ?>">
							<img src="<?= $v["PICTURE_SRC"] ?>" alt="<?= $v["NAME"] ?>" class="pull-left">
						</a>
						<div class="media-body">
								<span>
									<? if ($v["DETAIL_PAGE_URL"]):?>
										<a href="<?= $v["DETAIL_PAGE_URL"] ?>"><?= $v["NAME"] ?></a>
									<? else:?>
										<?= $v["NAME"] ?>
									<? endif ?>
								</span>
							<? if ($arParams["SHOW_PRICE"] == "Y"):?>
								<p class="cart-price"><?= $v["PRICE"] ?> <i class="fa fa-rub" aria-hidden="true"></i></p>
							<? endif ?>
							<div class="cart-dropdown-status"><p>В наличии</p></div>
						</div>

					</div>
				</li>
				<? $count += $items['QUANTITY'] ?>
			<? endforeach ?>
			<? endforeach ?>
		</ul>
		<p class="cart-sub-totle"><span class="pull-left"><?= GetMessage('TSB1_TOTAL_PRICE') ?></span> <span class="pull-right"><strong class="price-box"><?= $totalPrice ?><i
							class="fa fa-rub" aria-hidden="true"></i></strong></span></p>
		<div class="clearfix"></div>
		<div class="mt-20">
			<a class="button blue" href="<?= $arParams['PATH_TO_BASKET'] ?>"><?= GetMessage('TSB1_CART') ?></a>

			<a href="<?= $arParams['PATH_TO_ORDER'] ?>" class="button green right-side"><?= GetMessage('TSB1_2ORDER') ?></a>
		</div>
	</div>

	<script>
        BX.ready(function () {
			<?=$cartId?>.
            fixCart();
        });
	</script>
	<?
}