<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

#var_dump($arResult['CATALOG_TYPE']);
#exit("**");


/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);
$this->addExternalCss('/bitrix/css/main/bootstrap.css');

$templateLibrary = array('popup', 'fx');
$currencyList = '';

if (!empty($arResult['CURRENCIES']))
{
	$templateLibrary[] = 'currency';
	$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}

$templateData = array(
	'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
	'TEMPLATE_LIBRARY' => $templateLibrary,
	'CURRENCIES' => $currencyList,
	'ITEM' => array(
		'ID' => $arResult['ID'],
		'IBLOCK_ID' => $arResult['IBLOCK_ID'],
		'OFFERS_SELECTED' => $arResult['OFFERS_SELECTED'],
		'JS_OFFERS' => $arResult['JS_OFFERS']
	)
);
unset($currencyList, $templateLibrary);

$mainId = $this->GetEditAreaId($arResult['ID']);
$itemIds = array(
	'ID' => $mainId,
	'DISCOUNT_PERCENT_ID' => $mainId . '_dsc_pict',
	'STICKER_ID' => $mainId . '_sticker',
	'BIG_SLIDER_ID' => $mainId . '_big_slider',
	'BIG_IMG_CONT_ID' => $mainId . '_bigimg_cont',
	'SLIDER_CONT_ID' => $mainId . '_slider_cont',
	'OLD_PRICE_ID' => $mainId . '_old_price',
	'PRICE_ID' => $mainId . '_price',
	'DISCOUNT_PRICE_ID' => $mainId . '_price_discount',
	'PRICE_TOTAL' => $mainId . '_price_total',
	'SLIDER_CONT_OF_ID' => $mainId . '_slider_cont_',
	'QUANTITY_ID' => $mainId . '_quantity',
	'QUANTITY_DOWN_ID' => $mainId . '_quant_down',
	'QUANTITY_UP_ID' => $mainId . '_quant_up',
	'QUANTITY_MEASURE' => $mainId . '_quant_measure',
	'QUANTITY_LIMIT' => $mainId . '_quant_limit',
	'BUY_LINK' => $mainId . '_buy_link',
	'ADD_BASKET_LINK' => $mainId . '_add_basket_link',
	'BASKET_ACTIONS_ID' => $mainId . '_basket_actions',
	'NOT_AVAILABLE_MESS' => $mainId . '_not_avail',
	'COMPARE_LINK' => $mainId . '_compare_link',
	'TREE_ID' => $mainId . '_skudiv',
	'DISPLAY_PROP_DIV' => $mainId . '_sku_prop',
	'DISPLAY_MAIN_PROP_DIV' => $mainId . '_main_sku_prop',
	'OFFER_GROUP' => $mainId . '_set_group_',
	'BASKET_PROP_DIV' => $mainId . '_basket_prop',
	'SUBSCRIBE_LINK' => $mainId . '_subscribe',
	'TABS_ID' => $mainId . '_tabs',
	'TAB_CONTAINERS_ID' => $mainId . '_tab_containers',
	'SMALL_CARD_PANEL_ID' => $mainId . '_small_card_panel',
	'TABS_PANEL_ID' => $mainId . '_tabs_panel'
);
$obName = $templateData['JS_OBJ'] = 'ob' . preg_replace('/[^a-zA-Z0-9_]/', 'x', $mainId);
$name = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
	: $arResult['NAME'];
$title = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE']
	: $arResult['NAME'];
$alt = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT']
	: $arResult['NAME'];

$haveOffers = !empty($arResult['OFFERS']);
if ($haveOffers)
{
	$actualItem = isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']])
		? $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]
		: reset($arResult['OFFERS']);
	$showSliderControls = false;

	foreach ($arResult['OFFERS'] as $offer)
	{
		if ($offer['MORE_PHOTO_COUNT'] > 1)
		{
			$showSliderControls = true;
			break;
		}
	}
} else
{
	$actualItem = $arResult;
	$showSliderControls = $arResult['MORE_PHOTO_COUNT'] > 1;
}

$skuProps = array();
$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];
$measureRatio = $actualItem['ITEM_MEASURE_RATIOS'][$actualItem['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'];
$showDiscount = $price['PERCENT'] > 0;

$showDescription = !empty($arResult['PREVIEW_TEXT']) || !empty($arResult['DETAIL_TEXT']);
$showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
$buyButtonClassName = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);
$showButtonClassName = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showSubscribe = $arParams['PRODUCT_SUBSCRIPTION'] === 'Y' && ($arResult['CATALOG_SUBSCRIBE'] === 'Y' || $haveOffers);

$arParams['MESS_BTN_BUY'] = $arParams['MESS_BTN_BUY'] ?: Loc::getMessage('CT_BCE_CATALOG_BUY');
$arParams['MESS_BTN_ADD_TO_BASKET'] = $arParams['MESS_BTN_ADD_TO_BASKET'] ?: Loc::getMessage('CT_BCE_CATALOG_ADD');
$arParams['MESS_NOT_AVAILABLE'] = $arParams['MESS_NOT_AVAILABLE'] ?: Loc::getMessage('CT_BCE_CATALOG_NOT_AVAILABLE');
$arParams['MESS_BTN_COMPARE'] = $arParams['MESS_BTN_COMPARE'] ?: Loc::getMessage('CT_BCE_CATALOG_COMPARE');
$arParams['MESS_PRICE_RANGES_TITLE'] = $arParams['MESS_PRICE_RANGES_TITLE'] ?: Loc::getMessage('CT_BCE_CATALOG_PRICE_RANGES_TITLE');
$arParams['MESS_DESCRIPTION_TAB'] = $arParams['MESS_DESCRIPTION_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_DESCRIPTION_TAB');
$arParams['MESS_PROPERTIES_TAB'] = $arParams['MESS_PROPERTIES_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_PROPERTIES_TAB');
$arParams['MESS_COMMENTS_TAB'] = $arParams['MESS_COMMENTS_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_COMMENTS_TAB');
$arParams['MESS_SHOW_MAX_QUANTITY'] = $arParams['MESS_SHOW_MAX_QUANTITY'] ?: Loc::getMessage('CT_BCE_CATALOG_SHOW_MAX_QUANTITY');
$arParams['MESS_RELATIVE_QUANTITY_MANY'] = $arParams['MESS_RELATIVE_QUANTITY_MANY'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_MANY');
$arParams['MESS_RELATIVE_QUANTITY_FEW'] = $arParams['MESS_RELATIVE_QUANTITY_FEW'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_FEW');

$positionClassMap = array(
	'left' => 'product-item-label-left',
	'center' => 'product-item-label-center',
	'right' => 'product-item-label-right',
	'bottom' => 'product-item-label-bottom',
	'middle' => 'product-item-label-middle',
	'top' => 'product-item-label-top'
);

$discountPositionClass = 'product-item-label-big';
if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y' && !empty($arParams['DISCOUNT_PERCENT_POSITION']))
{
	foreach (explode('-', $arParams['DISCOUNT_PERCENT_POSITION']) as $pos)
	{
		$discountPositionClass .= isset($positionClassMap[$pos]) ? ' ' . $positionClassMap[$pos] : '';
	}
}

$labelPositionClass = 'product-item-label-big';
if (!empty($arParams['LABEL_PROP_POSITION']))
{
	foreach (explode('-', $arParams['LABEL_PROP_POSITION']) as $pos)
	{
		$labelPositionClass .= isset($positionClassMap[$pos]) ? ' ' . $positionClassMap[$pos] : '';
	}
}
?>

	<section class="product-sm-xs ptb-95 mobile-padding" id="<?= $itemIds['ID'] ?>" itemscope itemtype="http://schema.org/Product">
		<div class="container">
			<div class="row">
				<div class="col-lg-5 col-md-5 col-sm-6 mb-xs-30">
					<div class="fotorama">

						<a data-entity="image" data-id="<?= $photo['ID'] ?>">
							<img src="<?= $arResult[DETAIL_PICTURE]['SRC'] ?>" alt="<?= $alt ?>" title="<?= $title ?>"<?= ($key == 0 ? ' itemprop="image"' : '') ?>>
						</a>

						<?

						if (!empty($actualItem['MORE_PHOTO']))
						{
							foreach ($actualItem['MORE_PHOTO'] as $key => $photo)
							{
								?>
								<a data-entity="image" data-id="<?= $photo['ID'] ?>">
									<img src="<?= $photo['SRC'] ?>" alt="<?= $alt ?>" title="<?= $title ?>"<?= ($key == 0 ? ' itemprop="image"' : '') ?>>
								</a>
								<?
							}
						}
						?>


					</div>
				</div>
				<div class="col-lg-7 col-md-7">
					<div class="row">
						<div class="col-xs-12">
							<div class="product-detail-main">
								<div class="product-item-details">
									<? if ($arResult['PROPERTIES']['NEWPRODUCT']['VALUE'] == 'да')
									{ ?>
										<p class="product-detail-main__new"> Новинка </p>
									<? } else
									{ ?>
										<p class="product-item__new"> &nbsp; </p>
									<? } ?>
									<h1 class="product-item-name"><?= $name ?></h1>
									<div class="product-info">
										<div class="price-box">

											<span class="price" id="<?= $itemIds['PRICE_ID'] ?>"><?= $price['PRINT_RATIO_PRICE'] ?></span>


											<?
											if ($arParams['SHOW_OLD_PRICE'] === 'Y')
											{
												?>
												<del class="price old-price" id="<?= $itemIds['OLD_PRICE_ID'] ?>"
												     style="display: <?= ($showDiscount ? '' : 'none') ?>;">
													<?= ($showDiscount ? $price['PRINT_RATIO_BASE_PRICE'] : '') ?>
												</del>
												<?
											}
											?>

										</div>
										<? if ($arResult['PRODUCT']['QUANTITY'] > 0)
										{ ?>
											<div class="product-item__stock"> в наличии</div>
										<? } else
										{ ?>
											<div class="product-item__stock"> под заказ</div>
										<? } ?>
										<div class="product-info-stock-sku">
											<div>
												<label>SKU: </label>
												<span class="info-deta"><?= $arResult['PROPERTIES']['ARTNUMBER']['VALUE'] ?></span>
											</div>
										</div>
									</div>

									<p data-value="description" itemprop="description">

										<?
										if (
											$arResult['PREVIEW_TEXT'] != ''
											&& (
												$arParams['DISPLAY_PREVIEW_TEXT_MODE'] === 'S'
												|| ($arParams['DISPLAY_PREVIEW_TEXT_MODE'] === 'E' && $arResult['DETAIL_TEXT'] == '')
											)
										)
										{
											echo $arResult['PREVIEW_TEXT_TYPE'] === 'html' ? $arResult['PREVIEW_TEXT'] : '<p>' . $arResult['PREVIEW_TEXT'] . '</p>';
										}

										if ($arResult['DETAIL_TEXT'] != '')
										{
											echo $arResult['DETAIL_TEXT_TYPE'] === 'html' ? $arResult['DETAIL_TEXT'] : '<p>' . $arResult['DETAIL_TEXT'] . '</p>';
										}
										?>

									</p>


									<div class="product-dopinfo">
										<ul>
											<li>
												<a href="javascript:void(0);">Уточнить возможную скидку</a>
											</li>
											<li>
												<a href="javascript:void(0);">Скачать инструкцию</a>
											</li>
											<li>
												<a href="javascript:void(0);" class="product-dopinfo__button popup_help" data-content="Добавить в избранное" data-variation="large"><i
															class="fa fa-heart-o"></i></a>
											</li>
											<li>

												<?
												$jsSelectColor = '';
												if (isset($_SESSION[$arParams["COMPARE_NAME"]][$arResult["IBLOCK_ID"]]["ITEMS"][$arResult['ID']]))
												{
													$jsSelectColor = ' js-select-color';
												}
												?>
												<label style="cursor: pointer;margin: 0" id="<?= $itemIds['COMPARE_LINK'] ?>" data-compare="yes"
												       class="product-item__button popup_help<?= $jsSelectColor ?>">
													<i class="fa fa-balance-scale"></i>
													<input style="display:none;" data-entity="compare-checkbox" type="checkbox">
												</label>
											</li>
											<li>
												<a href="javascript:void(0);" class="product-dopinfo__button popup_help"
												   data-html="<ul><b>Поделиться:</b><li>Вконтакте&nbsp<i class=' fa fa-vk '> </i></li></ul>" data-variation="large"><i
															class="fa fa-thumbs-o-up" aria-hidden="true"></i></a>
											</li>
										</ul>
									</div>
									<div class="row product-contacts">


										<div class="col-lg-6">

											<? $APPLICATION->IncludeComponent(
												"bitrix:main.include",
												"",
												Array(
													"AREA_FILE_SHOW" => "file",
													"PATH" => SITE_DIR . "include/getconsult.php",
													"AREA_FILE_RECURSIVE" => "N",
													"EDIT_MODE" => "php",
												),
												false,
												Array('HIDE_ICONS' => 'N')
											); ?>


										</div>


										<div class="col-lg-6">
											<div class="product-feedback">
												<a href="#orderCall" class="button basic popup">Остались вопросы? Напишите нам!</a>
											</div>
										</div>


									</div>

									<?
									if ($haveOffers && !empty($arResult['OFFERS_PROP']))
									{
										?>
										<div class="mb-30" id="<?= $itemIds['TREE_ID'] ?>">
											<?
											foreach ($arResult['SKU_PROPS'] as $skuProperty)
											{
												if (!isset($arResult['OFFERS_PROP'][$skuProperty['CODE']]))
													continue;

												$propertyId = $skuProperty['ID'];
												$skuProps[] = array(
													'ID' => $propertyId,
													'SHOW_MODE' => $skuProperty['SHOW_MODE'],
													'VALUES' => $skuProperty['VALUES'],
													'VALUES_COUNT' => $skuProperty['VALUES_COUNT']
												);
												?>
												<div class="col-md-6 product-size select-arrow mb-20" data-entity="sku-line-block">

													<label><?= htmlspecialcharsEx($skuProperty['NAME']) ?></label>
													<div class="product-item-scu-container">
														<div class="product-item-scu-block">
															<div class="product-item-scu-list">
																<ul class="product-item-scu-item-list">
																	<?
																	foreach ($skuProperty['VALUES'] as &$value)
																	{
																		$value['NAME'] = htmlspecialcharsbx($value['NAME']);


																		?>
																		<li class="product-item-scu-item-text-container" title="<?= $value['NAME'] ?>"
																		    data-treevalue="<?= $propertyId ?>_<?= $value['ID'] ?>"
																		    data-onevalue="<?= $value['ID'] ?>">
																			<div class="product-item-scu-item-text-block">
																				<div class="product-item-scu-item-text"><?= $value['NAME'] ?></div>
																			</div>
																		</li>
																		<?

																	}
																	?>
																</ul>
																<div style="clear: both;"></div>
															</div>
														</div>
													</div>
												</div>
												<?
											}
											?>
										</div>
										<?
									} ?>


									<div class="mb-30">

										<div class="product-qty" style="<?= (!$actualItem['CAN_BUY'] ? 'display: none;' : '') ?>" data-entity="quantity-block">
											<label for="qty">Кол-во:</label>
											<div class="custom-qty">


												<button class="reduced items" id="<?= $itemIds['QUANTITY_DOWN_ID'] ?>" type="button"><i class="fa fa-minus"></i></button>
												<input class="input-text qty" id="<?= $itemIds['QUANTITY_ID'] ?>" type="tel" value="<?= $price['MIN_QUANTITY'] ?>"/>
												<button class="increase items" id="<?= $itemIds['QUANTITY_UP_ID'] ?>" type="button"><i class="fa fa-plus"></i></button>
											</div>
										</div>


										<? if ($arResult['PRODUCT']['QUANTITY'] > 0)
										{ ?>
											<div class="bottom-detail cart-button product-buttons">
												<ul>


													<li>

														<a href="#!" class="button yellow">Купить</a>

													</li>


													<li class="pro-cart-icon">
														<form class="product-item-detail-short-card-btn"
														      style="display: <?= ($actualItem['CAN_BUY'] ? '' : 'none') ?>;" id="<?= $itemIds['BASKET_ACTIONS_ID'] ?>"
														      style="display: <?= ($actualItem['CAN_BUY'] ? '' : 'none') ?>;">
															<a href="javascript:void(0);" id="<?= $itemIds['ADD_BASKET_LINK'] ?>"
															   class="<?= $showButtonClassName ?> button blue basic"><span></span><?= $arParams['MESS_BTN_ADD_TO_BASKET'] ?></a>

														</form>
													</li>


													<? if ($arParams['DISPLAY_COMPARE'])
													{
														?>
														<li>
															<a id="<?= $itemIds['COMPARE_LINK'] ?>" href="#" class="button grey basic">Сравнить
																<!--<input type="checkbox" data-entity="compare-checkbox">
     						<span data-entity="compare-title"><?= $arParams['MESS_BTN_COMPARE'] ?></span>
							  -->

															</a>
														</li>
														<?
													}
													?>

												</ul>
											</div>
										<? } ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<section class="pb-30">
		<div class="container">
			<div class="product-detail-tab">
				<div class="row">
					<div class="col-md-12">
						<div id="tabs">
							<ul class="nav nav-tabs">
								<li><a class="tab-Description selected" title="Description">Описание</a></li>
								<li><a class="tab-Product-Tags" title="Product-Tags">Спецификация</a></li>
								<li><a class="tab-Reviews" title="Reviews">Отзывы</a></li>
								<li class="right-side"><a class="tab-Delivery" title="Delivery">Доставка</a></li>
								<li class="right-side"><a class="tab-Payment" title="Payment">Оплата</a></li>
								<li class="right-side"><a class="tab-Garant" title="Garant">Гарантия</a></li>
							</ul>
						</div>
						<div id="items">
							<div class="tab_content">
								<ul>
									<li>
										<div class="items-Description selected">
											<div class="Description">

												<?

												if ($arResult['DETAIL_TEXT'] != '')
												{
													echo $arResult['DETAIL_TEXT_TYPE'] === 'html' ? $arResult['DETAIL_TEXT'] : '<p>' . $arResult['DETAIL_TEXT'] . '</p>';
												}

												?>


											</div>
										</div>
									</li>


									<li>
										<div class="items-Product-Tags">


											<dl class="product-item-detail-properties">
												<?
												foreach ($arResult['DISPLAY_PROPERTIES'] as $property)
												{
													?>
													<dt><?= $property['NAME'] ?></dt>
													<dd><?= (
														is_array($property['DISPLAY_VALUE'])
															? implode(' / ', $property['DISPLAY_VALUE'])
															: $property['DISPLAY_VALUE']
														) ?>
													</dd>
													<?
												}
												unset($property);
												?>
											</dl>


									</li>


									<li>
										<div class="items-Reviews">


											<div id="disqus_thread"></div>
											<script>

                                                /**
                                                 *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                                                 *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/

                                                var disqus_config = function () {
                                                    this.page.url = "http://watch.dev17.g-lab.ru<?=$APPLICATION->GetCurDir()?>";  // Replace PAGE_URL with your page's canonical URL variable
                                                    this.page.identifier = "<?=md5($APPLICATION->GetCurDir())?>"; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                                                };

                                                (function () { // DON'T EDIT BELOW THIS LINE
                                                    var d = document, s = d.createElement('script');
                                                    s.src = 'https://watch-dev17-g-lab-ru.disqus.com/embed.js';
                                                    s.setAttribute('data-timestamp', +new Date());
                                                    (d.head || d.body).appendChild(s);
                                                })();
											</script>
											<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>


											<!--
											  <div class="comments-area">
												<h4>Comments<span>(2)</span></h4>
												<ul class="comment-list mt-30">
												  <li>
													<div class="comment-user"> <img src="images/comment-user.jpg" alt="Tizzy"> </div>
													<div class="comment-detail">
													  <div class="user-name">John Doe</div>
													  <div class="post-info">
														<ul>
														  <li>Fab 11, 2016</li>
														  <li><a href="#"><i class="fa fa-reply"></i>Reply</a></li>
														</ul>
													  </div>
													  <p>Consectetur adipiscing elit integer sit amet augue laoreet maximus nuncac.</p>
													</div>
													<ul class="comment-list child-comment">
													  <li>
														<div class="comment-user"> <img src="images/comment-user.jpg" alt="Tizzy"> </div>
														<div class="comment-detail">
														  <div class="user-name">John Doe</div>
														  <div class="post-info">
															<ul>
															  <li>Fab 11, 2016</li>
															  <li><a href="#"><i class="fa fa-reply"></i>Reply</a></li>
															</ul>
														  </div>
														  <p>Consectetur adipiscing elit integer sit amet augue laoreet maximus nuncac.</p>
														</div>
													  </li>
													  <li>
														<div class="comment-user"> <img src="images/comment-user.jpg" alt="Tizzy"> </div>
														<div class="comment-detail">
														  <div class="user-name">John Doe</div>
														  <div class="post-info">
															<ul>
															  <li>Fab 11, 2016</li>
															  <li><a href="#"><i class="fa fa-reply"></i>Reply</a></li>
															</ul>
														  </div>
														  <p>Consectetur adipiscing elit integer sit amet augue laoreet maximus nuncac.</p>
														</div>
													  </li>
													</ul>
												  </li>
												  <li>
													<div class="comment-user"> <img src="images/comment-user.jpg" alt="Tizzy"> </div>
													<div class="comment-detail">
													  <div class="user-name">John Doe</div>
													  <div class="post-info">
														<ul>
														  <li>Fab 11, 2016</li>
														  <li><a href="#"><i class="fa fa-reply"></i>Reply</a></li>
														</ul>
													  </div>
													  <p>Consectetur adipiscing elit integer sit amet augue laoreet maximus nuncac.</p>
													</div>
												  </li>
												</ul>
											  </div>
											  <div class="main-form mt-30">
												<h4>Leave a comments</h4>
												<div class="row mt-30">
												  <form >
													<div class="col-sm-4 mb-30">
													  <input type="text" placeholder="Name" required>
													</div>
													<div class="col-sm-4 mb-30">
													  <input type="email" placeholder="Email" required>
													</div>
													<div class="col-sm-4 mb-30">
													  <input type="text" placeholder="Website" required>
													</div>
													<div class="col-xs-12 mb-30">
													  <textarea cols="30" rows="3" placeholder="Message" required></textarea>
													</div>
													<div class="col-xs-12 mb-30">
													  <button class="btn-black" name="submit" type="submit">Submit</button>
													</div>
												  </form>
												</div>
											  </div>
											  -->

										</div>
									</li>
									<li>
										<div class="items-Garant">
											<div class="Garant">

												<? $APPLICATION->IncludeComponent(
													"bitrix:main.include",
													"",
													Array(
														"AREA_FILE_SHOW" => "file",
														"PATH" => SITE_DIR . "include/garant.php",
														"AREA_FILE_RECURSIVE" => "N",
														"EDIT_MODE" => "php",
													),
													false,
													Array('HIDE_ICONS' => 'N')
												); ?>


											</div>
										</div>
									</li>
									<li>
										<div class="items-Delivery">
											<div class="Delivery">


												<? $APPLICATION->IncludeComponent(
													"bitrix:main.include",
													"",
													Array(
														"AREA_FILE_SHOW" => "file",
														"PATH" => SITE_DIR . "include/delivery.php",
														"AREA_FILE_RECURSIVE" => "N",
														"EDIT_MODE" => "php",
													),
													false,
													Array('HIDE_ICONS' => 'N')
												); ?>

											</div>
										</div>
									</li>
									<li>
										<div class="items-Payment">
											<div class="Payment">
												<? $APPLICATION->IncludeComponent(
													"bitrix:main.include",
													"",
													Array(
														"AREA_FILE_SHOW" => "file",
														"PATH" => SITE_DIR . "include/payment.php",
														"AREA_FILE_RECURSIVE" => "N",
														"EDIT_MODE" => "php",
													),
													false,
													Array('HIDE_ICONS' => 'N')
												); ?>

											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<meta itemprop="name" content="<?= $name ?>"/>
	<meta itemprop="category" content="<?= $arResult['CATEGORY_PATH'] ?>"/>
<?
if ($haveOffers)
{
	foreach ($arResult['JS_OFFERS'] as $offer)
	{
		$currentOffersList = array();

		if (!empty($offer['TREE']) && is_array($offer['TREE']))
		{
			foreach ($offer['TREE'] as $propName => $skuId)
			{
				$propId = (int)substr($propName, 5);

				foreach ($skuProps as $prop)
				{
					if ($prop['ID'] == $propId)
					{
						foreach ($prop['VALUES'] as $propId => $propValue)
						{
							if ($propId == $skuId)
							{
								$currentOffersList[] = $propValue['NAME'];
								break;
							}
						}
					}
				}
			}
		}

		$offerPrice = $offer['ITEM_PRICES'][$offer['ITEM_PRICE_SELECTED']];
		?>
		<span itemprop="offers" itemscope itemtype="http://schema.org/Offer">
				<meta itemprop="sku" content="<?= htmlspecialcharsbx(implode('/', $currentOffersList)) ?>"/>
				<meta itemprop="price" content="<?= $offerPrice['RATIO_PRICE'] ?>"/>
				<meta itemprop="priceCurrency" content="<?= $offerPrice['CURRENCY'] ?>"/>
				<link itemprop="availability" href="http://schema.org/<?= ($offer['CAN_BUY'] ? 'InStock' : 'OutOfStock') ?>"/>
			</span>
		<?
	}

	unset($offerPrice, $currentOffersList);
} else
{
	?>
	<span itemprop="offers" itemscope itemtype="http://schema.org/Offer">
			<meta itemprop="price" content="<?= $price['RATIO_PRICE'] ?>"/>
			<meta itemprop="priceCurrency" content="<?= $price['CURRENCY'] ?>"/>
			<link itemprop="availability" href="http://schema.org/<?= ($actualItem['CAN_BUY'] ? 'InStock' : 'OutOfStock') ?>"/>
		</span>
	<?
}
?>

<?
if ($haveOffers)
{
	$offerIds = array();
	$offerCodes = array();

	$useRatio = $arParams['USE_RATIO_IN_RANGES'] === 'Y';

	foreach ($arResult['JS_OFFERS'] as $ind => &$jsOffer)
	{
		$offerIds[] = (int)$jsOffer['ID'];
		$offerCodes[] = $jsOffer['CODE'];

		$fullOffer = $arResult['OFFERS'][$ind];
		$measureName = $fullOffer['ITEM_MEASURE']['TITLE'];

		$strAllProps = '';
		$strMainProps = '';
		$strPriceRangesRatio = '';
		$strPriceRanges = '';

		if ($arResult['SHOW_OFFERS_PROPS'])
		{
			if (!empty($jsOffer['DISPLAY_PROPERTIES']))
			{
				foreach ($jsOffer['DISPLAY_PROPERTIES'] as $property)
				{
					$current = '<dt>' . $property['NAME'] . '</dt><dd>' . (
						is_array($property['VALUE'])
							? implode(' / ', $property['VALUE'])
							: $property['VALUE']
						) . '</dd>';
					$strAllProps .= $current;

					if (isset($arParams['MAIN_BLOCK_OFFERS_PROPERTY_CODE'][$property['CODE']]))
					{
						$strMainProps .= $current;
					}
				}

				unset($current);
			}
		}

		if ($arParams['USE_PRICE_COUNT'] && count($jsOffer['ITEM_QUANTITY_RANGES']) > 1)
		{
			$strPriceRangesRatio = '(' . Loc::getMessage(
					'CT_BCE_CATALOG_RATIO_PRICE',
					array('#RATIO#' => ($useRatio
							? $fullOffer['ITEM_MEASURE_RATIOS'][$fullOffer['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']
							: '1'
						) . ' ' . $measureName)
				) . ')';

			foreach ($jsOffer['ITEM_QUANTITY_RANGES'] as $range)
			{
				if ($range['HASH'] !== 'ZERO-INF')
				{
					$itemPrice = false;

					foreach ($jsOffer['ITEM_PRICES'] as $itemPrice)
					{
						if ($itemPrice['QUANTITY_HASH'] === $range['HASH'])
						{
							break;
						}
					}

					if ($itemPrice)
					{
						$strPriceRanges .= '<dt>' . Loc::getMessage(
								'CT_BCE_CATALOG_RANGE_FROM',
								array('#FROM#' => $range['SORT_FROM'] . ' ' . $measureName)
							) . ' ';

						if (is_infinite($range['SORT_TO']))
						{
							$strPriceRanges .= Loc::getMessage('CT_BCE_CATALOG_RANGE_MORE');
						} else
						{
							$strPriceRanges .= Loc::getMessage(
								'CT_BCE_CATALOG_RANGE_TO',
								array('#TO#' => $range['SORT_TO'] . ' ' . $measureName)
							);
						}

						$strPriceRanges .= '</dt><dd>' . ($useRatio ? $itemPrice['PRINT_RATIO_PRICE'] : $itemPrice['PRINT_PRICE']) . '</dd>';
					}
				}
			}

			unset($range, $itemPrice);
		}

		$jsOffer['DISPLAY_PROPERTIES'] = $strAllProps;
		$jsOffer['DISPLAY_PROPERTIES_MAIN_BLOCK'] = $strMainProps;
		$jsOffer['PRICE_RANGES_RATIO_HTML'] = $strPriceRangesRatio;
		$jsOffer['PRICE_RANGES_HTML'] = $strPriceRanges;
	}

	$templateData['OFFER_IDS'] = $offerIds;
	$templateData['OFFER_CODES'] = $offerCodes;
	unset($jsOffer, $strAllProps, $strMainProps, $strPriceRanges, $strPriceRangesRatio, $useRatio);

	$jsParams = array(
		'CONFIG' => array(
			'USE_CATALOG' => $arResult['CATALOG'],
			'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
			'SHOW_PRICE' => true,
			'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'] === 'Y',
			'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'] === 'Y',
			'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
			'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
			'SHOW_SKU_PROPS' => $arResult['SHOW_OFFERS_PROPS'],
			'OFFER_GROUP' => $arResult['OFFER_GROUP'],
			'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
			'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
			'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'] === 'Y',
			'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
			'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
			'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
			'USE_STICKERS' => true,
			'USE_SUBSCRIBE' => $showSubscribe,
			'SHOW_SLIDER' => $arParams['SHOW_SLIDER'],
			'SLIDER_INTERVAL' => $arParams['SLIDER_INTERVAL'],
			'ALT' => $alt,
			'TITLE' => $title,
			'MAGNIFIER_ZOOM_PERCENT' => 200,
			'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
			'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
			'BRAND_PROPERTY' => !empty($arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']])
				? $arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]['DISPLAY_VALUE']
				: null
		),
		'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
		'VISUAL' => $itemIds,
		'DEFAULT_PICTURE' => array(
			'PREVIEW_PICTURE' => $arResult['DEFAULT_PICTURE'],
			'DETAIL_PICTURE' => $arResult['DEFAULT_PICTURE']
		),
		'PRODUCT' => array(
			'ID' => $arResult['ID'],
			'ACTIVE' => $arResult['ACTIVE'],
			'NAME' => $arResult['~NAME'],
			'CATEGORY' => $arResult['CATEGORY_PATH']
		),
		'BASKET' => array(
			'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
			'BASKET_URL' => $arParams['BASKET_URL'],
			'SKU_PROPS' => $arResult['OFFERS_PROP_CODES'],
			'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
			'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
		),
		'OFFERS' => $arResult['JS_OFFERS'],
		'OFFER_SELECTED' => $arResult['OFFERS_SELECTED'],
		'TREE_PROPS' => $skuProps
	);
} else
{
	$emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
	if ($arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y' && !$emptyProductProperties)
	{
		?>
		<div id="<?= $itemIds['BASKET_PROP_DIV'] ?>" style="display: none;">
			<?
			if (!empty($arResult['PRODUCT_PROPERTIES_FILL']))
			{
				foreach ($arResult['PRODUCT_PROPERTIES_FILL'] as $propId => $propInfo)
				{
					?>
					<input type="hidden" name="<?= $arParams['PRODUCT_PROPS_VARIABLE'] ?>[<?= $propId ?>]" value="<?= htmlspecialcharsbx($propInfo['ID']) ?>">
					<?
					unset($arResult['PRODUCT_PROPERTIES'][$propId]);
				}
			}

			$emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
			if (!$emptyProductProperties)
			{
				?>
				<table>
					<?
					foreach ($arResult['PRODUCT_PROPERTIES'] as $propId => $propInfo)
					{
						?>
						<tr>
							<td><?= $arResult['PROPERTIES'][$propId]['NAME'] ?></td>
							<td>
								<?
								if (
									$arResult['PROPERTIES'][$propId]['PROPERTY_TYPE'] === 'L'
									&& $arResult['PROPERTIES'][$propId]['LIST_TYPE'] === 'C'
								)
								{
									foreach ($propInfo['VALUES'] as $valueId => $value)
									{
										?>
										<label>
											<input type="radio" name="<?= $arParams['PRODUCT_PROPS_VARIABLE'] ?>[<?= $propId ?>]"
											       value="<?= $valueId ?>" <?= ($valueId == $propInfo['SELECTED'] ? '"checked"' : '') ?>>
											<?= $value ?>
										</label>
										<br>
										<?
									}
								} else
								{
									?>
									<select name="<?= $arParams['PRODUCT_PROPS_VARIABLE'] ?>[<?= $propId ?>]">
										<?
										foreach ($propInfo['VALUES'] as $valueId => $value)
										{
											?>
											<option value="<?= $valueId ?>" <?= ($valueId == $propInfo['SELECTED'] ? '"selected"' : '') ?>>
												<?= $value ?>
											</option>
											<?
										}
										?>
									</select>
									<?
								}
								?>
							</td>
						</tr>
						<?
					}
					?>
				</table>
				<?
			}
			?>
		</div>
		<?
	}

	$jsParams = array(
		'CONFIG' => array(
			'USE_CATALOG' => $arResult['CATALOG'],
			'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
			'SHOW_PRICE' => !empty($arResult['ITEM_PRICES']),
			'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'] === 'Y',
			'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'] === 'Y',
			'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
			'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
			'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
			'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
			'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'] === 'Y',
			'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
			'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
			'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
			'USE_STICKERS' => true,
			'USE_SUBSCRIBE' => $showSubscribe,
			'ALT' => $alt,
			'TITLE' => $title,
			'MAGNIFIER_ZOOM_PERCENT' => 200,
			'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
			'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
			'BRAND_PROPERTY' => !empty($arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']])
				? $arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]['DISPLAY_VALUE']
				: null
		),
		'VISUAL' => $itemIds,
		'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
		'PRODUCT' => array(
			'ID' => $arResult['ID'],
			'ACTIVE' => $arResult['ACTIVE'],
			'PICT' => reset($arResult['MORE_PHOTO']),
			'NAME' => $arResult['~NAME'],
			'SUBSCRIPTION' => true,
			'ITEM_PRICE_MODE' => $arResult['ITEM_PRICE_MODE'],
			'ITEM_PRICES' => $arResult['ITEM_PRICES'],
			'ITEM_PRICE_SELECTED' => $arResult['ITEM_PRICE_SELECTED'],
			'ITEM_QUANTITY_RANGES' => $arResult['ITEM_QUANTITY_RANGES'],
			'ITEM_QUANTITY_RANGE_SELECTED' => $arResult['ITEM_QUANTITY_RANGE_SELECTED'],
			'ITEM_MEASURE_RATIOS' => $arResult['ITEM_MEASURE_RATIOS'],
			'ITEM_MEASURE_RATIO_SELECTED' => $arResult['ITEM_MEASURE_RATIO_SELECTED'],
			'SLIDER_COUNT' => $arResult['MORE_PHOTO_COUNT'],
			'SLIDER' => $arResult['MORE_PHOTO'],
			'CAN_BUY' => $arResult['CAN_BUY'],
			'CHECK_QUANTITY' => $arResult['CHECK_QUANTITY'],
			'QUANTITY_FLOAT' => is_float($arResult['ITEM_MEASURE_RATIOS'][$arResult['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']),
			'MAX_QUANTITY' => $arResult['CATALOG_QUANTITY'],
			'STEP_QUANTITY' => $arResult['ITEM_MEASURE_RATIOS'][$arResult['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'],
			'CATEGORY' => $arResult['CATEGORY_PATH']
		),
		'BASKET' => array(
			'ADD_PROPS' => $arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y',
			'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
			'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
			'EMPTY_PROPS' => $emptyProductProperties,
			'BASKET_URL' => $arParams['BASKET_URL'],
			'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
			'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
		)
	);
	unset($emptyProductProperties);
}

if ($arParams['DISPLAY_COMPARE'])
{
	$jsParams['COMPARE'] = array(
		'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
		'COMPARE_DELETE_URL_TEMPLATE' => $arResult['~COMPARE_DELETE_URL_TEMPLATE'],
		'COMPARE_PATH' => $arParams['COMPARE_PATH']
	);
}
?>
	<script>
        BX.message({
            ECONOMY_INFO_MESSAGE: '<?=GetMessageJS('CT_BCE_CATALOG_ECONOMY_INFO2')?>',
            TITLE_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_TITLE_ERROR')?>',
            TITLE_BASKET_PROPS: '<?=GetMessageJS('CT_BCE_CATALOG_TITLE_BASKET_PROPS')?>',
            BASKET_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR')?>',
            BTN_SEND_PROPS: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_SEND_PROPS')?>',
            BTN_MESSAGE_BASKET_REDIRECT: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_BASKET_REDIRECT')?>',
            BTN_MESSAGE_CLOSE: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE')?>',
            BTN_MESSAGE_CLOSE_POPUP: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE_POPUP')?>',
            TITLE_SUCCESSFUL: '<?=GetMessageJS('CT_BCE_CATALOG_ADD_TO_BASKET_OK')?>',
            COMPARE_MESSAGE_OK: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_OK')?>',
            COMPARE_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_UNKNOWN_ERROR')?>',
            COMPARE_TITLE: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_TITLE')?>',
            BTN_MESSAGE_COMPARE_REDIRECT: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT')?>',
            PRODUCT_GIFT_LABEL: '<?=GetMessageJS('CT_BCE_CATALOG_PRODUCT_GIFT_LABEL')?>',
            PRICE_TOTAL_PREFIX: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_PRICE_TOTAL_PREFIX')?>',
            RELATIVE_QUANTITY_MANY: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_MANY'])?>',
            RELATIVE_QUANTITY_FEW: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_FEW'])?>',
            SITE_ID: '<?=SITE_ID?>'
        });

        var <?=$obName?> =
        new JCCatalogElement(<?=CUtil::PhpToJSObject($jsParams, false, true)?>);

	</script>
<?
unset($actualItem, $itemIds, $jsParams);