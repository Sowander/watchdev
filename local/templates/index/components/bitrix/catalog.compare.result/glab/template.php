<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$isAjax = ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST["ajax_action"]) && $_POST["ajax_action"] == "Y");
$countProducts = count($arResult['ITEMS']);
$arResult['JS-STRING'] = ' ';
?>
<section class="ptb-55 compare-page" id="bx_catalog_compare_block">
	<? if ($isAjax)
	{
		$APPLICATION->RestartBuffer();
	}
	?>
	<div class="container">
		<div class="row compare-row">


			<div class="col-xs-2">
				<div class="compare-name">
					<p>
						<?= $countProducts . " " . getWordCase($countProducts, 'Товар', 'a', 'ов') ?><i class="fa fa-arrow-right" aria-hidden="true"></i>
					</p>
				</div>
			</div>
			<div class="col-xs-10">
				<?
				global $compareFilter;

				$compareFilter = array(
					"ID" => $arResult['compareFilter'],
				);

				$APPLICATION->IncludeComponent(
					"bitrix:catalog.section",
					"glabcompare",
					array(
						"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
						"IBLOCK_ID" => $arParams["IBLOCK_ID"],
						"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
						"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
						"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
						"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
						"PAGE_ELEMENT_COUNT" => '100',
						"LINE_ELEMENT_COUNT" => '100',
						"PROPERTY_CODE" => $arParams["PROPERTY_CODE"],
						"OFFERS_CART_PROPERTIES" => array(
							0 => "COLOR_REF",
							1 => "SIZES_SHOES",
							2 => "SIZES_CLOTHES",
						),
						"OFFERS_FIELD_CODE" => "",
						"OFFERS_PROPERTY_CODE" => $arParams["OFFERS_PROPERTY_CODE"],
						"OFFERS_SORT_FIELD" => "id",
						"OFFERS_SORT_ORDER" => "asc",
						"OFFERS_SORT_FIELD2" => "id",
						"OFFERS_SORT_ORDER2" => "asc",
						"OFFERS_LIMIT" => "5",
						"SECTION_URL" => "",
						"DETAIL_URL" => "",
						"BASKET_URL" => $arParams["BASKET_URL"],
						"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
						"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
						"PRODUCT_QUANTITY_VARIABLE" => "quantity",
						"PRODUCT_PROPS_VARIABLE" => "prop",
						"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "36000000",
						"DISPLAY_COMPARE" => "N",
						"PRICE_CODE" => $arParams["PRICE_CODE"],
						"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
						"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
						"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
						"PRODUCT_PROPERTIES" => array(),
						"USE_PRODUCT_QUANTITY" => "Y",
						"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
						"CURRENCY_ID" => $arParams["CURRENCY_ID"],
						"HIDE_NOT_AVAILABLE" => "N",
						"DISPLAY_TOP_PAGER" => "N",
						"DISPLAY_BOTTOM_PAGER" => "N",
						"FILTER_NAME" => "compareFilter",
						"SECTION_ID" => "",
						"SECTION_CODE" => "",
						"SECTION_USER_FIELDS" => array(),
						"INCLUDE_SUBSECTIONS" => "Y",
						"SHOW_ALL_WO_SECTION" => "Y",
						"META_KEYWORDS" => "",
						"META_DESCRIPTION" => "",
						"BROWSER_TITLE" => "",
						"ADD_SECTIONS_CHAIN" => "N",
						"SET_TITLE" => "N",
						"SET_STATUS_404" => "N",
						"CACHE_FILTER" => "N",
						"CACHE_GROUPS" => "N",
						"SHOW_OLD_PRICE" => "Y"
					),
					$arResult["THEME_COMPONENT"],
					array('HIDE_ICONS' => 'Y')
				); ?>

			</div>
			<div class="col-md-12">
				<div class="compare-table-wrapper">
					<table class="compare-table">
						<? foreach ($arResult['JSstrName'] as $i => $value): ?>
							<tr id="comp-<?= $i ?>"></tr>
						<? endforeach; ?>
					</table>
				</div>
			</div>


		</div>
	</div>
	<script type="text/javascript">
        var compName = [];
        var compResult = [];
        var compareItem = 0;
        var compareItems;
        var productsCompare = [];
        var productCompare = [];
        var iprice;

        var navig;

        $(document).ready(function () {
            compName = <?=CUtil::PhpToJSObject($arResult['JSstrName']);?>;
            compResult = <?=CUtil::PhpToJSObject($arResult['JSresult']);?>;
            compareItems = 0;


            for (var id in compResult) {
                productCompare = [];
                console.log(id);
                iprice = $('#' + id).attr('data-iprice');
                console.log(iprice);
                productCompare.push(iprice);
                for (var name in compResult[id]) {
                    productCompare.push(compResult[id][name])
                }
                productsCompare.push(productCompare);
                compareItems++;
            }
            console.log(productsCompare);

            switch (true) {
                case +compareItems >= 4:
                    compareItem = 4;
                    for (var i = 0; i < compName.length; i++) {
                        $('#comp-' + i).html(
                            '<td>' + compName[i] + '</td>' +
                            '<td>' + productsCompare[0][i] + '</td>' +
                            '<td>' + productsCompare[1][i] + '</td>' +
                            '<td>' + productsCompare[2][i] + '</td>' +
                            '<td>' + productsCompare[3][i] + '</td>'
                        );
                    }
                    break;
                case +compareItems == 3:
                    compareItem = 3;
                    for (var i = 0; i < compName.length; i++) {
                        $('#comp-' + i).html(
                            '<td>' + compName[i] + '</td>' +
                            '<td>' + productsCompare[0][i] + '</td>' +
                            '<td>' + productsCompare[1][i] + '</td>' +
                            '<td>' + productsCompare[2][i] + '</td>'
                        );
                    }
                    break;
                case +compareItems == 2:
                    compareItem = 2;
                    for (var i = 0; i < compName.length; i++) {
                        $('#comp-' + i).html(
                            '<td>' + compName[i] + '</td>' +
                            '<td>' + productsCompare[0][i] + '</td>' +
                            '<td>' + productsCompare[1][i] + '</td>'
                        );
                    }
                    break;
            }
            var items = 4;
            if (compareItems > 4) {
                navig = true;
            } else {
                navig = false;
            }

            $('.pro_compare_slider').owlCarousel({
                nav: navig,
                items: items
            });
            $('.pro_compare_slider').on('changed.owl.carousel', function (event) {
                var size = event.page.size;
                console.log(event);
                var item = event.item.index;
                console.log(item);
                for (var i = 0; i < compName.length; i++) {
                    for (var im = 0; im < size; im++) {
                        $('#comp-' + i).html(
                            '<td>' + compName[i] + '</td>' +
                            '<td>' + productsCompare[item][i] + '</td>' +
                            '<td>' + productsCompare[item + 1][i] + '</td>' +
                            '<td>' + productsCompare[item + 2][i] + '</td>' +
                            '<td>' + productsCompare[item + 3][i] + '</td>'
                        );
                    }
                }
            });
            $('.remove-compare').click(function () {
                id = $(this).attr('data-id');
                $('#' + id).trigger('click');
            });
        });
	</script>
	<div class="hidden">
		<? foreach ($arResult['ITEMS'] as $arElement): ?>

			<a id= <?= "del" . $arElement['ID'] ?> onclick="CatalogCompareObj.MakeAjaxAction('<?= CUtil::JSEscape($arElement['~DELETE_URL']) ?>');"
			   href="javascript:void(0)"><?= $arElement['ID'] ?></a>
		<? endforeach; ?>

	</div>
	<?
	if ($isAjax)
	{
		die();
	}
	?>
</section>
<script type="text/javascript">
    var CatalogCompareObj = new BX.Iblock.Catalog.CompareClass("bx_catalog_compare_block");
</script>