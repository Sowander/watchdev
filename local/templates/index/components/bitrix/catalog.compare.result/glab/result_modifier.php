<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @global array $arParams */
use Bitrix\Main\Type\Collection;

$arJStable = array();
$arResult['JS-STRING'] = '';
$arJStable[] = 'Цена';
$compareFilter = array();

if (is_array($arResult["ITEMS"]))
	foreach ($arResult["ITEMS"] as $arElement)
{
	Collection::sortByColumn($arElement["DISPLAY_PROPERTIES"], array('SORT' => SORT_ASC));
	$compareFilter[] = intval($arElement['ID']);
	foreach ($arElement["DISPLAY_PROPERTIES"] as $code => $value)
	{

		if (!in_array($value['NAME'], $arJStable))
			$arJStable[] = $value['NAME'];

		if (strlen($value['VALUE']) > 2)
		{
			$arJSresult['id' . $arElement['ID']][$value['NAME']] = $value['VALUE'];
		} else
		{
			$arJSresult['id' . $arElement['ID']][$value['NAME']] = 'нет';
		}

	}


}
$arResult['JSstrName'] = $arJStable;
$arResult['JSresult'] = $arJSresult;
$arResult['compareFilter'] = $compareFilter;