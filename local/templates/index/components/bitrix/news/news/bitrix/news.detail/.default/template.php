<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<div class="row">
	<div class="col-xs-12 mb-60">
		<div class="blog-detail single-blog ">
			<div class="post-info">
				<ul>
					<li><span class="post-date"><?= $arResult["DISPLAY_ACTIVE_FROM"] ?></span></li>
					<!--  <li><span>By</span><a href="#"> cormon jons</a></li> -->
				</ul>
			</div>
			<h3><?= $arResult["NAME"] ?></h3>
			<p> <? echo $arResult["DETAIL_TEXT"]; ?></p>
		</div>
	</div>
</div>
