<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="blog-listing">
	<div class="row">


		<? foreach ($arResult["ITEMS"] as $arItem): ?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<?
			#print_r($arItem);
			?>
			<div class="col-xs-12" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
				<a class="blog-item" href="<? echo $arItem["DETAIL_PAGE_URL"] ?>">
					<? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"]))
					{ ?>
						<div class="blog-media">
							<div class="read"><img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt="<?= $arItem[NAME] ?>">&nbsp;</div>
						</div>
					<? } ?>
					<div class="blog-detail">
						<h3><? echo $arItem["NAME"]; ?></h3>
						<div class="post-info">
							<span class="post-date"><? echo $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
							<div class="post-autor"><span>Автор:</span> <?= $arItem[FIELDS][CREATED_USER_NAME] ?></div>
						</div>

						<p><? echo $arItem["PREVIEW_TEXT"]; ?></p>
						<span class="button blue">Подробнее</span>
					</div>
				</a>
			</div>


		<? endforeach; ?>

	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="pagination-bar">
				<?= $arResult["NAV_STRING"] ?>

			</div>
		</div>
	</div>
</div>


