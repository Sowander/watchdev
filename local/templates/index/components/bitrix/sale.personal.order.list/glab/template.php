<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\Page\Asset;

Asset::getInstance()->addJs("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/script.js");
Asset::getInstance()->addCss("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/style.css");
$this->addExternalCss("/bitrix/css/main/bootstrap.css");
CJSCore::Init(array('clipboard', 'fx'));

Loc::loadMessages(__FILE__);

if (!empty($arResult['ERRORS']['FATAL']))
{
	foreach ($arResult['ERRORS']['FATAL'] as $error)
	{
		ShowError($error);
	}
	$component = $this->__component;
	if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED]))
	{
		$APPLICATION->AuthForm('', false, false, 'N', false);
	}

} else
{
	if (!empty($arResult['ERRORS']['NONFATAL']))
	{
		foreach ($arResult['ERRORS']['NONFATAL'] as $error)
		{
			ShowError($error);
		}
	}
	if (!count($arResult['ORDERS']))
	{
		if ($_REQUEST["filter_history"] == 'Y')
		{
			if ($_REQUEST["show_canceled"] == 'Y')
			{
				?>
				<h3><?= Loc::getMessage('SPOL_TPL_EMPTY_CANCELED_ORDER') ?></h3>
				<?
			} else
			{
				?>
				<h3><?= Loc::getMessage('SPOL_TPL_EMPTY_HISTORY_ORDER_LIST') ?></h3>
				<?
			}
		} else
		{
			?>
			<h3><?= Loc::getMessage('SPOL_TPL_EMPTY_ORDER_LIST') ?></h3>
			<?
		}
	}
	?>
	<div class="row">
		<div class="col-xs-12">
			<div class="sv-header--h3">
				История заказов
			</div>
		</div>
	</div>
	<hr class="mb-20">
	<div class="row">
		<div class="order-tabs mb-20">
			<div class="col-md-12">
				<?
				$nothing = !isset($_REQUEST["filter_history"]) && !isset($_REQUEST["show_all"]);
				$clearFromLink = array("filter_history", "filter_status", "show_all", "show_canceled","start","pwd");
				?>
				<a class="button grey basic order-tab" href="<?= $APPLICATION->GetCurPageParam("", $clearFromLink, false) ?>">
					<? echo Loc::getMessage("SPOL_TPL_CUR_ORDERS") ?>
				</a>
				<a class="button grey basic order-tab" href="<?= $APPLICATION->GetCurPageParam("filter_history=Y", $clearFromLink, false) ?>">
					<? echo Loc::getMessage("SPOL_TPL_VIEW_ORDERS_HISTORY") ?>
				</a>
				<?if ($_REQUEST["filter_history"] == 'Y'):?>
				<a class="button grey basic order-tab" href="<?= $APPLICATION->GetCurPageParam("filter_history=Y&show_canceled=Y", $clearFromLink, false) ?>">
					<? echo Loc::getMessage("SPOL_TPL_ORDERS_CANCELED_HEADER") ?>
				</a>
				<?endif;?>
			</div>
		</div>
	</div>
	<?
	if (!count($arResult['ORDERS']))
	{
		?>
		<div class="row col-md-12 col-sm-12">

		</div>
		<?
	}

	if ($_REQUEST["filter_history"] !== 'Y')
	{

		$paymentChangeData = array();
		$orderHeaderStatus = null;
		?>


		<?
		foreach ($arResult['ORDERS'] as $key => $order)
		{
			if ($orderHeaderStatus !== $order['ORDER']['STATUS_ID'] && $arResult['SORT_TYPE'] == 'STATUS')
			{
				$orderHeaderStatus = $order['ORDER']['STATUS_ID'];

			}
			?>
			<div class="order-list">
				<div class="order-block" id="block-target1">
					<div class="sv-card mb-20">
						<div class="row">
							<div class="col-md-6">
								<div class="order-info">
									<p>
										<strong>
											<?= Loc::getMessage('SPOL_TPL_ORDER') ?>
											<?= Loc::getMessage('SPOL_TPL_NUMBER_SIGN') . $order['ORDER']['ACCOUNT_NUMBER'] ?>
										</strong>
										<?= Loc::getMessage('SPOL_TPL_FROM_DATE') ?>
										<?= $order['ORDER']['DATE_INSERT']->format('d-m-Y H:i:s') ?>
									</p>
									<p>
										<strong><?= count($order['BASKET_ITEMS']); ?></strong>
										<?
										$count = count($order['BASKET_ITEMS']) % 10;
										if ($count == '1')
										{
											echo Loc::getMessage('SPOL_TPL_GOOD');
										} elseif ($count >= '2' && $count <= '4')
										{
											echo Loc::getMessage('SPOL_TPL_TWO_GOODS');
										} else
										{
											echo Loc::getMessage('SPOL_TPL_GOODS');
										}
										?>
									</p>
									<p><?= Loc::getMessage('SPOL_TPL_SUM_TO_PAID') ?>
										<strong><?= $order['ORDER']['FORMATED_PRICE'] ?></strong>
									</p>
								</div>
							</div>
							<div class="col-md-6">
								<div class="order-status right-side">
									<? $showDelimeter = false;
									foreach ($order['PAYMENT'] as $payment):
										if ($order['ORDER']['LOCK_CHANGE_PAYSYSTEM'] !== 'Y')
										{
											$paymentChangeData[$payment['ACCOUNT_NUMBER']] = array(
												"order" => htmlspecialcharsbx($order['ORDER']['ACCOUNT_NUMBER']),
												"payment" => htmlspecialcharsbx($payment['ACCOUNT_NUMBER']),
												"allow_inner" => $arParams['ALLOW_INNER'],
												"only_inner_full" => $arParams['ONLY_INNER_FULL']
											);

										} ?>

									<p>
										<? if ($orderHeaderStatus == 'N')
										{ ?>
											<a target="_blank" class="button green" href="<?=htmlspecialcharsbx($payment['PSA_ACTION_FILE'])?>"><?= Loc::getMessage('SPOL_TPL_PAY') ?></a>

										<? } elseif ($orderHeaderStatus == 'F' || $orderHeaderStatus == 'P')
										{ ?>

										<? } else
										{ ?>
											<?
										} ?>
									</p>
									<? endforeach; ?>
									<p>
										<a href="<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_CANCEL"])?>" class="button grey basic"><?= Loc::getMessage('SPOL_TPL_CANCEL_ORDER') ?></a>
									</p>

								</div>


							</div>
						</div>
						<div class="row">
							<div class="order-action">
								<div class="col-md-6">
									<a href="<?= htmlspecialcharsbx($order["ORDER"]["URL_TO_DETAIL"]) ?>" class="button blue">Подробнее о заказе</a>
								</div>
								<div class="col-md-6">
									<a href="<?= htmlspecialcharsbx($order["ORDER"]["URL_TO_COPY"]) ?>" class="link--blue right-side"><i class="fa fa-repeat"
									                                                                                                     aria-hidden="true"></i> <?= Loc::getMessage('SPOL_TPL_REPEAT_ORDER') ?>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<?
		}
	} else
	{
		$orderHeaderStatus = null;


		foreach ($arResult['ORDERS'] as $key => $order)
		{
			if ($orderHeaderStatus !== $order['ORDER']['STATUS_ID'] && $_REQUEST["show_canceled"] !== 'Y')
			{
				$orderHeaderStatus = $order['ORDER']['STATUS_ID'];
			} ?>

			<div class="order-list">
				<div class="order-block" id="block-target1">
					<div class="sv-card mb-20">
						<div class="row">
							<div class="col-md-6">
								<div class="order-info">
									<p><strong><?= Loc::getMessage('SPOL_TPL_ORDER') ?>
											<?= Loc::getMessage('SPOL_TPL_NUMBER_SIGN') ?>
											<?= htmlspecialcharsbx($order['ORDER']['ACCOUNT_NUMBER']) ?></strong>
										<?= Loc::getMessage('SPOL_TPL_FROM_DATE') ?>
										<?= $order['ORDER']['DATE_INSERT']->format('d-m-Y H:i:s') ?></p>
									<p><strong><?= count($order['BASKET_ITEMS']); ?></strong>
										<?
										$count = substr(count($order['BASKET_ITEMS']), -1);
										if ($count == '1')
										{
											echo Loc::getMessage('SPOL_TPL_GOOD');
										} elseif ($count >= '2' || $count <= '4')
										{
											echo Loc::getMessage('SPOL_TPL_TWO_GOODS');
										} else
										{
											echo Loc::getMessage('SPOL_TPL_GOODS');
										}
										?>
										<?= Loc::getMessage('SPOL_TPL_SUMOF') ?>
										<strong><?= $order['ORDER']['FORMATED_PRICE'] ?></strong></p>
								</div>
							</div>
							<div class="col-md-6">
								<div class="order-status right-side">
									<?
									if ($_REQUEST["show_canceled"] !== 'Y')
									{
										?>
										<span class="realized-order">
										<?= Loc::getMessage('SPOL_TPL_ORDER_FINISHED') ?>
									</span>:
										<?
									} else
									{
										?>
										<span class="canceled-order">
										<?= Loc::getMessage('SPOL_TPL_ORDER_CANCELED') ?>
									</span>:
										<?
									}
									?>
									<?= $order['ORDER']['DATE_INSERT']->format('d-m-Y') ?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="order-action">
								<div class="col-md-6">
									<a href="<?= htmlspecialcharsbx($order["ORDER"]["URL_TO_DETAIL"]) ?>" class="button blue">Подробнее о заказе</a>
								</div>
								<div class="col-md-6">
									<a href="<?= htmlspecialcharsbx($order["ORDER"]["URL_TO_COPY"]) ?>" class="link--blue right-side"><i class="fa fa-repeat"
									                                                                                                     aria-hidden="true"></i> <?= Loc::getMessage('SPOL_TPL_REPEAT_ORDER') ?>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
			<?
		}
	}
	?>
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-xs-12">
			<div class="pagination-bar">
				<?= $arResult['NAV_STRING'] ?>
			</div>
		</div>
	</div>
	<?

	if ($_REQUEST["filter_history"] !== 'Y')
	{
		$javascriptParams = array(
			"url" => CUtil::JSEscape($this->__component->GetPath() . '/ajax.php'),
			"templateFolder" => CUtil::JSEscape($templateFolder),
			"paymentList" => $paymentChangeData
		);
		$javascriptParams = CUtil::PhpToJSObject($javascriptParams);
		?>
		<script>
            BX.Sale.PersonalOrderComponent.PersonalOrderList.init(<?=$javascriptParams?>);
		</script>
		<?
	}
}
?>
