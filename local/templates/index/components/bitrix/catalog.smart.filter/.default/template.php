<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
/*
$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/colors.css',
	'TEMPLATE_CLASS' => 'bx-'.$arParams['TEMPLATE_THEME']
);

if (isset($templateData['TEMPLATE_THEME']))
{
	$this->addExternalCss($templateData['TEMPLATE_THEME']);
}*/
#$this->addExternalCss("/bitrix/css/main/bootstrap.css");
#$this->addExternalCss("/bitrix/css/main/font-awesome.css");
?>

<form name="<? echo $arResult["FILTER_NAME"] . "_form" ?>" action="<? echo $arResult["FORM_ACTION"] ?>" method="get" class="smartfilter">
	<? foreach ($arResult["HIDDEN"] as $arItem): ?>
		<input type="hidden" name="<? echo $arItem["CONTROL_NAME"] ?>" id="<? echo $arItem["CONTROL_ID"] ?>" value="<? echo $arItem["HTML_VALUE"] ?>"/>
	<? endforeach; ?>


	<div class="sidebar-box"><span class="opener plus"></span>
		<div class="sidebar-title">
			<h3>Фильтр товаров</h3>
		</div>

		<? foreach ($arResult["ITEMS"] as $key => $arItem)//prices
		{
			$key = $arItem["ENCODED_ID"];
			if (isset($arItem["PRICE"])):
				if ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0)
					continue;

				$step_num = 4;
				$step = ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"]) / $step_num;
				$prices = array();
				if (Bitrix\Main\Loader::includeModule("currency"))
				{
					for ($i = 0; $i < $step_num; $i++)
					{
						$prices[$i] = CCurrencyLang::CurrencyFormat($arItem["VALUES"]["MIN"]["VALUE"] + $step * $i, $arItem["VALUES"]["MIN"]["CURRENCY"], false);
					}
					$prices[$step_num] = CCurrencyLang::CurrencyFormat($arItem["VALUES"]["MAX"]["VALUE"], $arItem["VALUES"]["MAX"]["CURRENCY"], false);
				} else
				{
					$precision = $arItem["DECIMALS"] ? $arItem["DECIMALS"] : 0;
					for ($i = 0; $i < $step_num; $i++)
					{
						$prices[$i] = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * $i, $precision, ".", "");
					}
					$prices[$step_num] = number_format($arItem["VALUES"]["MAX"]["VALUE"], $precision, ".", "");
				}

				if (!$arItem["VALUES"]["MIN"]["HTML_VALUE"])
				{
					$arItem["VALUES"]["MIN"]["HTML_VALUE"] = $arItem["VALUES"]["MIN"]["VALUE"];
				}

				if (!$arItem["VALUES"]["MAX"]["HTML_VALUE"])
				{
					$arItem["VALUES"]["MAX"]["HTML_VALUE"] = $arItem["VALUES"]["MAX"]["VALUE"];
				}


				?>

				<div class="sidebar-contant">
					<div class="price-range   mb-20" minRange="<?= $arItem["VALUES"]["MIN"]["VALUE"] ?>" maxRange="<?= $arItem["VALUES"]["MAX"]["VALUE"] ?>"
					     minValue="<? echo $arItem["VALUES"]["MIN"]["HTML_VALUE"] ?>" maxValue="<? echo $arItem["VALUES"]["MAX"]["HTML_VALUE"] ?>">
						<div class="inner-title"><?= $arItem[NAME] ?></div>
						<div class="price-txt" id="amount"></div>
						<div id="slider-range"></div>
					</div>

					<?
					foreach ($arResult["ITEMS"] as $key => $arItem)
						if ($arItem[CODE] == "SEX")
						{
							?>
							<div class="mb-20 bx-filter-parameters-box male-female">
								<span class="bx-filter-container-modef"></span>
								<div class="inner-title"><?= $arItem[NAME] ?></div>
								<ul class="sidebar-checking">

									<? foreach ($arItem["VALUES"] as $val => $ar):?>


										<li>
											<input type="checkbox"

											       type="checkbox"
											       value="<? echo $ar["HTML_VALUE"] ?>"
											       name="<? echo $ar["CONTROL_NAME"] ?>"
											       id="<? echo $ar["CONTROL_ID"] ?>"
												<? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
												   onclick="smartFilter.click(this)"
												   data-title="<?= $ar["VALUE"]; ?>"


											>
											<label for="<? echo $ar["CONTROL_ID"] ?>"
											       class="button grey basic <? echo $ar["DISABLED"] ? 'disabled' : '' ?>"><?= substr($ar["VALUE"], 0, 3); ?></label>
										</li>


									<?endforeach; ?>


								</ul>
							</div>
						<?
						} ?>


				</div>

				<input
						class="min-price"
						type="text"
						name="<? echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"] ?>"
						id="<? echo $arItem["VALUES"]["MIN"]["CONTROL_ID"] ?>"
						value="<? echo $arItem["VALUES"]["MIN"]["HTML_VALUE"] ?>"
						size="5"
						onkeyup="smartFilter.keyup(this)"
				/>


				<input
						class="max-price"
						type="text"
						name="<? echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"] ?>"
						id="<? echo $arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>"
						value="<? echo $arItem["VALUES"]["MAX"]["HTML_VALUE"] ?>"
						size="5"
						onkeyup="smartFilter.keyup(this)"
				/>


			<?endif;
		} ?>


	</div>
	<div class="mobile-sidebar">
		<div class="mobile-sidebar-header">
			<h3>Фильтр товаров</h3>
			<div class="mobile-sidebar-close">
				<i class="fa fa-close"></i>
			</div>
		</div>

		<?
		//not prices
		foreach ($arResult["ITEMS"] as $key => $arItem)
			if ($arItem[CODE] != "SEX")
			{
				if (
					empty($arItem["VALUES"])
					|| isset($arItem["PRICE"])
				)
					continue;

				if (
					$arItem["DISPLAY_TYPE"] == "A"
					&& (
						$arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0
					)
				)
					continue;
				?>


				<div class="sidebar-box listing-box pb-30 box-viser bx-filter-parameters-box <? if ($arItem["DISPLAY_EXPANDED"] == "Y"):?>bx-active<?endif ?>">
					<span class="bx-filter-container-modef"></span>
					<span class="opener <? if ($arItem["DISPLAY_EXPANDED"] == "Y")
					{
						?>minus<?
					} else
					{
						?>plus<?
					} ?> viser">
						<i class="fa fa-angle-down"></i>
						</span>

					<div class="sidebar-title">
						<h4><?= $arItem[NAME] ?></h4>
					</div>


					<div class="bx-filter-parameters-box-container sidebar-contant <? if ($arItem["DISPLAY_EXPANDED"] != "Y"):?>side-hidden<?endif ?> bx-filter-block"
					     data-role="bx_filter_block">

						<?
						$arItem["DISPLAY_TYPE"] = "F";
						$arCur = current($arItem["VALUES"]);
						switch ($arItem["DISPLAY_TYPE"])
						{

							default://CHECKBOXES
								?>
								<ul>
									<? foreach ($arItem["VALUES"] as $val => $ar):?>
										<li class="checkbox">
											<label data-role="label_<?= $ar["CONTROL_ID"] ?>" class="bx-filter-param-label <? echo $ar["DISABLED"] ? 'disabled' : '' ?>"
											       for="<? echo $ar["CONTROL_ID"] ?>">
													<span class="bx-filter-input-checkbox">
														<input
																type="checkbox"
																value="<? echo $ar["HTML_VALUE"] ?>"
																name="<? echo $ar["CONTROL_NAME"] ?>"
																id="<? echo $ar["CONTROL_ID"] ?>"
															<? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
																onclick="smartFilter.click(this)"
																data-title="<?= $ar["VALUE"]; ?>"
														/>
														<span class="checkbox-checked">
															<i class="fa fa-square-o"></i>
															<i class="fa fa-check-square-o"></i>
														</span>
														<span class="bx-filter-param-text" title="<?= $ar["VALUE"]; ?>"><?= $ar["VALUE"]; ?><?
															if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):
																?>&nbsp;(<span data-role="count_<?= $ar["CONTROL_ID"] ?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<?
															endif; ?></span>
													</span>
											</label>
										</li>
									<?endforeach; ?>
								</ul>

								<?
						}
						?>

					</div>
				</div>
				<?
			}
		?>
		<div class="bx-filter-parameters-box-container mobile-filter">
			<input class="" type="button" id="set_filter" name="set_filter" value="Показать">
			<input class="" type="button" id="del_filter" name="del_filter" value="Сбросить">
		</div>
		<div></div>
	</div>


	<div class="row">
		<div class="col-xs-12 bx-filter-button-box">
			<div class="bx-filter-block">
				<div class="bx-filter-parameters-box-container">
					<input
							class=""
							type="submit"
							id="set_filter"
							name="set_filter"
							value="<?= GetMessage("CT_BCSF_SET_FILTER") ?>"
					/>
					<input
							class=""
							type="submit"
							id="del_filter"
							name="del_filter"
							value="<?= GetMessage("CT_BCSF_DEL_FILTER") ?>"
					/>
					<div class="bx-filter-popup-result <? if ($arParams["FILTER_VIEW_MODE"] == "VERTICAL") echo $arParams["POPUP_POSITION"] ?>"
					     id="modef" <? if (!isset($arResult["ELEMENT_COUNT"])) echo 'style="display:none"'; ?> style="display: inline-block;">
						<? #echo GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<span id="modef_num">'.intval($arResult["ELEMENT_COUNT"]).'</span>'));?>
						<span class="arrow"></span>
						<? /*<br/>*/ ?>
						<a href="<? echo $arResult["FILTER_URL"] ?>" target=""><? echo GetMessage("CT_BCSF_FILTER_SHOW") ?></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="clb"></div>
</form>

<script type="text/javascript">
    var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>', <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>);
</script>