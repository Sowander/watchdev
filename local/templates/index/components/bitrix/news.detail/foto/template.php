<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? if (is_array($arResult["DETAIL_PICTURE"]))
{ ?>


	<section class="single-blog-media">
		<img src="<?= $arResult["DETAIL_PICTURE"]["SRC"] ?>" alt="<?= $arResult["DETAIL_PICTURE"]["ALT"] ?>"
		     title="<?= $arResult["DETAIL_PICTURE"]["TITLE"] ?>">
		<div class="single-blog-title">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2><?= $arResult["NAME"] ?></h2>
					</div>
				</div>
			</div>
		</div>
	</section>


<? } ?>