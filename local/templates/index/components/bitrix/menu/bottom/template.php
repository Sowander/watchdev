<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult)): ?>
	<ul class="footer-block-contant link">

		<?
		foreach ($arResult as $arItem):
			if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
				continue;
			?>
			<? if ($arItem["SELECTED"]):?>
			<li><a href="<?= $arItem["LINK"] ?>" class="selected"><span>■</span> <?= $arItem["TEXT"] ?></a></li>
		<? else:?>
			<li><a href="<?= $arItem["LINK"] ?>"><span>■</span> <?= $arItem["TEXT"] ?></a></li>
		<? endif ?>

		<? endforeach ?>

	</ul>
<? endif ?>