<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);

if (empty($arResult["ALL_ITEMS"]))
	return;

CUtil::InitJSCore();

if (file_exists($_SERVER["DOCUMENT_ROOT"] . $this->GetFolder() . '/themes/' . $arParams["MENU_THEME"] . '/colors.css'))
	$APPLICATION->SetAdditionalCSS($this->GetFolder() . '/themes/' . $arParams["MENU_THEME"] . '/colors.css');

$menuBlockId = "catalog_menu_" . $this->randString();
?>

<!-- ************* -->
<div id="menu" class="navbar-collapse collapse">
	<ul class="nav navbar-nav">
		<? foreach ($arResult["MENU_STRUCTURE"] as $itemID => $arColumns)
		{ ?>     <!-- first level-->
			<li class="level">
				<span class="<? if (is_array($arColumns) && count($arColumns) > 0)
				{ ?>opener plus<? } ?>"></span>
				<a href="<?= $arResult["ALL_ITEMS"][$itemID]["LINK"] ?>" class="page-scroll">
					<?= $arResult["ALL_ITEMS"][$itemID]["TEXT"] ?>
				</a>
				<? if (is_array($arColumns) && count($arColumns) > 0)
				{ ?>
					<div class="megamenu mobile-sub-menu">
						<div class="megamenu-inner-top">
							<? foreach ($arColumns as $key => $arRow)
							{ ?>
								<ul class="sub-menu-level1">
									<? foreach ($arRow as $itemIdLevel_2 => $arLevel_3)
									{ ?>  <!-- second level-->
										<li class="level2">
											<a href="<?= $arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"] ?>">
												<span><?= $arResult["ALL_ITEMS"][$itemIdLevel_2]["TEXT"] ?></span>
											</a>
											<? if (is_array($arLevel_3) && count($arLevel_3) > 0)
											{ ?>
												<ul class="sub-menu-level2">
													<? foreach ($arLevel_3 as $itemIdLevel_3)
													{ ?>    <!-- third level-->
														<li class="level3">
															<a href="<?= $arResult["ALL_ITEMS"][$itemIdLevel_3]["LINK"] ?>"><?= $arResult["ALL_ITEMS"][$itemIdLevel_3]["TEXT"] ?></a>
														</li>

													<? } ?>
												</ul>
											<? } ?>
										</li>
									<? } ?>
								</ul>
							<? } ?>
						</div>
					</div>
				<? } ?>
			</li>
		<? } ?>
		<li class="level"><a href="shop.html" class="page-scroll">Избранное</a></li>
		<li class="level"><a href="shop.html" class="page-scroll">Сравнение</a></li>
	</ul>
</div>

