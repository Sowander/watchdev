<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>

<? if (!empty($arResult["ERROR_MESSAGE"]))
{
	foreach ($arResult["ERROR_MESSAGE"] as $v)
		ShowError($v);
}
if (strlen($arResult["OK_MESSAGE"]) > 0)
{
	?>
	<div class="mf-ok-text"><?= $arResult["OK_MESSAGE"] ?></div><?
}
?>


<form action="<?= POST_FORM_ACTION_URI ?>" method="POST" name="contactform">
	<?= bitrix_sessid_post() ?>
	<div class="col-sm-4 mb-30">
		<label for="name">Имя<span>*</span></label>
		<input type="text" required="" placeholder="Введите имя" name="user_name" value="<?= $arResult["AUTHOR_NAME"] ?>" id="name">
	</div>
	<div class="col-sm-4 mb-30">
		<label for="email">E-Mail<span>*</span></label>
		<input type="email" required="" placeholder="Введите почту" name="user_email" value="<?= $arResult["AUTHOR_EMAIL"] ?>" id="email">
	</div>

	<div class="col-sm-4 mb-30">
		<label for="phone">Телефон<span>*</span></label>
		<input type="tel" required="" placeholder="Введите телефон" name="user_phone" value="<?= $arResult["AUTHOR_PHONE"] ?>" id="phome">
	</div>

	<div class="col-xs-12 mb-30">
		<label for="message">Сообщение<span>*</span></label>
		<textarea required="" placeholder="Введите сообщение" rows="3" cols="30" name="MESSAGE" id="message"><?= $arResult["MESSAGE"] ?></textarea>
	</div>
	<div class="col-xs-12">
		<div class="align-right">
			<input type="hidden" name="PARAMS_HASH" value="<?= $arResult["PARAMS_HASH"] ?>">
			<button type="submit" name="submit" class="button green big">Отправить</button>
		</div>
	</div>
</form>


