<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Page\Asset,
	Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
//Loc::getMessage("ASD");

global $APPLICATION;
$curPage = $APPLICATION->GetCurPage(true);
$dir = $APPLICATION->GetCurDir();
$dirlist = explode("/", $dir);
?>
<!DOCTYPE html>
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->

<head>

	<? $APPLICATION->ShowHead(); ?>
	<?
	//CSS
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/font-awesome.min.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/bootstrap.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/jquery-ui.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/owl.carousel.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/fotorama.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/magnific-popup.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/popup.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/dropdown.min.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/transition.min.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/custom.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/jquery.fancybox.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/responsive.css");
	//JS
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/jquery-2.2.4.min.js');
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/popup.js');
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/bootstrap.min.js');
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/jquery-ui.min.js');
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/fotorama.js');
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/owl.carousel.min.js');
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/jquery.stellar.js');
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/jquery.fancybox.js');
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/transition.min.js');
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/jquery.magnific-popup.js');
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/dropdown.min.js');
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/jquery.maskedinput.min.js');
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/custom.js');
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/favorites.js');
	?>
	<title><? $APPLICATION->ShowTitle() ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="shortcut icon" href="<?= SITE_TEMPLATE_PATH ?>/images/favicon.png">
	<link rel="apple-touch-icon" href="<?= SITE_TEMPLATE_PATH ?>/images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?= SITE_TEMPLATE_PATH ?>/images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?= SITE_TEMPLATE_PATH ?>/images/apple-touch-icon-114x114.png">
</head>


<body class="homepage">

<? /* <div class="se-pre-con"></div>*/ ?>



<? $APPLICATION->ShowPanel(); ?>
<div class="main">
	<!-- HEADER START -->
	<header class="navbar navbar-custom " id="header">
		<div class="container">
			<div class="header-inner">
				<div class="row">
					<div class="col-md-3 col-xs-6">
						<div class="navbar-header"><a class="navbar-brand page-scroll" href="/"> <img alt="" src="/images/storelogo.png"> </a></div>
					</div>
					<div class="col-md-3 col-xs-6 right-side">
						<div class="right-side float-left-xs header-right-link">
							<ul>
								<li class="main-search">
									<div class="header_search_toggle desktop-view">
										<form action="/search/" method=get>
											<div class="search-box">
												<input class="input-text" type="text" placeholder="Введите поисковый запрос..." name=q>
												<button class="search-btn"></button>
											</div>
										</form>
									</div>
								</li>
								<li class="account-icon">
									<a href="<?if($USER->isAuthorized()){?>/personal/<?}else{?>/auth/<?}?>">
						<span><i class="fa fa-user" aria-hidden="true"></i>
						</span>
									</a>
									
									<? /*
									<div class="header-link-dropdown account-link-dropdown">
										<ul class="link-dropdown-list">
											<li><span class="dropdown-title">Default welcome msg!</span>
												<ul>
													<li><a href="login.html">Sign In</a></li>
													<li><a href="register.html">Create an Account</a></li>
												</ul>
											</li>
											<li><span class="dropdown-title">Language :</span>
												<ul>
													<li><a class="active" href="#">English</a></li>
													<li><a href="#">French</a></li>
													<li><a href="#">German</a></li>
												</ul>
											</li>
											<li><span class="dropdown-title">Currency :</span>
												<ul>
													<li><a class="active" href="#">USD</a></li>
													<li><a href="#">AUD</a></li>
													<li><a href="#">EUR</a></li>
												</ul>
											</li>
										</ul>
									</div>
									*/?>
									
									
								</li>

								<li class="cart-icon">
									<? $APPLICATION->IncludeComponent(
										"bitrix:sale.basket.basket.line",
										"glab",
										array(
											"PATH_TO_BASKET" => SITE_DIR . "personal/cart/",
											"PATH_TO_PERSONAL" => SITE_DIR . "personal/",
											"SHOW_PERSONAL_LINK" => "N",
											"SHOW_NUM_PRODUCTS" => "Y",
											"SHOW_TOTAL_PRICE" => "Y",
											"SHOW_PRODUCTS" => "Y",
											"POSITION_FIXED" => "N",
											"SHOW_AUTHOR" => "Y",
											"PATH_TO_REGISTER" => SITE_DIR . "login/",
											"PATH_TO_PROFILE" => SITE_DIR . "personal/",
											"COMPONENT_TEMPLATE" => "glab",
											"PATH_TO_ORDER" => SITE_DIR . "personal/order/make/",
											"SHOW_EMPTY_VALUES" => "Y",
											"PATH_TO_AUTHORIZE" => "",
											"SHOW_DELAY" => "N",
											"SHOW_NOTAVAIL" => "Y",
											"SHOW_SUBSCRIBE" => "N",
											"SHOW_IMAGE" => "Y",
											"SHOW_PRICE" => "Y",
											"SHOW_SUMMARY" => "Y",
											"HIDE_ON_BASKET_PAGES" => "Y",
											"POSITION_HORIZONTAL" => "right",
											"POSITION_VERTICAL" => "top"
										),
										false
									); ?>


								</li>
							</ul>
						</div>
						<button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button"><i class="fa fa-bars"></i></button>
					</div>


					<div class="col-md-6 col-sm-12 position-s left-side float-none-sm">
						<div class="align-center">


							<? $APPLICATION->IncludeComponent("bitrix:menu", "top_horizontal", Array(
								"ROOT_MENU_TYPE" => "top",    // Тип меню для первого уровня
								"MENU_CACHE_TYPE" => "A",    // Тип кеширования
								"MENU_CACHE_TIME" => "36000000",    // Время кеширования (сек.)
								"MENU_CACHE_USE_GROUPS" => "Y",    // Учитывать права доступа
								"MENU_THEME" => "site",    // Тема меню
								"CACHE_SELECTED_ITEMS" => "N",
								"MENU_CACHE_GET_VARS" => "",    // Значимые переменные запроса
								"MAX_LEVEL" => "2",    // Уровень вложенности меню
								"CHILD_MENU_TYPE" => "left",    // Тип меню для остальных уровней
								"USE_EXT" => "Y",    // Подключать файлы с именами вида .тип_меню.menu_ext.php
								"DELAY" => "N",    // Откладывать выполнение шаблона меню
								"ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
							),
								false
							); ?>


						</div>
					</div>
				</div>
				<script>
                    $(document).ready(function () {
                        $('.sub-menu-level1:eq(2) .level2').css('display', 'list-item');
                    });

				</script>
				<div class="header_search_toggle mobile-view">
					<form>
						<div class="search-box">
							<input class="input-text" type="text" placeholder="Search entire store here...">
							<button class="search-btn"></button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</header>
	<!-- HEADER END -->

	<? if ($dir != '/')
	{ ?>

	<div class="banner inner-banner">
		<div class="container"></div>
	</div>
	<div class="fix-menu">
		<ul>
			<li class="fav">
				<a href="/favorites/">
					<i class="fa fa-heart-o"></i>
					<span>9</span>
				</a>
			</li>
			<? if ($dir !== '/compare/'): ?>
				<? $APPLICATION->IncludeComponent(
					"bitrix:catalog.compare.list",
					"glab",
					array(
						"ACTION_VARIABLE" => "action",
						"AJAX_MODE" => "N",
						"AJAX_OPTION_ADDITIONAL" => "",
						"AJAX_OPTION_HISTORY" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "Y",
						"COMPARE_URL" => "/compare/",
						"DETAIL_URL" => "",
						"IBLOCK_ID" => "2",
						"IBLOCK_TYPE" => "catalog",
						"NAME" => "CATALOG_COMPARE_LIST",
						"POSITION" => "top left",
						"POSITION_FIXED" => "N",
						"PRODUCT_ID_VARIABLE" => "id",
						"COMPONENT_TEMPLATE" => "glab"
					),
					false
				); ?>
			<? endif; ?>
		</ul>
	</div>


<? if ($dirlist[1] == 'news' and strlen($dirlist[2]) > 1 and strlen($dirlist[3]) > 1)
{
	//детальная новость, выводим картинку	

	?>

	<? $APPLICATION->IncludeComponent(
	"bitrix:news.detail",
	"foto",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"BROWSER_TITLE" => "-",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "N",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_CODE" => $dirlist[3],
		"ELEMENT_ID" => "",
		"FIELD_CODE" => array("ID", "CODE", "XML_ID", "NAME", "TAGS", "SORT", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DETAIL_TEXT", "DETAIL_PICTURE", "DATE_ACTIVE_FROM", "ACTIVE_FROM", "DATE_ACTIVE_TO", "ACTIVE_TO", "SHOW_COUNTER", "SHOW_COUNTER_START", "IBLOCK_TYPE_ID", "IBLOCK_ID", "IBLOCK_CODE", "IBLOCK_NAME", "IBLOCK_EXTERNAL_ID", "DATE_CREATE", "CREATED_BY", "CREATED_USER_NAME", "TIMESTAMP_X", "MODIFIED_BY", "USER_NAME", ""),
		"IBLOCK_ID" => "1",
		"IBLOCK_TYPE" => "news",
		"IBLOCK_URL" => "",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"MESSAGE_404" => "",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Страница",
		"PROPERTY_CODE" => array("", ""),
		"SET_BROWSER_TITLE" => "N",
		"SET_CANONICAL_URL" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"STRICT_SECTION_CHECK" => "N",
		"USE_PERMISSIONS" => "N",
		"USE_SHARE" => "N"
	)
); ?>
<? } ?>

<? if ($dir == "/contacts/")
{ ?>
	<? $APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_DIR . "include/contactmap.php",
		"AREA_FILE_RECURSIVE" => "N",
		"EDIT_MODE" => "php",
	),
	false,
	Array('HIDE_ICONS' => 'N')
); ?>

<? } ?>
	<section>
		<div class="container">

			<? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
				"START_FROM" => "0",
				"PATH" => "",
				"SITE_ID" => "-"
			),
				false,
				Array('HIDE_ICONS' => 'Y')
			); ?>

			<? } ?>
	</section>


	<?
	if (CSite::InDir('/about/') and $APPLICATION->GetCurDir() != '/about/')
	{
	?>

	<section class="service-section ptb-55">
		<div class="container">
			<div class="row">
				<div class="col-md-12 pb-30">


					<? } ?>

