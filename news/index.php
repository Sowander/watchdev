<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Новости");
?>


	<section class="ptb-55 mobile-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-4 mb-xs-30">
					<div class="sidebar-block">
						<div class="sidebar-box listing-box pb-10"><span class="opener plus"></span>
							<div class="sidebar-title">
								<h3>Категории</h3>
							</div>
							<div class="sidebar-contant">

								<? $APPLICATION->IncludeComponent(
									"bitrix:catalog.section.list",
									"news",
									array(
										"ADD_SECTIONS_CHAIN" => "N",
										"CACHE_GROUPS" => "Y",
										"CACHE_TIME" => "36000000",
										"CACHE_TYPE" => "A",
										"COUNT_ELEMENTS" => "Y",
										"IBLOCK_ID" => "1",
										"IBLOCK_TYPE" => "news",
										"SECTION_CODE" => $_REQUEST["SECTION_CODE"],
										"SECTION_FIELDS" => array(
											0 => "",
											1 => "",
										),
										"SECTION_ID" => "",
										"SECTION_URL" => "",
										"SECTION_USER_FIELDS" => array(
											0 => "",
											1 => "",
										),
										"SHOW_PARENT_NAME" => "Y",
										"TOP_DEPTH" => "1",
										"VIEW_MODE" => "LINE",
										"COMPONENT_TEMPLATE" => "news"
									),
									false
								); ?>


							</div>
						</div>
						<div class="sidebar-box sidebar-item sidebar-item-wide"><span class="opener plus"></span>
							<div class="sidebar-title">
								<h3>Последние новости</h3>
							</div>
							<div class="sidebar-contant">
								<? $APPLICATION->IncludeComponent(
									"bitrix:news.list",
									"news_last",
									array(
										"ACTIVE_DATE_FORMAT" => "",
										"ADD_SECTIONS_CHAIN" => "N",
										"AJAX_MODE" => "N",
										"AJAX_OPTION_ADDITIONAL" => "",
										"AJAX_OPTION_HISTORY" => "N",
										"AJAX_OPTION_JUMP" => "N",
										"AJAX_OPTION_STYLE" => "Y",
										"CACHE_FILTER" => "N",
										"CACHE_GROUPS" => "Y",
										"CACHE_TIME" => "3600",
										"CACHE_TYPE" => "A",
										"CHECK_DATES" => "Y",
										"DETAIL_URL" => "",
										"DISPLAY_BOTTOM_PAGER" => "N",
										"DISPLAY_DATE" => "Y",
										"DISPLAY_NAME" => "Y",
										"DISPLAY_PICTURE" => "Y",
										"DISPLAY_PREVIEW_TEXT" => "Y",
										"DISPLAY_TOP_PAGER" => "N",
										"FIELD_CODE" => array(
											0 => "ID",
											1 => "CODE",
											2 => "XML_ID",
											3 => "NAME",
											4 => "TAGS",
											5 => "SORT",
											6 => "PREVIEW_TEXT",
											7 => "PREVIEW_PICTURE",
											8 => "DETAIL_TEXT",
											9 => "DETAIL_PICTURE",
											10 => "DATE_ACTIVE_FROM",
											11 => "ACTIVE_FROM",
											12 => "DATE_ACTIVE_TO",
											13 => "ACTIVE_TO",
											14 => "SHOW_COUNTER",
											15 => "SHOW_COUNTER_START",
											16 => "IBLOCK_TYPE_ID",
											17 => "IBLOCK_ID",
											18 => "IBLOCK_CODE",
											19 => "IBLOCK_NAME",
											20 => "IBLOCK_EXTERNAL_ID",
											21 => "DATE_CREATE",
											22 => "CREATED_BY",
											23 => "CREATED_USER_NAME",
											24 => "TIMESTAMP_X",
											25 => "MODIFIED_BY",
											26 => "USER_NAME",
											27 => "",
										),
										"FILTER_NAME" => "",
										"HIDE_LINK_WHEN_NO_DETAIL" => "N",
										"IBLOCK_ID" => "1",
										"IBLOCK_TYPE" => "news",
										"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
										"INCLUDE_SUBSECTIONS" => "Y",
										"MESSAGE_404" => "",
										"NEWS_COUNT" => "4",
										"PAGER_BASE_LINK_ENABLE" => "N",
										"PAGER_DESC_NUMBERING" => "N",
										"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
										"PAGER_SHOW_ALL" => "N",
										"PAGER_SHOW_ALWAYS" => "N",
										"PAGER_TEMPLATE" => ".default",
										"PAGER_TITLE" => "Новости",
										"PARENT_SECTION" => "",
										"PARENT_SECTION_CODE" => "",
										"PREVIEW_TRUNCATE_LEN" => "",
										"PROPERTY_CODE" => array(
											0 => "",
											1 => "",
										),
										"SET_BROWSER_TITLE" => "N",
										"SET_LAST_MODIFIED" => "N",
										"SET_META_DESCRIPTION" => "N",
										"SET_META_KEYWORDS" => "N",
										"SET_STATUS_404" => "N",
										"SET_TITLE" => "N",
										"SHOW_404" => "N",
										"SORT_BY1" => "ACTIVE_FROM",
										"SORT_BY2" => "ID",
										"SORT_ORDER1" => "DESC",
										"SORT_ORDER2" => "ASC",
										"STRICT_SECTION_CHECK" => "N",
										"COMPONENT_TEMPLATE" => "news_last"
									),
									false
								); ?>


							</div>
						</div>
					</div>
				</div>
				<div class="col-md-9 col-sm-8 pb-xs-60">


					<? $APPLICATION->IncludeComponent(
						"bitrix:news",
						"news",
						array(
							"IBLOCK_TYPE" => "news",
							"IBLOCK_ID" => "1",
							"TEMPLATE_THEME" => "site",
							"NEWS_COUNT" => "3",
							"USE_SEARCH" => "N",
							"USE_RSS" => "N",
							"NUM_NEWS" => "20",
							"NUM_DAYS" => "180",
							"YANDEX" => "N",
							"USE_RATING" => "N",
							"USE_CATEGORIES" => "N",
							"USE_REVIEW" => "N",
							"USE_FILTER" => "N",
							"SORT_BY1" => "ACTIVE_FROM",
							"SORT_ORDER1" => "DESC",
							"SORT_BY2" => "SORT",
							"SORT_ORDER2" => "ASC",
							"CHECK_DATES" => "Y",
							"SEF_MODE" => "Y",
							"SEF_FOLDER" => "/news/",
							"AJAX_MODE" => "N",
							"AJAX_OPTION_SHADOW" => "Y",
							"AJAX_OPTION_JUMP" => "N",
							"AJAX_OPTION_STYLE" => "Y",
							"AJAX_OPTION_HISTORY" => "N",
							"CACHE_TYPE" => "A",
							"CACHE_TIME" => "36000000",
							"CACHE_FILTER" => "N",
							"CACHE_GROUPS" => "Y",
							"DISPLAY_PANEL" => "Y",
							"SET_TITLE" => "Y",
							"SET_STATUS_404" => "Y",
							"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
							"ADD_SECTIONS_CHAIN" => "Y",
							"ADD_ELEMENT_CHAIN" => "Y",
							"USE_PERMISSIONS" => "N",
							"PREVIEW_TRUNCATE_LEN" => "",
							"LIST_ACTIVE_DATE_FORMAT" => "j F Y",
							"LIST_FIELD_CODE" => array(
								0 => "",
								1 => "CREATED_BY",
								2 => "CREATED_USER_NAME",
								3 => "",
							),
							"LIST_PROPERTY_CODE" => array(
								0 => "",
								1 => "",
							),
							"HIDE_LINK_WHEN_NO_DETAIL" => "N",
							"DISPLAY_NAME" => "Y",
							"META_KEYWORDS" => "-",
							"META_DESCRIPTION" => "-",
							"BROWSER_TITLE" => "-",
							"DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
							"DETAIL_FIELD_CODE" => array(
								0 => "",
								1 => "",
							),
							"DETAIL_PROPERTY_CODE" => array(
								0 => "",
								1 => "",
							),
							"DETAIL_DISPLAY_TOP_PAGER" => "N",
							"DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
							"DETAIL_PAGER_TITLE" => "Страница",
							"DETAIL_PAGER_TEMPLATE" => "salvador",
							"DETAIL_PAGER_SHOW_ALL" => "N",
							"DISPLAY_TOP_PAGER" => "N",
							"DISPLAY_BOTTOM_PAGER" => "Y",
							"PAGER_TITLE" => "Новости",
							"PAGER_SHOW_ALWAYS" => "N",
							"PAGER_TEMPLATE" => "orange",
							"PAGER_DESC_NUMBERING" => "N",
							"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000000",
							"PAGER_SHOW_ALL" => "N",
							"DISPLAY_DATE" => "Y",
							"DISPLAY_PICTURE" => "Y",
							"DISPLAY_PREVIEW_TEXT" => "Y",
							"AJAX_OPTION_ADDITIONAL" => "",
							"SLIDER_PROPERTY" => "PICS_NEWS",
							"COMPONENT_TEMPLATE" => "news",
							"SET_LAST_MODIFIED" => "Y",
							"STRICT_SECTION_CHECK" => "Y",
							"USE_SHARE" => "N",
							"DETAIL_SET_CANONICAL_URL" => "N",
							"PAGER_BASE_LINK_ENABLE" => "N",
							"SHOW_404" => "N",
							"MESSAGE_404" => "",
							"SEF_URL_TEMPLATES" => array(
								"news" => "",
								"section" => "#SECTION_CODE#/",
								"detail" => "#SECTION_CODE#/#ELEMENT_CODE#/",
							)
						),
						false
					); ?>


				</div>
			</div>
		</div>
	</section>


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>