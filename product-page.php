<!DOCTYPE html>
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->
<head>
	<!-- Basic Page Needs
	  ================================================== -->
	<meta charset="utf-8">
	<title>Tizzy Shop</title>
	<!-- SEO Meta
	  ================================================== -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="distribution" content="global">
	<meta name="revisit-after" content="2 Days">
	<meta name="robots" content="ALL">
	<meta name="rating" content="8 YEARS">
	<meta name="Language" content="en-us">
	<meta name="GOOGLEBOT" content="NOARCHIVE">
	<!-- Mobile Specific Metas
	  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- CSS
	  ================================================== -->
	<link rel="stylesheet" type="text/css" href="/local/templates/index/css/font-awesome.min.css"/>
	<link rel="stylesheet" type="text/css" href="/local/templates/index/css/bootstrap.css"/>
	<link rel="stylesheet" type="text/css" href="/local/templates/index/css/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="/local/templates/index/css/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="/local/templates/index/css/fotorama.css">
	<link rel="stylesheet" type="text/css" href="/local/templates/index/css/custom.css">
	<link rel="stylesheet" type="text/css" href="/local/templates/index/css/responsive.css">
	<link rel="shortcut icon" href="/images/favicon.png">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
</head>
<body>
<div class="se-pre-con"></div>
<div class="main">
	<!-- HEADER START -->
	<header class="navbar navbar-custom " id="header">
		<div class="container">
			<div class="header-inner">
				<div class="row">
					<div class="col-md-3 col-xs-6">
						<div class="navbar-header"><a class="navbar-brand page-scroll" href="index.html"> <img alt="Tizzy" src="images/logo.png"> </a></div>
					</div>
					<div class="col-md-3 col-xs-6 right-side">
						<div class="right-side float-left-xs header-right-link">
							<ul>
								<li class="main-search">
									<div class="header_search_toggle desktop-view">
										<form>
											<div class="search-box">
												<input class="input-text" type="text" placeholder="Search entire store here...">
												<button class="search-btn"></button>
											</div>
										</form>
									</div>
								</li>
								<li class="account-icon"><a href="#"><span></span></a>
									<div class="header-link-dropdown account-link-dropdown">
										<ul class="link-dropdown-list">
											<li><span class="dropdown-title">Default welcome msg!</span>
												<ul>
													<li><a href="login.html">Sign In</a></li>
													<li><a href="register.html">Create an Account</a></li>
												</ul>
											</li>
											<li><span class="dropdown-title">Language :</span>
												<ul>
													<li><a class="active" href="#">English</a></li>
													<li><a href="#">French</a></li>
													<li><a href="#">German</a></li>
												</ul>
											</li>
											<li><span class="dropdown-title">Currency :</span>
												<ul>
													<li><a class="active" href="#">USD</a></li>
													<li><a href="#">AUD</a></li>
													<li><a href="#">EUR</a></li>
												</ul>
											</li>
										</ul>
									</div>
								</li>
								<li class="cart-icon"><a href="#"> <span> <small class="cart-notification">2</small> </span> </a>
									<div class="cart-dropdown header-link-dropdown">
										<ul class="cart-list link-dropdown-list">
											<li><a class="close-cart"><i class="fa fa-times-circle"></i></a>
												<div class="media"><a class="pull-left"> <img alt="Minimo" src="images/1.jpg"></a>
													<div class="media-body"><span><a>Black African Print Skirt</a></span>
														<p class="cart-price">$14.99</p>
														<div class="product-qty">
															<label>Qty:</label>
															<div class="custom-qty">
																<input type="text" name="qty" maxlength="8" value="1" title="Qty" class="input-text qty">
															</div>
														</div>
													</div>
												</div>
											</li>
											<li><a class="close-cart"><i class="fa fa-times-circle"></i></a>
												<div class="media"><a class="pull-left"> <img alt="Minimo" src="images/2.jpg"></a>
													<div class="media-body"><span><a>Black African Print Skirt</a></span>
														<p class="cart-price">$14.99</p>
														<div class="product-qty">
															<label>Qty:</label>
															<div class="custom-qty">
																<input type="text" name="qty" maxlength="8" value="1" title="Qty" class="input-text qty">
															</div>
														</div>
													</div>
												</div>
											</li>
										</ul>
										<p class="cart-sub-totle"><span class="pull-left">Cart Subtotal</span> <span class="pull-right"><strong
														class="price-box">$29.98</strong></span></p>
										<div class="clearfix"></div>
										<div class="mt-20"><a href="cart.html" class="btn-color btn">Cart</a> <a href="checkout.html" class="btn-color btn right-side">Checkout</a>
										</div>
									</div>
								</li>
							</ul>
						</div>
						<button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button"><i class="fa fa-bars"></i></button>
					</div>
					<div class="col-md-6 col-sm-12 position-s left-side float-none-sm">
						<div class="align-center">
							<div id="menu" class="navbar-collapse collapse">
								<ul class="nav navbar-nav">
									<li class="level"><span class="opener plus"></span> <a href="shop.html" class="page-scroll">Women</a>
										<div class="megamenu mobile-sub-menu">
											<div class="megamenu-inner-top">
												<ul class="sub-menu-level1">
													<li class="level2"><a href="shop.html"><span>Women Clothing</span></a>
														<ul class="sub-menu-level2">
															<li class="level3"><a href="shop.html">Dresses</a></li>
															<li class="level3"><a href="shop.html">Sport Jeans</a></li>
															<li class="level3"><a href="shop.html">Skirts</a></li>
															<li class="level3"><a href="shop.html">Tops</a></li>
														</ul>
													</li>
													<li class="level2"><a href="shop.html"><span>Women Fashion</span></a>
														<ul class="sub-menu-level2 ">
															<li class="level3"><a href="shop.html">Blazer & Coat</a></li>
															<li class="level3"><a href="shop.html">Sport Shoes</a></li>
															<li class="level3"><a href="shop.html">Phone Cases</a></li>
															<li class="level3"><a href="shop.html">Trousers</a></li>
														</ul>
													</li>
													<li class="level2"><a href="shop.html"><span>Women Clothings</span></a>
														<ul class="sub-menu-level2">
															<li class="level3"><a href="shop.html">Dresses</a></li>
															<li class="level3"><a href="shop.html">Sport Jeans</a></li>
															<li class="level3"><a href="shop.html">Skirts</a></li>
															<li class="level3"><a href="shop.html">Tops</a></li>
														</ul>
													</li>
												</ul>
											</div>
											<div class="megamenu-inner-bottom mt-20 visible-lg visible-md">
												<img src="images/drop_banner1.jpg" alt="Tizzy">
											</div>
										</div>
									</li>
									<li class="level"><span class="opener plus"></span> <a href="shop.html" class="page-scroll">Men</a>
										<div class="megamenu full mobile-sub-menu">
											<div class="container">
												<div class="megamenu-inner">
													<div class="megamenu-inner-top">
														<div class="row">
															<div class="col-md-3 level2"><a href="shop.html"><span>Men Fashion</span></a>
																<ul class="sub-menu-level2 ">
																	<li class="level3"><a href="shop.html">Blazer & Coat</a></li>
																	<li class="level3"><a href="shop.html">Sport Shoes</a></li>
																	<li class="level3"><a href="shop.html">Phone Cases</a></li>
																	<li class="level3"><a href="shop.html">Trousers</a></li>
																	<li class="level3"><a href="shop.html">Purse</a></li>
																	<li class="level3"><a href="shop.html">Wallets</a></li>
																</ul>
															</div>
															<div class="col-md-3 level2"><a href="shop.html"><span>Juniors kid</span></a>
																<ul class="sub-menu-level2 ">
																	<li class="level3"><a href="shop.html">Blazer & Coat</a></li>
																	<li class="level3"><a href="shop.html">Sport Shoes</a></li>
																	<li class="level3"><a href="shop.html">Phone Cases</a></li>
																	<li class="level3"><a href="shop.html">Trousers</a></li>
																	<li class="level3"><a href="shop.html">Purse</a></li>
																	<li class="level3"><a href="shop.html">Wallets</a></li>
																</ul>
															</div>
															<div class="col-md-3 level2"><a href="shop.html"><span>Men Clothings</span></a>
																<ul class="sub-menu-level2 ">
																	<li class="level3"><a href="shop.html">Blazer & Coat</a></li>
																	<li class="level3"><a href="shop.html">Chinos & Trousers</a></li>
																	<li class="level3"><a href="shop.html">Underwear</a></li>
																	<li class="level3"><a href="shop.html">Trousers</a></li>
																	<li class="level3"><a href="shop.html">Purse</a></li>
																	<li class="level3"><a href="shop.html">Wallets</a></li>
																</ul>
															</div>
															<div class="col-md-3 visible-lg visible-md"><a href="shop.html"> <img src="images/drop_banner.jpg" alt="Tizzy"> </a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</li>
									<li class="level"><a href="shop.html" class="page-scroll">Juniors</a></li>
									<li class="level"><span class="opener plus"></span> <a href="shop.html" class="page-scroll">Kids</a>
										<div class="megamenu mobile-sub-menu">
											<div class="megamenu-inner-top">
												<ul class="sub-menu-level1">
													<li class="level2"><a href="shop.html"><span>Kids Fashion</span></a>
														<ul class="sub-menu-level2 ">
															<li class="level3"><a href="shop.html">Blazer & Coat</a></li>
															<li class="level3"><a href="shop.html">Sport Shoes</a></li>
															<li class="level3"><a href="shop.html">Phone Cases</a></li>
															<li class="level3"><a href="shop.html">Trousers</a></li>
															<li class="level3"><a href="shop.html">Purse</a></li>
															<li class="level3"><a href="shop.html">Wallets</a></li>
														</ul>
													</li>
												</ul>
											</div>
										</div>
									</li>
									<li class="level"><a href="shop.html" class="page-scroll">Baby</a></li>
									<li class="level"><span class="opener plus"></span> <a class="page-scroll">Pages</a>
										<div class="megamenu mobile-sub-menu">
											<div class="megamenu-inner-top">
												<ul class="sub-menu-level1">
													<li class="level2">
														<ul class="sub-menu-level2 ">
															<li class="level3"><a href="about.html">About Us</a></li>
															<li class="level3"><a href="account.html">Account</a></li>
															<li class="level3"><a href="checkout.html">Checkout</a></li>
															<li class="level3"><a href="contact.html">Contact</a></li>
															<li class="level3"><a href="404.html">404 Error</a></li>
															<li class="level3"><a href="blog.html">Blog</a></li>
															<li class="level3"><a href="single-blog.html">Single Blog</a></li>
														</ul>
													</li>
												</ul>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="header_search_toggle mobile-view">
					<form>
						<div class="search-box">
							<input class="input-text" type="text" placeholder="Search entire store here...">
							<button class="search-btn"></button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</header>
	<!-- HEADER END -->

	<!-- BANNER STRAT -->
	<div class="banner inner-banner">
	</div>
	<!-- BANNER END -->

	<section>
		<div class="container">
			<div class="bread-crumb">
				<ul>
					<li><a href="index.html">Главная</a>/</li>
					<li><span>Каталог</span>/</li>
					<li><span>Cross Colours Camo Print Tank half mengo</span></li>
				</ul>
			</div>
		</div>
	</section>

	<!-- CONTAIN START -->
	<section class="product-sm-xs ptb-95 mobile-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-5 mb-xs-30">
					<div class="fotorama" data-nav="thumbs" data-allowfullscreen="native"><a href="#"><img src="images/1.jpg" alt="Tizzy"></a> <a href="#"><img src="images/2.jpg"
					                                                                                                                                            alt="Tizzy"></a> <a
								href="#"><img src="images/3.jpg" alt="Tizzy"></a> <a href="#"><img src="images/4.jpg" alt="Tizzy"></a> <a href="#"><img src="images/5.jpg"
					                                                                                                                                    alt="Tizzy"></a> <a
								href="#"><img src="images/6.jpg" alt="Tizzy"></a> <a href="#"><img src="images/4.jpg" alt="Tizzy"></a> <a href="#"><img src="images/5.jpg"
					                                                                                                                                    alt="Tizzy"></a> <a
								href="#"><img src="images/6.jpg" alt="Tizzy"></a></div>
				</div>
				<div class="col-md-8 col-sm-7">
					<div class="row">
						<div class="col-xs-12">
							<div class="product-detail-main">
								<div class="product-item-details">
									<h1 class="product-item-name">Cross Colours Camo Print Tank half mengo</h1>
									<div class="product-info">
										<div class="price-box">
											<span class="price">7000₽</span>
											<del class="price old-price">5000₽</del>
										</div>
										<div class="product-info-stock-sku">
											<div>
												<label>SKU: </label>
												<span class="info-deta">20MVC-18</span>
											</div>
										</div>
									</div>
									<p>Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus
										et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus
										malesuada </p>
									<div class="product-dopinfo">
										<ul>
											<li>
												<a href="#!">Уточнить возможную скидику</a>
											</li>
											<li>
												<a href="#!">Скачать инструкцию</a>
											</li>
										</ul>
									</div>
									<div class="row product-contacts">
										<div class="col-md-6">
											<p>
												Консультация персонального менеджера<br>
												по телефону <a href="tel: +7999999999">+7 999 999 99 99</a>
											</p>
										</div>
										<div class="col-lg-6">
											<div class="product-feedback">
												<a href="#!" class="button basic">Остались вопросы? Напишите нам!</a>
											</div>
										</div>
									</div>
									<div class="row mt-10">
										<div class="col-md-6 product-size select-arrow mb-20">
											<label>Размер</label>
											<select class="selectpicker form-control" id="select-by-size">
												<option selected="selected" value="#">S</option>
												<option value="#">M</option>
												<option value="#">L</option>
											</select>
										</div>
										<div class="col-md-6 product-color select-arrow mb-20">
											<label>Цвет</label>
											<select class="selectpicker form-control" id="select-by-color">
												<option selected="selected" value="#">Blue</option>
												<option value="#">Green</option>
												<option value="#">Orange</option>
												<option value="#">White</option>
											</select>
										</div>
									</div>
									<div class="mb-30">
										<div class="product-qty">
											<label for="qty">Кол-во:</label>
											<div class="custom-qty">
												<button onclick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 1 ) result.value--;return false;"
												        class="reduced items" type="button"><i class="fa fa-minus"></i></button>
												<input type="text" class="input-text qty" title="Qty" value="1" maxlength="8" id="qty" name="qty">
												<button onclick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;"
												        class="increase items" type="button"><i class="fa fa-plus"></i></button>
											</div>
										</div>
										<div class="bottom-detail cart-button product-buttons">
											<ul>
												<li class="pro-cart-icon">
													<form>
														<button class="button basic"><span></span>Добавить в корзину</button>
													</form>
												</li>
												<li>
													<a href="#!" class="button basic">Сравнить</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="pb-30">
		<div class="container">
			<div class="product-detail-tab">
				<div class="row">
					<div class="col-md-12">
						<div id="tabs">
							<ul class="nav nav-tabs">
								<li><a class="tab-Description selected" title="Description">Описание</a></li>
								<li><a class="tab-Product-Tags" title="Product-Tags">Спецификация</a></li>
								<li><a class="tab-Reviews" title="Reviews">Отзывы</a></li>
								<li class="right-side"><a class="tab-Delivery" title="Delivery">Доставка</a></li>
								<li class="right-side"><a class="tab-Payment" title="Payment">Оплата</a></li>
								<li class="right-side"><a class="tab-Garant" title="Garant">Гарантия</a></li>
							</ul>
						</div>
						<div id="items">
							<div class="tab_content">
								<ul>
									<li>
										<div class="items-Description selected">
											<div class="Description"><strong>The standard Lorem Ipsum passage, used since the 1500s</strong><br/>
												<p>Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus
													orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim
													semper. Nulla luctus malesuada Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
													the industry's standard dummy took a galley of type and scrambled it to make a type specimen book. It has survived not only five
													centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with
													the release of Letraset sheets</p>
												<p>Tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
													Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada Lorem Ipsum is simply
													dummy text of the printing and typesetting industry.</p>
											</div>
										</div>
									</li>
									<li>
										<div class="items-Product-Tags"><strong>Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</strong><br/>
											Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab
											illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur
											aut odit aut fugit, sed quia consequuntur
										</div>
									</li>
									<li>
										<div class="items-Reviews">
											<div class="comments-area">
												<h4>Comments<span>(2)</span></h4>
												<ul class="comment-list mt-30">
													<li>
														<div class="comment-user"><img src="images/comment-user.jpg" alt="Tizzy"></div>
														<div class="comment-detail">
															<div class="user-name">John Doe</div>
															<div class="post-info">
																<ul>
																	<li>Fab 11, 2016</li>
																	<li><a href="#"><i class="fa fa-reply"></i>Reply</a></li>
																</ul>
															</div>
															<p>Consectetur adipiscing elit integer sit amet augue laoreet maximus nuncac.</p>
														</div>
														<ul class="comment-list child-comment">
															<li>
																<div class="comment-user"><img src="images/comment-user.jpg" alt="Tizzy"></div>
																<div class="comment-detail">
																	<div class="user-name">John Doe</div>
																	<div class="post-info">
																		<ul>
																			<li>Fab 11, 2016</li>
																			<li><a href="#"><i class="fa fa-reply"></i>Reply</a></li>
																		</ul>
																	</div>
																	<p>Consectetur adipiscing elit integer sit amet augue laoreet maximus nuncac.</p>
																</div>
															</li>
															<li>
																<div class="comment-user"><img src="images/comment-user.jpg" alt="Tizzy"></div>
																<div class="comment-detail">
																	<div class="user-name">John Doe</div>
																	<div class="post-info">
																		<ul>
																			<li>Fab 11, 2016</li>
																			<li><a href="#"><i class="fa fa-reply"></i>Reply</a></li>
																		</ul>
																	</div>
																	<p>Consectetur adipiscing elit integer sit amet augue laoreet maximus nuncac.</p>
																</div>
															</li>
														</ul>
													</li>
													<li>
														<div class="comment-user"><img src="images/comment-user.jpg" alt="Tizzy"></div>
														<div class="comment-detail">
															<div class="user-name">John Doe</div>
															<div class="post-info">
																<ul>
																	<li>Fab 11, 2016</li>
																	<li><a href="#"><i class="fa fa-reply"></i>Reply</a></li>
																</ul>
															</div>
															<p>Consectetur adipiscing elit integer sit amet augue laoreet maximus nuncac.</p>
														</div>
													</li>
												</ul>
											</div>
											<div class="main-form mt-30">
												<h4>Leave a comments</h4>
												<div class="row mt-30">
													<form>
														<div class="col-sm-4 mb-30">
															<input type="text" placeholder="Name" required>
														</div>
														<div class="col-sm-4 mb-30">
															<input type="email" placeholder="Email" required>
														</div>
														<div class="col-sm-4 mb-30">
															<input type="text" placeholder="Website" required>
														</div>
														<div class="col-xs-12 mb-30">
															<textarea cols="30" rows="3" placeholder="Message" required></textarea>
														</div>
														<div class="col-xs-12 mb-30">
															<button class="btn-black" name="submit" type="submit">Submit</button>
														</div>
													</form>
												</div>
											</div>
										</div>
									</li>
									<li>
										<div class="items-Garant">
											<div class="Garant"><strong>The standard Lorem Ipsum passage, used since the 1500s1</strong><br/>
												<p>Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus
													orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim
													semper. Nulla luctus malesuada Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
													the industry's standard dummy took a galley of type and scrambled it to make a type specimen book. It has survived not only five
													centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with
													the release of Letraset sheets</p>
												<p>Tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
													Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada Lorem Ipsum is simply
													dummy text of the printing and typesetting industry.</p>
											</div>
										</div>
									</li>
									<li>
										<div class="items-Delivery">
											<div class="Delivery"><strong>The standard Lorem Ipsum passage, used since the 1500s2</strong><br/>
												<p>Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus
													orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim
													semper. Nulla luctus malesuada Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
													the industry's standard dummy took a galley of type and scrambled it to make a type specimen book. It has survived not only five
													centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with
													the release of Letraset sheets</p>
												<p>Tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
													Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada Lorem Ipsum is simply
													dummy text of the printing and typesetting industry.</p>
											</div>
										</div>
									</li>
									<li>
										<div class="items-Payment">
											<div class="Payment"><strong>The standard Lorem Ipsum passage, used since the 1500s3</strong><br/>
												<p>Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus
													orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim
													semper. Nulla luctus malesuada Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
													the industry's standard dummy took a galley of type and scrambled it to make a type specimen book. It has survived not only five
													centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with
													the release of Letraset sheets</p>
												<p>Tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
													Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada Lorem Ipsum is simply
													dummy text of the printing and typesetting industry.</p>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="pb-95">
		<div class="container">
			<div class="product-slider mb-60">
				<div class="row">
					<div class="col-xs-12">
						<div class="heading-part align-center mb-40">
							<h2 class="main_title">С этим товаром покупают:</h2>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="product-slider-main position-r product-slider-custom"><!-- dresses -->
						<div class="owl-carousel pro_cat_slider"><!-- id="product-slider" -->
							<div class="item">
								<div class="product-item">
									<div class="product-image"><a href="#"> <img src="images/1.jpg" alt=""> </a>
										<div class="product-detail-inner">
											<div class="detail-inner-left left-side">
												<ul>
													<li class="pro-cart-icon">
														<form>
															<button title="Add to Cart"><i class="fa fa-cart-plus"></i></button>
														</form>
													</li>
													<li class="pro-wishlist-icon"><a href="#"><i class="fa fa-heart"></i></a></li>
													<li class="pro-compare-icon"><a href="#"><i class="fa fa-bar-chart"></i></a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="product-item-details">
										<div class="product-item-name"><a href="#">Defyant Reversible Dot Shorts</a></div>
										<div class="price-box"><span class="price">$80.00</span>
											<del class="price old-price">$100.00</del>
										</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="product-item">
									<div class="product-image"><a href="#"> <img src="images/2.jpg" alt=""> </a>
										<div class="product-detail-inner">
											<div class="detail-inner-left left-side">
												<ul>
													<li class="pro-cart-icon">
														<form>
															<button title="Add to Cart"><i class="fa fa-cart-plus"></i></button>
														</form>
													</li>
													<li class="pro-wishlist-icon"><a href="#"><i class="fa fa-heart"></i></a></li>
													<li class="pro-compare-icon"><a href="#"><i class="fa fa-bar-chart"></i></a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="product-item-details">
										<div class="product-item-name"><a href="#">Defyant Reversible Dot Shorts</a></div>
										<div class="price-box"><span class="price">$80.00</span>
											<del class="price old-price">$100.00</del>
										</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="product-item">
									<div class="product-image"><a href="#"> <img src="images/3.jpg" alt=""> </a>
										<div class="product-detail-inner">
											<div class="detail-inner-left left-side">
												<ul>
													<li class="pro-cart-icon">
														<form>
															<button title="Add to Cart"><i class="fa fa-cart-plus"></i></button>
														</form>
													</li>
													<li class="pro-wishlist-icon"><a href="#"><i class="fa fa-heart"></i></a></li>
													<li class="pro-compare-icon"><a href="#"><i class="fa fa-bar-chart"></i></a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="product-item-details">
										<div class="product-item-name"><a href="#">Defyant Reversible Dot Shorts</a></div>
										<div class="price-box"><span class="price">$80.00</span>
											<del class="price old-price">$100.00</del>
										</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="product-item">
									<div class="product-image"><a href="#"> <img src="images/4.jpg" alt=""> </a>
										<div class="product-detail-inner">
											<div class="detail-inner-left left-side">
												<ul>
													<li class="pro-cart-icon">
														<form>
															<button title="Add to Cart"><i class="fa fa-cart-plus"></i></button>
														</form>
													</li>
													<li class="pro-wishlist-icon"><a href="#"><i class="fa fa-heart"></i></a></li>
													<li class="pro-compare-icon"><a href="#"><i class="fa fa-bar-chart"></i></a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="product-item-details">
										<div class="product-item-name"><a href="#">Defyant Reversible Dot Shorts</a></div>
										<div class="price-box"><span class="price">$80.00</span>
											<del class="price old-price">$100.00</del>
										</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="product-item">
									<div class="product-image"><a href="#"> <img src="images/5.jpg" alt=""> </a>
										<div class="product-detail-inner">
											<div class="detail-inner-left left-side">
												<ul>
													<li class="pro-cart-icon">
														<form>
															<button title="Add to Cart"><i class="fa fa-cart-plus"></i></button>
														</form>
													</li>
													<li class="pro-wishlist-icon"><a href="#"><i class="fa fa-heart"></i></a></li>
													<li class="pro-compare-icon"><a href="#"><i class="fa fa-bar-chart"></i></a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="product-item-details">
										<div class="product-item-name"><a href="#">Defyant Reversible Dot Shorts</a></div>
										<div class="price-box"><span class="price">$80.00</span>
											<del class="price old-price">$100.00</del>
										</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="product-item">
									<div class="product-image"><a href="#"> <img src="images/6.jpg" alt=""> </a>
										<div class="product-detail-inner">
											<div class="detail-inner-left left-side">
												<ul>
													<li class="pro-cart-icon">
														<form>
															<button title="Add to Cart"><i class="fa fa-cart-plus"></i></button>
														</form>
													</li>
													<li class="pro-wishlist-icon"><a href="#"><i class="fa fa-heart"></i></a></li>
													<li class="pro-compare-icon"><a href="#"><i class="fa fa-bar-chart"></i></a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="product-item-details">
										<div class="product-item-name"><a href="#">Defyant Reversible Dot Shorts</a></div>
										<div class="price-box"><span class="price">$80.00</span>
											<del class="price old-price">$100.00</del>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="product-slider mb-60">
				<div class="row">
					<div class="col-xs-12">
						<div class="heading-part align-center mb-40">
							<h2 class="main_title">Вы смотрели:</h2>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="product-slider-main position-r product-slider-custom"><!-- dresses -->
						<div class="owl-carousel pro_cat_slider"><!-- id="product-slider" -->
							<div class="item">
								<div class="product-item">
									<div class="product-image"><a href="#"> <img src="images/1.jpg" alt=""> </a>
										<div class="product-detail-inner">
											<div class="detail-inner-left left-side">
												<ul>
													<li class="pro-cart-icon">
														<form>
															<button title="Add to Cart"><i class="fa fa-cart-plus"></i></button>
														</form>
													</li>
													<li class="pro-wishlist-icon"><a href="#"><i class="fa fa-heart"></i></a></li>
													<li class="pro-compare-icon"><a href="#"><i class="fa fa-bar-chart"></i></a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="product-item-details">
										<div class="product-item-name"><a href="#">Defyant Reversible Dot Shorts</a></div>
										<div class="price-box"><span class="price">$80.00</span>
											<del class="price old-price">$100.00</del>
										</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="product-item">
									<div class="product-image"><a href="#"> <img src="images/2.jpg" alt=""> </a>
										<div class="product-detail-inner">
											<div class="detail-inner-left left-side">
												<ul>
													<li class="pro-cart-icon">
														<form>
															<button title="Add to Cart"><i class="fa fa-cart-plus"></i></button>
														</form>
													</li>
													<li class="pro-wishlist-icon"><a href="#"><i class="fa fa-heart"></i></a></li>
													<li class="pro-compare-icon"><a href="#"><i class="fa fa-bar-chart"></i></a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="product-item-details">
										<div class="product-item-name"><a href="#">Defyant Reversible Dot Shorts</a></div>
										<div class="price-box"><span class="price">$80.00</span>
											<del class="price old-price">$100.00</del>
										</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="product-item">
									<div class="product-image"><a href="#"> <img src="images/3.jpg" alt=""> </a>
										<div class="product-detail-inner">
											<div class="detail-inner-left left-side">
												<ul>
													<li class="pro-cart-icon">
														<form>
															<button title="Add to Cart"><i class="fa fa-cart-plus"></i></button>
														</form>
													</li>
													<li class="pro-wishlist-icon"><a href="#"><i class="fa fa-heart"></i></a></li>
													<li class="pro-compare-icon"><a href="#"><i class="fa fa-bar-chart"></i></a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="product-item-details">
										<div class="product-item-name"><a href="#">Defyant Reversible Dot Shorts</a></div>
										<div class="price-box"><span class="price">$80.00</span>
											<del class="price old-price">$100.00</del>
										</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="product-item">
									<div class="product-image"><a href="#"> <img src="images/4.jpg" alt=""> </a>
										<div class="product-detail-inner">
											<div class="detail-inner-left left-side">
												<ul>
													<li class="pro-cart-icon">
														<form>
															<button title="Add to Cart"><i class="fa fa-cart-plus"></i></button>
														</form>
													</li>
													<li class="pro-wishlist-icon"><a href="#"><i class="fa fa-heart"></i></a></li>
													<li class="pro-compare-icon"><a href="#"><i class="fa fa-bar-chart"></i></a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="product-item-details">
										<div class="product-item-name"><a href="#">Defyant Reversible Dot Shorts</a></div>
										<div class="price-box"><span class="price">$80.00</span>
											<del class="price old-price">$100.00</del>
										</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="product-item">
									<div class="product-image"><a href="#"> <img src="images/5.jpg" alt=""> </a>
										<div class="product-detail-inner">
											<div class="detail-inner-left left-side">
												<ul>
													<li class="pro-cart-icon">
														<form>
															<button title="Add to Cart"><i class="fa fa-cart-plus"></i></button>
														</form>
													</li>
													<li class="pro-wishlist-icon"><a href="#"><i class="fa fa-heart"></i></a></li>
													<li class="pro-compare-icon"><a href="#"><i class="fa fa-bar-chart"></i></a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="product-item-details">
										<div class="product-item-name"><a href="#">Defyant Reversible Dot Shorts</a></div>
										<div class="price-box"><span class="price">$80.00</span>
											<del class="price old-price">$100.00</del>
										</div>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="product-item">
									<div class="product-image"><a href="#"> <img src="images/6.jpg" alt=""> </a>
										<div class="product-detail-inner">
											<div class="detail-inner-left left-side">
												<ul>
													<li class="pro-cart-icon">
														<form>
															<button title="Add to Cart"><i class="fa fa-cart-plus"></i></button>
														</form>
													</li>
													<li class="pro-wishlist-icon"><a href="#"><i class="fa fa-heart"></i></a></li>
													<li class="pro-compare-icon"><a href="#"><i class="fa fa-bar-chart"></i></a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="product-item-details">
										<div class="product-item-name"><a href="#">Defyant Reversible Dot Shorts</a></div>
										<div class="price-box"><span class="price">$80.00</span>
											<del class="price old-price">$100.00</del>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- CONTAINER END -->

	<!-- FOOTER START -->
	<div class="footer dark-bg">
		<div class="container">
			<div class="footer-inner">
				<div class="footer-middle">
					<div class="row">
						<div class="col-md-3 col-sm-12 col-xs-12 f-col">
							<div class="footer-static-block"><span class="opener plus"></span>
								<h3 class="title">Services</h3>
								<ul class="footer-block-contant link">
									<li><a><span>■</span> About Us</a></li>
									<li><a><span>■</span> Jobs</a></li>
									<li><a><span>■</span> Affiliates</a></li>
									<li><a><span>■</span> Contact</a></li>
									<li><a><span>■</span> Jobs</a></li>
									<li><a><span>■</span> Affiliates</a></li>
								</ul>
							</div>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12 align-center">
							<div class="footer-static-block"><span class="opener plus"></span>
								<h3 class="title hidden-lg hidden-md">Tizzy</h3>
								<div class="footer-block-contant">
									<div class="f-logo hidden-xs hidden-sm"><a href="index.html" class=""> <img src="images/footer-logo.png" alt="Tizzy"> </a></div>
									<p>Aenean dapibusMorbi dictum orci non vehicula sollicitudin. Sed auctor dui nibh, id elementum dolor dictum et. Nulla eu purus</p>
								</div>
							</div>
							<div class="footer-static-block"><span class="opener plus"></span>
								<h3 class="title hidden-lg hidden-md">Newsletter</h3>
								<div class="newsletter footer-block-contant">
									<form>
										<div class="newsletter-inner">
											<input type="email" placeholder="Sign up for Newsletter">
											<button title="Subscribe">Subscribe</button>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-12 col-xs-12 f-col">
							<div class="footer-static-block f-address"><span class="opener plus"></span>
								<h3 class="title">Contact Us</h3>
								<div class=""> <!-- <span class="opener plus"></span> -->
									<ul class="footer-block-contant address-footer">
										<li class="item">
											<p>150-A Appolo aprtment, opp. Hopewell Junction, Allen st Road, new york-405001.</p>
										</li>
										<li class="item">
											<p><a>infoservices@Tizzy.com </a></p>
										</li>
										<li class="item">
											<p>(+91) 9834567890</p>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<hr>
				<div class="footer-bottom mb-30">
					<div class="row">
						<div class="col-md-4 col-sm-12 center-sm">
							<div class="payment left-side float-none-sm float-none-xs center-xs">
								<ul class="payment_icon">
									<li class="discover"><a></a></li>
									<li class="visa"><a></a></li>
									<li class="mastro"><a></a></li>
									<li class="paypal"><a></a></li>
								</ul>
							</div>
						</div>
						<div class="col-md-4 col-sm-12 center-sm">
							<div class="copy-right center-xs">© 2017 All Rights Reserved. Design By <a href="#">Webcotheme</a></div>
						</div>
						<div class="col-md-4 col-sm-12 center-sm">
							<div class="footer_social pt-xs-15 right-side float-none-xs float-none-sm center-xs mt-xs-15">
								<ul class="social-icon">
									<li><a title="Facebook" class="facebook"><i class="fa fa-facebook"> </i></a></li>
									<li><a title="Twitter" class="twitter"><i class="fa fa-twitter"> </i></a></li>
									<li><a title="Linkedin" class="linkedin"><i class="fa fa-linkedin"> </i></a></li>
									<li><a title="RSS" class="rss"><i class="fa fa-rss"> </i></a></li>
									<li><a title="Pinterest" class="pinterest"><i class="fa fa-pinterest"> </i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<hr>
				<div class="footer-bottom mt-30">
					<div class="site-link align-center">
						<ul>
							<li><a>Cycling</a>/</li>
							<li><a>Running</a>/</li>
							<li><a>Swimming</a>/</li>
							<li><a>Football</a>/</li>
							<li><a>Golf</a>/</li>
							<li><a>Tools</a>/</li>
							<li><a>Bedding</a>/</li>
							<li><a>Food</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="scroll-top">
		<div id="scrollup"></div>
	</div>
	<!-- FOOTER END -->
</div>
<script src="/local/templates/index/js/jquery-1.12.3.min.js"></script>
<script src="/local/templates/index/js/bootstrap.min.js"></script>
<script src="/local/templates/index/js/jquery-ui.min.js"></script>
<script src="/local/templates/index/js/fotorama.js"></script>
<script src="/local/templates/index/js/owl.carousel.min.js"></script>
<script src="/local/templates/index/js/custom.js"></script>
</body>
</html>
