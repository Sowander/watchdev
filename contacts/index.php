<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?>


	<section class="contacts-ptb">
		<div class="container">
			<div class="about-detail">
				<div class="row">
					<div class="col-sm-12">
						<p>
							<? $APPLICATION->IncludeComponent(
								"bitrix:main.include",
								"",
								Array(
									"AREA_FILE_SHOW" => "file",
									"PATH" => SITE_DIR . "include/contact1.php",
									"AREA_FILE_RECURSIVE" => "N",
									"EDIT_MODE" => "php",
								),
								false,
								Array('HIDE_ICONS' => 'N')
							); ?>

						</p>
					</div>
				</div>
			</div>
		</div>
	</section>


	<section class="pb-50">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div class="row">
						<div class="col-xs-12">
							<div class="heading-part align-center mb-40">
								<h2 class="main_title">Напишите нам</h2>
							</div>
						</div>
					</div>
					<div class="main-form">
						<div class="row">

							<? $APPLICATION->IncludeComponent(
								"salvador:main.feedback",
								"template",
								array(
									"USE_CAPTCHA" => "Y",
									"OK_TEXT" => "Спасибо, ваше сообщение принято.",
									"EMAIL_TO" => "kulbachenko@gmail.com",
									"REQUIRED_FIELDS" => array(
										0 => "PHONE",
										1 => "NAME",
										2 => "EMAIL",
										3 => "MESSAGE"

									),
									"EVENT_MESSAGE_ID" => array(
										0 => "7",
									),
									"COMPONENT_TEMPLATE" => "template"
								),
								false
							); ?>


						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="row">
						<div class="col-xs-12">
							<div class="heading-part align-center mb-40">
								<h2 class="main_title">Контакты</h2>
							</div>
						</div>
					</div>
					<div class="footer-static-block">
						<div class="footer-block-contant address-footer">


							<? $APPLICATION->IncludeComponent(
								"bitrix:main.include",
								"",
								Array(
									"AREA_FILE_SHOW" => "file",
									"PATH" => SITE_DIR . "include/contact2.php",
									"AREA_FILE_RECURSIVE" => "N",
									"EDIT_MODE" => "php",
								),
								false,
								Array('HIDE_ICONS' => 'N')
							); ?>

							<div class="footer_social pt-xs-15 float-none-xs float-none-sm contact-page">
								<ul class="social-icon">
									<? $APPLICATION->IncludeComponent(
										"bitrix:main.include",
										"",
										Array(
											"AREA_FILE_SHOW" => "file",
											"PATH" => SITE_DIR . "include/contact3.php",
											"AREA_FILE_RECURSIVE" => "N",
											"EDIT_MODE" => "php",
										),
										false,
										Array('HIDE_ICONS' => 'N')
									); ?>

								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php") ?>