<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("О магазине");
?>

	<section class="pt-55 mobile-padding">
		<div class="container">
			<div class="about-detail">
				<div class="row">
					<div class="col-sm-12">


						<div class="attentive-block">
							<div class="sw-about-company">
								<div class="sw-about-company__title">О компании</div>
								<div class="sw_about-company__description">


									<? $APPLICATION->IncludeComponent(
										"bitrix:main.include",
										"",
										Array(
											"AREA_FILE_SHOW" => "file",
											"PATH" => SITE_DIR . "include/about.php",
											"AREA_FILE_RECURSIVE" => "N",
											"EDIT_MODE" => "php",
										),
										false,
										Array('HIDE_ICONS' => 'N')
									); ?>

								</div>
							</div>
						</div>


						<div class="advantages-block">
							<div class="sw-advantages-items__title">Преимущества</div>
							<div class="sw-advantages-items">
								<div class="sw-advantages-item">
									<div class="sw-advantages-item__title">Подарки</div>
									<div class="sw-advantages-item__icon">
										<i class="fa fa-gift fa-3x" aria-hidden="true"></i>
									</div>
									<div class="sw-advantages-item__description">Благодаря многолетнему опыту работы и отличному знанию индустрии часов, мы предлагаем покупателям
										только оригинальные мужские и женские наручные часы от известных мировых производителей. Кроме того, в нашем ассортименте аксессуары,
										ювелирные украшения и подарки от самых популярных брендов.
									</div>
								</div>
								<div class="sw-advantages-item">
									<div class="sw-advantages-item__title">Доставка</div>
									<div class="sw-advantages-item__icon">
										<i class="fa fa-gift fa-3x" aria-hidden="true"></i>
									</div>
									<div class="sw-advantages-item__description">Наши магазины обладают различными наградами и официально сертифицированы компаниями и брендами.
									</div>
								</div>
								<div class="sw-advantages-item">
									<div class="sw-advantages-item__title">Подарки</div>
									<div class="sw-advantages-item__icon">
										<i class="fa fa-gift fa-3x" aria-hidden="true"></i>
									</div>
									<div class="sw-advantages-item__description">Благодаря многолетнему опыту работы и отличному знанию индустрии часов, мы предлагаем покупателям
										только оригинальные мужские и женские наручные часы от известных мировых производителей. Кроме того, в нашем ассортименте аксессуары,
										ювелирные украшения и подарки от самых популярных брендов.
									</div>
								</div>
								<div class="sw-advantages-item">
									<div class="sw-advantages-item__title">Подарки</div>
									<div class="sw-advantages-item__icon">
										<i class="fa fa-gift fa-3x" aria-hidden="true"></i>
									</div>
									<div class="sw-advantages-item__description">Благодаря многолетнему опыту работы и отличному знанию индустрии часов, мы предлагаем покупателям
										только оригинальные мужские и женские наручные часы от известных мировых производителей. Кроме того, в нашем ассортименте аксессуары,
										ювелирные украшения и подарки от самых популярных брендов.
									</div>
								</div>
								<div class="sw-advantages-item">
									<div class="sw-advantages-item__title">Подарки</div>
									<div class="sw-advantages-item__icon">
										<i class="fa fa-gift fa-3x" aria-hidden="true"></i>
									</div>
									<div class="sw-advantages-item__description">Благодаря многолетнему опыту работы и отличному знанию индустрии часов, мы предлагаем покупателям
										только оригинальные мужские и женские наручные часы от известных мировых производителей. Кроме того, в нашем ассортименте аксессуары,
										ювелирные украшения и подарки от самых популярных брендов.
									</div>
								</div>
							</div>
						</div>


					</div>


				</div>
			</div>
		</div>
		</div>
	</section>


<? /*
  <section class="pb-50">
    <div class="container">
      <div class="about-blocks">
        <div class="row">
          <div class="col-md-8">
            <div class="about-block">
              <img src="/images/blog_img1.jpg" alt="">
              <div class="about-block-wrapper">
                <div class="about-block-iner">
                  <p>
				  
				  <?$APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => SITE_DIR."include/about1.php",
							"AREA_FILE_RECURSIVE" => "N",
							"EDIT_MODE" => "php",
						),
						false,
						Array('HIDE_ICONS' => 'N')
					);?>
				  </p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="about-block">
              <img src="/images/blog_img2.jpg" alt="">
              <div class="about-block-wrapper">
                <div class="about-block-iner">
                  <p>
				   <?$APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => SITE_DIR."include/about2.php",
							"AREA_FILE_RECURSIVE" => "N",
							"EDIT_MODE" => "php",
						),
						false,
						Array('HIDE_ICONS' => 'N')
					);?>
				  </p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="about-block">
              <img src="/images/blog_img3.jpg" alt="">
              <div class="about-block-wrapper">
                <div class="about-block-iner">
                  <p> <?$APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => SITE_DIR."include/about3.php",
							"AREA_FILE_RECURSIVE" => "N",
							"EDIT_MODE" => "php",
						),
						false,
						Array('HIDE_ICONS' => 'N')
					);?></p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-8">
            <div class="about-block">
              <img src="/images/blog_img4.jpg" alt="">
              <div class="about-block-wrapper">
                <div class="about-block-iner">
                  <p> <?$APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_SHOW" => "file",
							"PATH" => SITE_DIR."include/about4.php",
							"AREA_FILE_RECURSIVE" => "N",
							"EDIT_MODE" => "php",
						),
						false,
						Array('HIDE_ICONS' => 'N')
					);?></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  */ ?>


<? $APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"about_cert",
	array(
		"ACTIVE_DATE_FORMAT" => "",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "CODE",
			2 => "XML_ID",
			3 => "NAME",
			4 => "TAGS",
			5 => "SORT",
			6 => "PREVIEW_TEXT",
			7 => "PREVIEW_PICTURE",
			8 => "DETAIL_TEXT",
			9 => "DETAIL_PICTURE",
			10 => "DATE_ACTIVE_FROM",
			11 => "ACTIVE_FROM",
			12 => "DATE_ACTIVE_TO",
			13 => "ACTIVE_TO",
			14 => "SHOW_COUNTER",
			15 => "SHOW_COUNTER_START",
			16 => "IBLOCK_TYPE_ID",
			17 => "IBLOCK_ID",
			18 => "IBLOCK_CODE",
			19 => "IBLOCK_NAME",
			20 => "IBLOCK_EXTERNAL_ID",
			21 => "DATE_CREATE",
			22 => "CREATED_BY",
			23 => "CREATED_USER_NAME",
			24 => "TIMESTAMP_X",
			25 => "MODIFIED_BY",
			26 => "USER_NAME",
			27 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "10",
		"IBLOCK_TYPE" => "references",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "40",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "50",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "CLASS",
			2 => "NAME2",
			3 => "NAME3",
			4 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "ID",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "about_cert"
	),
	false
); ?>


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>