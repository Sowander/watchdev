<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Интернет-магазин ");
?>
	<div>

<? $APPLICATION->IncludeComponent("bitrix:news.list", "main_banner", Array(
	"ACTIVE_DATE_FORMAT" => "",    // Формат показа даты
	"ADD_SECTIONS_CHAIN" => "N",    // Включать раздел в цепочку навигации
	"AJAX_MODE" => "N",    // Включить режим AJAX
	"AJAX_OPTION_ADDITIONAL" => "",    // Дополнительный идентификатор
	"AJAX_OPTION_HISTORY" => "N",    // Включить эмуляцию навигации браузера
	"AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
	"AJAX_OPTION_STYLE" => "Y",    // Включить подгрузку стилей
	"CACHE_FILTER" => "N",    // Кешировать при установленном фильтре
	"CACHE_GROUPS" => "Y",    // Учитывать права доступа
	"CACHE_TIME" => "3600",    // Время кеширования (сек.)
	"CACHE_TYPE" => "A",    // Тип кеширования
	"CHECK_DATES" => "Y",    // Показывать только активные на данный момент элементы
	"DETAIL_URL" => "",    // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
	"DISPLAY_BOTTOM_PAGER" => "N",    // Выводить под списком
	"DISPLAY_DATE" => "Y",    // Выводить дату элемента
	"DISPLAY_NAME" => "Y",    // Выводить название элемента
	"DISPLAY_PICTURE" => "Y",    // Выводить изображение для анонса
	"DISPLAY_PREVIEW_TEXT" => "Y",    // Выводить текст анонса
	"DISPLAY_TOP_PAGER" => "N",    // Выводить над списком
	"FIELD_CODE" => array(    // Поля
		0 => "",
		1 => "",
	),
	"FILTER_NAME" => "",    // Фильтр
	"HIDE_LINK_WHEN_NO_DETAIL" => "N",    // Скрывать ссылку, если нет детального описания
	"IBLOCK_ID" => "4",    // Код информационного блока
	"IBLOCK_TYPE" => "references",    // Тип информационного блока (используется только для проверки)
	"INCLUDE_IBLOCK_INTO_CHAIN" => "N",    // Включать инфоблок в цепочку навигации
	"INCLUDE_SUBSECTIONS" => "Y",    // Показывать элементы подразделов раздела
	"MESSAGE_404" => "",    // Сообщение для показа (по умолчанию из компонента)
	"NEWS_COUNT" => "200",    // Количество новостей на странице
	"PAGER_BASE_LINK_ENABLE" => "N",    // Включить обработку ссылок
	"PAGER_DESC_NUMBERING" => "N",    // Использовать обратную навигацию
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",    // Время кеширования страниц для обратной навигации
	"PAGER_SHOW_ALL" => "N",    // Показывать ссылку "Все"
	"PAGER_SHOW_ALWAYS" => "N",    // Выводить всегда
	"PAGER_TEMPLATE" => ".default",    // Шаблон постраничной навигации
	"PAGER_TITLE" => "Новости",    // Название категорий
	"PARENT_SECTION" => "",    // ID раздела
	"PARENT_SECTION_CODE" => "",    // Код раздела
	"PREVIEW_TRUNCATE_LEN" => "",    // Максимальная длина анонса для вывода (только для типа текст)
	"PROPERTY_CODE" => array(    // Свойства
		0 => "",
		1 => "",
	),
	"SET_BROWSER_TITLE" => "N",    // Устанавливать заголовок окна браузера
	"SET_LAST_MODIFIED" => "N",    // Устанавливать в заголовках ответа время модификации страницы
	"SET_META_DESCRIPTION" => "N",    // Устанавливать описание страницы
	"SET_META_KEYWORDS" => "N",    // Устанавливать ключевые слова страницы
	"SET_STATUS_404" => "N",    // Устанавливать статус 404
	"SET_TITLE" => "N",    // Устанавливать заголовок страницы
	"SHOW_404" => "N",    // Показ специальной страницы
	"SORT_BY1" => "ACTIVE_FROM",    // Поле для первой сортировки новостей
	"SORT_BY2" => "ID",    // Поле для второй сортировки новостей
	"SORT_ORDER1" => "DESC",    // Направление для первой сортировки новостей
	"SORT_ORDER2" => "ASC",    // Направление для второй сортировки новостей
	"STRICT_SECTION_CHECK" => "N",    // Строгая проверка раздела для показа списка
),
	false
); ?>


<? $APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"main_banners_block",
	array(
		"ACTIVE_DATE_FORMAT" => "",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "5",
		"IBLOCK_TYPE" => "references",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "300",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "NAME2",
			1 => "NAME3",
			2 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "ID",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "main_banners_block"
	),
	false
); ?>

	<!-- CONTAIN START -->


<? $APPLICATION->IncludeComponent(
	"bitrix:catalog.section",
	"main_items", 
	array(
		"IBLOCK_TYPE_ID" => "catalog",
		"IBLOCK_ID" => "2",
		"BASKET_URL" => "/personal/cart/",
		"COMPONENT_TEMPLATE" => "main_items",
		"IBLOCK_TYPE" => "catalog",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_CODE" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "desc",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER2" => "desc",
		"FILTER_NAME" => "arrFilter",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "Y",
		"HIDE_NOT_AVAILABLE" => "N",
		"PAGE_ELEMENT_COUNT" => "8",
		"LINE_ELEMENT_COUNT" => "3",
		"PROPERTY_CODE" => array(
			0 => "NEWPRODUCT",
			1 => "",
		),
		"OFFERS_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"OFFERS_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_ORDER" => "desc",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER2" => "desc",
		"OFFERS_LIMIT" => "5",
		"TEMPLATE_THEME" => "site",
		"PRODUCT_DISPLAY_MODE" => "N",
		"ADD_PICT_PROP" => "-",
		"LABEL_PROP" => array(
			0 => "NEWPRODUCT",
		),
		"OFFER_ADD_PICT_PROP" => "-",
		"OFFER_TREE_PROPS" => array(
			0 => "COLOR_REF",
			1 => "SIZES_SHOES",
			2 => "SIZES_CLOTHES",
		),
		"PRODUCT_SUBSCRIPTION" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_OLD_PRICE" => "Y",
		"SHOW_CLOSE_POPUP" => "N",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"SECTION_URL" => "",
		"DETAIL_URL" => "",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SEF_MODE" => "N",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"BROWSER_TITLE" => "-",
		"SET_META_KEYWORDS" => "N",
		"META_KEYWORDS" => "-",
		"SET_META_DESCRIPTION" => "N",
		"META_DESCRIPTION" => "-",
		"SET_LAST_MODIFIED" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"CACHE_FILTER" => "N",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"USE_PRODUCT_QUANTITY" => "Y",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"ADD_PROPERTIES_TO_BASKET" => "N",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRODUCT_PROPERTIES" => array(),
		"OFFERS_CART_PROPERTIES" => array(
			0 => "COLOR_REF",
			1 => "SIZES_SHOES",
			2 => "SIZES_CLOTHES",
		),
		"ADD_TO_BASKET_ACTION" => "ADD",
		"PAGER_TEMPLATE" => "round",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Товары",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "Y",
		"SHOW_404" => "Y",
		"MESSAGE_404" => "",
		"CUSTOM_FILTER" => "",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"PROPERTY_CODE_MOBILE" => array(),
		"BACKGROUND_IMAGE" => "-",
		"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'3','BIG_DATA':false},{'VARIANT':'3','BIG_DATA':false}]",
		"ENLARGE_PRODUCT" => "STRICT",
		"PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons,compare",
		"SHOW_SLIDER" => "N",
		"LABEL_PROP_MOBILE" => array(),
		"LABEL_PROP_POSITION" => "top-left",
		"SHOW_MAX_QUANTITY" => "N",
		"RCM_TYPE" => "personal",
		"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
		"SHOW_FROM_SECTION" => "N",
		"DISPLAY_COMPARE" => "Y",
		"LAZY_LOAD" => "N",
		"LOAD_ON_SCROLL" => "N",
		"COMPATIBLE_MODE" => "N",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"COMPARE_PATH" => "/compare/",
		"MESS_BTN_COMPARE" => "Сравнить",
		"COMPARE_NAME" => "CATALOG_COMPARE_LIST",
		"FILE_404" => ""
	),
	false
); ?>


<? $APPLICATION->IncludeComponent("bitrix:news.list", "main_banners_block_undex", Array(
	"ACTIVE_DATE_FORMAT" => "",    // Формат показа даты
	"ADD_SECTIONS_CHAIN" => "N",    // Включать раздел в цепочку навигации
	"AJAX_MODE" => "N",    // Включить режим AJAX
	"AJAX_OPTION_ADDITIONAL" => "",    // Дополнительный идентификатор
	"AJAX_OPTION_HISTORY" => "N",    // Включить эмуляцию навигации браузера
	"AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
	"AJAX_OPTION_STYLE" => "Y",    // Включить подгрузку стилей
	"CACHE_FILTER" => "N",    // Кешировать при установленном фильтре
	"CACHE_GROUPS" => "Y",    // Учитывать права доступа
	"CACHE_TIME" => "3600",    // Время кеширования (сек.)
	"CACHE_TYPE" => "A",    // Тип кеширования
	"CHECK_DATES" => "Y",    // Показывать только активные на данный момент элементы
	"DETAIL_URL" => "",    // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
	"DISPLAY_BOTTOM_PAGER" => "N",    // Выводить под списком
	"DISPLAY_DATE" => "Y",    // Выводить дату элемента
	"DISPLAY_NAME" => "Y",    // Выводить название элемента
	"DISPLAY_PICTURE" => "Y",    // Выводить изображение для анонса
	"DISPLAY_PREVIEW_TEXT" => "Y",    // Выводить текст анонса
	"DISPLAY_TOP_PAGER" => "N",    // Выводить над списком
	"FIELD_CODE" => array(    // Поля
		0 => "",
		1 => "",
	),
	"FILTER_NAME" => "",    // Фильтр
	"HIDE_LINK_WHEN_NO_DETAIL" => "N",    // Скрывать ссылку, если нет детального описания
	"IBLOCK_ID" => "6",    // Код информационного блока
	"IBLOCK_TYPE" => "references",    // Тип информационного блока (используется только для проверки)
	"INCLUDE_IBLOCK_INTO_CHAIN" => "N",    // Включать инфоблок в цепочку навигации
	"INCLUDE_SUBSECTIONS" => "Y",    // Показывать элементы подразделов раздела
	"MESSAGE_404" => "",    // Сообщение для показа (по умолчанию из компонента)
	"NEWS_COUNT" => "3",    // Количество новостей на странице
	"PAGER_BASE_LINK_ENABLE" => "N",    // Включить обработку ссылок
	"PAGER_DESC_NUMBERING" => "N",    // Использовать обратную навигацию
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",    // Время кеширования страниц для обратной навигации
	"PAGER_SHOW_ALL" => "N",    // Показывать ссылку "Все"
	"PAGER_SHOW_ALWAYS" => "N",    // Выводить всегда
	"PAGER_TEMPLATE" => ".default",    // Шаблон постраничной навигации
	"PAGER_TITLE" => "Новости",    // Название категорий
	"PARENT_SECTION" => "",    // ID раздела
	"PARENT_SECTION_CODE" => "",    // Код раздела
	"PREVIEW_TRUNCATE_LEN" => "",    // Максимальная длина анонса для вывода (только для типа текст)
	"PROPERTY_CODE" => array(    // Свойства
		0 => "CLASS",
		1 => "NAME2",
		2 => "NAME3",
		3 => "",
	),
	"SET_BROWSER_TITLE" => "N",    // Устанавливать заголовок окна браузера
	"SET_LAST_MODIFIED" => "N",    // Устанавливать в заголовках ответа время модификации страницы
	"SET_META_DESCRIPTION" => "N",    // Устанавливать описание страницы
	"SET_META_KEYWORDS" => "N",    // Устанавливать ключевые слова страницы
	"SET_STATUS_404" => "N",    // Устанавливать статус 404
	"SET_TITLE" => "N",    // Устанавливать заголовок страницы
	"SHOW_404" => "N",    // Показ специальной страницы
	"SORT_BY1" => "ACTIVE_FROM",    // Поле для первой сортировки новостей
	"SORT_BY2" => "ID",    // Поле для второй сортировки новостей
	"SORT_ORDER1" => "DESC",    // Направление для первой сортировки новостей
	"SORT_ORDER2" => "ASC",    // Направление для второй сортировки новостей
	"STRICT_SECTION_CHECK" => "N",    // Строгая проверка раздела для показа списка
	"COMPONENT_TEMPLATE" => "main_banners_block"
),
	false
); ?>
<? /*

<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"main_block_shopby", 
	array(
		"ACTIVE_DATE_FORMAT" => "",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "7",
		"IBLOCK_TYPE" => "references",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "4",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "CLASS",
			1 => "NAME2",
			2 => "NAME3",
			3 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ID",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "main_block_shopby"
	),
	false
);?>

*/ ?>
<? /*
<?$APPLICATION->IncludeComponent("bitrix:news.list", "main_block_review", Array(
	"ACTIVE_DATE_FORMAT" => "",	// Формат показа даты
		"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
		"AJAX_MODE" => "N",	// Включить режим AJAX
		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
		"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
		"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
		"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
		"CACHE_GROUPS" => "Y",	// Учитывать права доступа
		"CACHE_TIME" => "3600",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
		"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
		"DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
		"DISPLAY_DATE" => "Y",	// Выводить дату элемента
		"DISPLAY_NAME" => "Y",	// Выводить название элемента
		"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
		"DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
		"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
		"FIELD_CODE" => array(	// Поля
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",	// Фильтр
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
		"IBLOCK_ID" => "8",	// Код информационного блока
		"IBLOCK_TYPE" => "references",	// Тип информационного блока (используется только для проверки)
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
		"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
		"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
		"NEWS_COUNT" => "40",	// Количество новостей на странице
		"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
		"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
		"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
		"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
		"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
		"PAGER_TITLE" => "Новости",	// Название категорий
		"PARENT_SECTION" => "",	// ID раздела
		"PARENT_SECTION_CODE" => "",	// Код раздела
		"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
		"PROPERTY_CODE" => array(	// Свойства
			0 => "",
			1 => "CLASS",
			2 => "NAME2",
			3 => "NAME3",
			4 => "",
		),
		"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
		"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
		"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
		"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
		"SET_STATUS_404" => "N",	// Устанавливать статус 404
		"SET_TITLE" => "N",	// Устанавливать заголовок страницы
		"SHOW_404" => "N",	// Показ специальной страницы
		"SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
		"SORT_BY2" => "ID",	// Поле для второй сортировки новостей
		"SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
		"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
		"STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа списка
		"COMPONENT_TEMPLATE" => "main_block_shopby"
	),
	false
);?>
*/ ?>


<? $APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"main_news",
	array(
		"ACTIVE_DATE_FORMAT" => "j F Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "1",
		"IBLOCK_TYPE" => "references",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "4",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "50",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "CLASS",
			2 => "NAME2",
			3 => "NAME3",
			4 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "ID",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "main_news"
	),
	false
); ?>


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>